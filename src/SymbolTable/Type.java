package SymbolTable;

import AST.Main;
import javafx.scene.web.WebHistory;


import java.util.*;

public class Type {

    private String name;
    private Map<String, Type> columns = new LinkedHashMap<String, Type>();
    private boolean isPrimitive = false;
    private boolean optional;
    public final static String NUMBER_CONST = "number";
    public final static String STRING_CONST = "string";
    public final static String BOOLEAN_CONST = "boolean";
    public boolean isTable = false;
    public boolean isAgg = false;
    public boolean isPrimitive() {
        return isPrimitive;
    }

    public void setPrimitive(boolean primitive) {
        isPrimitive = primitive;
    }

    public String getName() {
        return name;
    }

    public static boolean hasComplexType(String variable) {
        if (variable == null) return false;
        return !(variable.toLowerCase().equals(NUMBER_CONST) || variable.toLowerCase().equals(STRING_CONST) || variable.toLowerCase().equals(BOOLEAN_CONST));
    }

    public void setName(String name) {
        if ((name.toLowerCase().equals(NUMBER_CONST) || name.toLowerCase().equals(STRING_CONST) || name.toLowerCase().equals(BOOLEAN_CONST))) {
            isPrimitive = true;
        } else {
            isPrimitive = false;
        }
        this.name = name;
    }

    public Map<String, Type> getColumns() {
        return columns;
    }

    public void setColumns(Map<String, Type> columns) {
        this.columns = columns;
    }


    public String getTypeAsString() {
        if (columns == null || columns.isEmpty()) return name;
        return columns.get(name) != null ? columns.get(name).getName() : name;
    }

    public void printColumns() {
        int i = 1;
        System.out.println("Name: " + name);
        for (Map.Entry<String, Type> e : columns.entrySet()) {
            System.out.println("Column " + (i) + " ---> name: " + e.getKey() + ", type: " + e.getValue().getTypeAsString() + ", isPrimitive: " + e.getValue().isPrimitive() + ", isOptional: " + e.getValue().isOptional());
            i++;
        }
    }


    public LinkedHashMap<String, Type> flat(String baseName) {
        if (baseName == null) baseName = this.getName();
        LinkedHashMap<String, Type> rs = new LinkedHashMap<String, Type>();
        for (Map.Entry<String, Type> entry : this.columns.entrySet()) {
            if (entry.getValue().isPrimitive()) {
                rs.put(baseName + '.' + entry.getKey(), entry.getValue());
            } else {
                Type complexType = Main.symbolTable.getTypeByName(entry.getValue().getName());
                rs.putAll(complexType.flat(baseName + '.' + complexType.getName()));
            }
        }
        return rs;
    }

    public void appendColumn(String name, Type type) {
        if(this.getColumns().get(name) != null) return;
        this.getColumns().put(name, type);
    }
    public static boolean compareTwoTypes(Type first, Type second) {
        try {
            if (first.getName().equals(second.getName())) return true;
            if (first.getColumns().size() != second.getColumns().size()) return false;
            List<String> firstList = new ArrayList<String>(first.getColumns().keySet());
            List<String> secondList = new ArrayList<String>(second.getColumns().keySet());
            for (int i = 0; i < firstList.size(); i++) {
                if (!secondList.contains(firstList.get(i))) return false;
            }
            for (int i = 0; i < secondList.size(); i++) {
                if (!firstList.contains(secondList.get(i))) return false;
            }
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    public boolean checkIfTypeIsPartOf(Type t, int index) {
        if (this.getColumns() == null) return false;
        try {
            Map.Entry<String, Type> entry = (Map.Entry<String, Type>) this.getColumns().entrySet().toArray()[index];
            if (Type.hasComplexType(entry.getValue().getTypeAsString())) {
                return Type.compareTwoTypes(t, entry.getValue());
            } else {
                return (t.getName().equals(entry.getValue().getTypeAsString()));
            }
        } catch (Exception e) {
            return false;
        }
    }

    public boolean checkIfAllColumnsAreExisted(ArrayList<String> cols) {
        for (int i = 0; i < cols.size(); i++) {
            if (columns.get(cols.get(i)) == null)
                return false;
        }
        return true;
    }

    public boolean isOptional() {
        return optional;
    }

    public void setOptional(boolean optional) {
        this.optional = optional;
    }

    public static int getMandatoryTypesCounter(Type t) {
        int cn = 0;
        for (Map.Entry<String, Type> entry : t.getColumns().entrySet()) {
            if (!entry.getValue().isOptional()) cn++;
        }
        return cn;
    }

    public static int getOptionalTypesCounter(Type t) {
        int cn = 0;
        for (Map.Entry<String, Type> entry : t.getColumns().entrySet()) {
            if (entry.getValue().isOptional()) cn++;
        }
        return cn;
    }

    public Type getSubTypeByName(String subName) {
        if (this.getColumns() != null) {
            for (Map.Entry<String, Type> entry : this.getColumns().entrySet()) {
                if (entry.getValue().getTypeAsString().equals(subName)) return entry.getValue();
            }
        }
        return null;
    }

    public Type getTypeForColumn(String col) {
        return this.getColumns().get(col);
    }

    public boolean checkIfColumnIsExisted(String col) {
        Type t = columns.get(col);
        return t != null;
    }


    public boolean cEoColumn(String col) {
        for(Map.Entry<String, Type> entry: columns.entrySet()) {
            if(entry.getKey().contains(col)) return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "Type{" +
                "name='" + name + '\'' +
                ", columns=" + columns +
                ", isPrimitive=" + isPrimitive +
                ", optional=" + optional +
                '}';
    }
}
