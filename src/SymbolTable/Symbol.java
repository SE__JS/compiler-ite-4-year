package SymbolTable;

public class Symbol {

    private String name;
    private Type type;
    private Scope scope;
    private boolean isParam;
    private boolean isFunction = false;
    private int numberOfTimeDeclaredInSameScope = 0;

    public String getName() {
        return name;
    }

    public Type getType() {
        return type;
    }

    public Scope getScope() {
        return scope;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public boolean getIsParam() {
        return isParam;
    }

    public void setIsParam(boolean param) {
        isParam = param;
    }

    public boolean isFunction() {
        return isFunction;
    }

    public void setFunction(boolean function) {
        isFunction = function;
    }

    public int getNumberOfTimeDeclaredInSameScope() {
        return numberOfTimeDeclaredInSameScope;
    }

    public void setNumberOfTimeDeclaredInSameScope(int numberOfTimeDeclaredInSameScope) {
        this.numberOfTimeDeclaredInSameScope = numberOfTimeDeclaredInSameScope;
    }

    public String getAsString() {
        return "Symbol{" +
                "name='" + name + '\'' +
                ", type=" + (type == null ? "NoTypeProvided" : type.getTypeAsString()) +
                ", scopeID=" + scope.getId() +
                ", isParam=" + isParam +
                '}';
    }

}
