package SymbolTable;

import javafx.util.Pair;

import java.util.*;


public class SymbolTable {


    private ArrayList<Scope> scopes = new ArrayList<Scope>();

    private ArrayList<Type> declaredTypes = new ArrayList<Type>();
    private ArrayList<AggregationFunctionSymbol> declaredAggregationFunctionSymbol = new ArrayList<AggregationFunctionSymbol>();
    private static Stack<Scope> scopeStack = new Stack<Scope>();


    public ArrayList<Scope> getScopes() {
        return scopes;
    }

    public void setScopes(ArrayList<Scope> scopes) {
        this.scopes = scopes;
    }

    public ArrayList<Type> getDeclaredTypes() {
        return declaredTypes;
    }

    public void setDeclaredTypes(ArrayList<Type> declaredTypes) {
        this.declaredTypes = declaredTypes;
    }


    public void addScope(Scope scope) {
        this.scopes.add(scope);
    }

    public void addType(Type type) {
        this.declaredTypes.add(type);
    }

    public static Scope getLastScope() {
        if (scopeStack.isEmpty()) return null;
        return scopeStack.peek();
    }

    public static void popLastScope() {
        scopeStack.pop();
    }

    public static Scope addScopeToStack(Scope scopeToBePushed) {
        return scopeStack.push(scopeToBePushed);
    }

    public static Stack<Scope> getScopeStack() {
        return scopeStack;
    }

    public static void setScopeStack(Stack<Scope> scopeStack) {
        SymbolTable.scopeStack = scopeStack;
    }


    public boolean doesTypeExist(String type) {
        if (!Type.hasComplexType(type)) return true;
        for (int i = 0; i < declaredTypes.size(); i++) {
            if ((declaredTypes.get(i).getName().equals(type))) {
                return true;
            }
        }
        return false;
    }

    public Type getTypeByName(String typeName) {
        for (int i = 0; i < declaredTypes.size(); i++) {
            if ((declaredTypes.get(i).getName().equals(typeName))) {
                return declaredTypes.get(i);
            }
        }
        
        return null;
    }


    public Type getTypeFromProvidedSymbols(String typeName) {
        for(Scope s: scopes) {
            if(s.getSymbolMap().get(typeName) != null) {
                return s.getSymbolMap().get(typeName).getType();
            }
            for(Map.Entry<String, Symbol> entry: s.getSymbolMap().entrySet()) {
                System.out.println("entry.getKey(): " + entry.getKey() + " :::: " + " Type name: " + typeName);
                if(entry.getKey().toLowerCase().startsWith(typeName.toLowerCase()))
                    return entry.getValue().getType();
            }

            for(Type t: declaredTypes) {
                if(t.getName().startsWith(typeName)) {
                    return t;
                }
            }
        }
        return null;
    }
    public Type getTypeByVariableName(String typeName) {
        for (int i = 0; i < declaredTypes.size(); i++) {
            Type temp = getTypeByName(typeName);
            if (temp != null) return temp;
            LinkedHashMap<String, Type> typeHashMap = (LinkedHashMap<String, Type>) declaredTypes.get(i).getColumns();
            for (Map.Entry<String, Type> entry : typeHashMap.entrySet()) {
                if (entry.getKey().equals(typeName)) return entry.getValue();
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "SymbolTable{" +
                "scopes=" + scopes +
                ", declaredTypes=" + declaredTypes +
                ", declaredAggregationFunction=" + declaredAggregationFunctionSymbol +
                '}';
    }

    public void printSymbolTable() {
        System.out.println("==================Symbol Table==================");

        for (int i = 0; i < scopes.size(); i++) {
            scopes.get(i).printScope();
        }

        /*for(int i = 0; i < declaredTypes.size(); i++) {
            System.out.println("Type " + (i + 1) + " is: " + declaredTypes.get(i));
        }*/
        for (int i = 0; i < declaredAggregationFunctionSymbol.size(); i++) {
            System.out.println("Aggregation Function " + (i + 1) + " is: " + declaredAggregationFunctionSymbol.get(i));
        }

        System.out.println("~_~_~_~_~_~_~_~_~~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~");
        System.out.println("~_~_~_~_~_~_~_~_~~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~");
        System.out.println("==================Declared types==================");
        for (int i = 0; i < declaredTypes.size(); i++) {
            Type t = declaredTypes.get(i);
            t.printColumns();
        }
        System.out.println("==================Declared types Ended==================");
        System.out.println("~_~_~_~_~_~_~_~_~~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~");
        System.out.println("~_~_~_~_~_~_~_~_~~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~_~");
        System.out.println("==================Symbol Table Ended==================");

    }

    public boolean compareTwoColumns(String firstColumn, String secondColumn, String firstTableName, String secondTableName) {
        try {
            Type t1 = getTypeByName(firstTableName);
            Type t2 = getTypeByName(secondTableName);

            Type col1Type = t1.getTypeForColumn(firstColumn);
            Type col2Type = t2.getTypeForColumn(secondColumn);

            return col1Type.getName().toLowerCase().equals(col2Type.getName().toLowerCase());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public ArrayList<AggregationFunctionSymbol> getDeclaredAggregationFunctionSymbol() {
        return declaredAggregationFunctionSymbol;
    }

    public void setDeclaredAggregationFunctionSymbol(ArrayList<AggregationFunctionSymbol> declaredAggregationFunctionSymbol) {
        this.declaredAggregationFunctionSymbol = declaredAggregationFunctionSymbol;
    }

    public boolean checkAggFunction(String funcName) {
        for(AggregationFunctionSymbol symbol: this.declaredAggregationFunctionSymbol) {
            if(symbol.getAggregationFunctionName().equals(funcName)) return true;
        }
        return false;
    }

    public AggregationFunctionSymbol getAggFunc(String funcName) {
        for(AggregationFunctionSymbol symbol: this.declaredAggregationFunctionSymbol) {
            if(symbol.getAggregationFunctionName().equals(funcName)) return symbol;
        }
        return null;
    }
}
