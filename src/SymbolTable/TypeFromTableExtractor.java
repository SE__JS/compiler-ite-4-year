package SymbolTable;

import AST.FileHandler;
import AST.Main;
import CG.Helper.DataMembersFromTypeGetter;
import CG.Helper.TableDetector;
import CG.Mappers.SelectMapper;
import CG.Templates.Core.ClassTemplate;
import CG.Templates.Core.TypeTemplate;
import SingleParse.Java.AllowedName;
import SingleParse.SQL.SqlStmtList.SqlStatement.JoinClause.JoiningTableData;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore.SelectCore;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class TypeFromTableExtractor {

    private static ArrayList<String> getAllColumnDonTBelongToType(Type t,
                                                                  ArrayList<String> normalColumns,
                                                                  ArrayList<Pair<String, String>> complexColumns) {
        ArrayList<String> res = new ArrayList<>();
        for (String nc : normalColumns) {
            if (!t.cEoColumn(nc)) res.add(nc);
        }
        for (Pair<String, String> cc : complexColumns) {
            String toLookFor = cc.getKey() + '.' + cc.getValue();
            if (!t.cEoColumn(toLookFor)) res.add(toLookFor);
        }
        return res;

    }

    private static void handleComplexColumn(Map.Entry<String, Type> entry,
                                            ArrayList<Pair<String, String>> complexColumnsToGet,
                                            Type variableType,
                                            ArrayList<Pair<String, String>> fullColsNameWithTypes) {
        String complexTypeName = entry.getValue().getName(); // If entry user.id, the getValue().getName() returns user type
        Type tableToBeFlatten = Main.symbolTable.getTypeByName(complexTypeName); // Grabbing the complex table

        // Flatten the table
        LinkedHashMap<String, Type> flatHashMap = tableToBeFlatten.flat(complexTypeName);
        String typeName = entry.getValue().getName();
        boolean isTable = TableDetector.isTable(typeName);

        if (flatHashMap != null) {
            for (Map.Entry<String, Type> entry1 : flatHashMap.entrySet()) {
                String[] s = entry1.getKey().split("\\.");

                String fullKey = entry1.getKey();
                Pair<String, String> p = new Pair<>(s[s.length - 2], s[s.length - 1]);

                String arrType = "";
                if (isTable) {
                    arrType = "[]";
                }
                if (complexColumnsToGet != null &&
                        complexColumnsToGet.size() > 0 &&
                        complexColumnsToGet.contains(p)) { // Column User.id is asked for
                    variableType.getColumns().put(entry1.getKey(), entry1.getValue());
                    fullColsNameWithTypes.add(new Pair<String, String>(entry1.getValue().getTypeAsString() + arrType, fullKey));

                } else if (complexColumnsToGet != null &&
                        complexColumnsToGet.size() > 0 &&
                        !complexColumnsToGet.contains(p)) {
                } else { // Select *
                    fullColsNameWithTypes.add(new Pair<String, String>(entry1.getValue().getTypeAsString() + arrType, fullKey));
                    entry1.getValue().setPrimitive(true);
                    variableType.getColumns().put(entry1.getKey(), entry1.getValue());
                }
            }
        }
    }

    private static void handlePrimitiveColumn(Map.Entry<String, Type> entry,
                                              ArrayList<String> normalColumnsToGet,
                                              Type variableType,
                                              ArrayList<Pair<String, String>> fullColsNameWithTypes) {


        // This condition to prevent un-required columns to be present in the final type, if not asked from user
        if (normalColumnsToGet != null && normalColumnsToGet.size() > 0 && !normalColumnsToGet.contains(entry.getKey())) {
            return; // Means this column is not requested from user, just escape
        }
        Type t = new Type();
        // Reach here, hence this column is asked for, create and fill the type accordingly
        t.setName(entry.getValue().getTypeAsString());
        t.setPrimitive(true);
        variableType.getColumns().put(entry.getKey(), t);
        fullColsNameWithTypes.add(new Pair<String, String>(entry.getValue().getTypeAsString(), entry.getKey()));
    }

    private static void fillTypeHelper(Type tableType,
                                       Type variableType,
                                       ArrayList<String> normalColumnsToGet,
                                       ArrayList<Pair<String, String>> complexColumnsToGet,
                                       ArrayList<Pair<String, String>> fullColsNameWithTypes) {
        // Getting the columns from type
        LinkedHashMap<String, Type> hashMap = (LinkedHashMap<String, Type>) tableType.getColumns();

        for (Map.Entry<String, Type> entry : hashMap.entrySet()) {
            if (entry.getValue().isPrimitive()) {

                if (complexColumnsToGet != null &&
                        complexColumnsToGet.size() > 0 &&
                        (normalColumnsToGet == null || normalColumnsToGet.size() == 0)) continue;


                handlePrimitiveColumn(entry,
                        normalColumnsToGet,
                        variableType,
                        fullColsNameWithTypes);
            } else { // Like user.name, here we need to flatten the object user

                if (normalColumnsToGet != null &&
                        normalColumnsToGet.size() > 0 &&
                        (complexColumnsToGet == null || complexColumnsToGet.size() == 0)) continue;

                handleComplexColumn(entry,
                        complexColumnsToGet,
                        variableType,
                        fullColsNameWithTypes);
            }
        }


    }

    private static Map<String, Type> getTypesForColumns(String tableName, ArrayList<String> cols) {
        Type t = Main.symbolTable.getTypeByName(tableName);
        if (t == null) {
            t = Main.symbolTable.getTypeFromProvidedSymbols(tableName);
        }
        if (t == null) return null;
        Map<String, Type> ans = new HashMap<String, Type>();
        for (String col : cols) {
            String[] str = col.split("\\.");
            String toCheck = str[str.length - 1];
            if (t.checkIfColumnIsExisted(toCheck)) {
                ans.put(toCheck, t.getTypeForColumn(toCheck));
            }
        }
        return ans;
    }

    public static void fillVariableTypeFromTable(Type variableType,
                                                 String tableName,
                                                 ArrayList<String> normalColumnsToGet,
                                                 ArrayList<Pair<String, String>> complexColumnsToGet,
                                                 AllowedName variableAllowedName,
                                                 SelectCore selectCore) {
        if (Main.symbolTable.doesTypeExist(tableName)) {
            Type tableType = Main.symbolTable.getTypeByName(tableName);
            if (tableType != null) {

                ArrayList<Pair<String, String>> fullColsNameWithTypes = new ArrayList<>();

                fillTypeHelper(tableType,
                        variableType,
                        normalColumnsToGet,
                        complexColumnsToGet,
                        fullColsNameWithTypes
                );


                JoiningTableData joiningTableData = SelectCore.getJoiningData(selectCore);
                if (joiningTableData != null && joiningTableData.getFirstTableName() != null && joiningTableData.getSecondTableName() != null) {
                    ArrayList<String> columnForJoinStatement = getAllColumnDonTBelongToType(variableType, normalColumnsToGet, complexColumnsToGet);
                    Map<String, Type> joinColumns = getTypesForColumns(joiningTableData.getSecondTableName(), columnForJoinStatement);
                    assert joinColumns != null;
                    /*Type t = new Type();
                    t.setName(joiningTableData.getSecondTableName());
                    t.setColumns(joinColumns);*/
                    variableType.getColumns().putAll(joinColumns);
                }


                ArrayList<Pair<String, String>> aggFuncs = SelectCore.getAggregationFunctionsInQuery(selectCore);
                for (Pair<String, String> agF : aggFuncs) {
                    AggregationFunctionSymbol symbol = Main.symbolTable.getAggFunc(agF.getKey());
                    if(symbol == null) continue;
                    Type t = new Type();
                    String javaType = TypeTemplate.getPrimitiveType(symbol.getReturnType());
                    t.setName(javaType);
                    t.setPrimitive(true);
                    t.isAgg = true;
                    String dataMemberName = agF.getValue();
                    dataMemberName = dataMemberName.replace(".", "_");
                    dataMemberName = dataMemberName.replace("*", "all");
                    variableType.getColumns().put(agF.getKey() /* + "_" + dataMemberName*/, t);
                }


                Symbol s = variableAllowedName.getSymbol();
                if (s != null) {
                    variableAllowedName.getSymbol().setType(variableType);
                }

                /**
                 * Creating new type for this select query and assigned variable
                 */
                System.out.println("WE ARE ADDING A TYPE OF: " + variableType.getName());
                Main.symbolTable.getDeclaredTypes().add(variableType);


                /**
                 * Creating java class for this custom type
                 */

                ArrayList<Pair<String, String>> dataMembers = DataMembersFromTypeGetter.get(variableType);
                createClassForType(variableType.getName(), dataMembers);

                /**
                 *
                 * Here: the mapping from SQL query to java code will begin
                 * Some data needed to be passed down
                 */
                handleMappingToJava(selectCore,
                        variableType,
                        tableName,
                        fullColsNameWithTypes);
            }
        }

    }

    private static void createClassForType(String typeName, ArrayList<Pair<String, String>> dataMembers) {
        String classTemplate = ClassTemplate.getClassTemplate("public", typeName, dataMembers, false);
        FileHandler.writeJavaFile(typeName + ".java", classTemplate);
    }

    private static void handleMappingToJava(SelectCore selectCore,
                                            Type variableType,
                                            String tableName,
                                            ArrayList<Pair<String, String>> fullColsNameWithTypes) {

        SelectMapper.assigningSelectToClass(tableName,
                variableType,
                selectCore,
                fullColsNameWithTypes);
    }


}
