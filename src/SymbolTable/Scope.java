package SymbolTable;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

public class Scope {
    private String id;
    private Scope parent;
    private Map<String, Symbol> symbolMap = new LinkedHashMap<String, Symbol>();
    private boolean openScope = true;
    public Scope() {
        this.id = UUID.randomUUID().toString();
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public Scope getParent() {
        return parent;
    }

    public void setParent(Scope parent) {
        this.parent = parent;
    }

    public Map<String, Symbol> getSymbolMap() {
        return symbolMap;
    }

    public void setSymbolMap(Map<String, Symbol> symbolMap) {
        this.symbolMap = symbolMap;
    }
    public void addSymbol(String name, Symbol symbol) {
        int counter = 0;
        if(symbolMap.get(name) != null) {
            Symbol s = symbolMap.get(name);
            counter = s.getNumberOfTimeDeclaredInSameScope();
            counter++;
            s.setNumberOfTimeDeclaredInSameScope(counter);
        } else {
            counter++;
            symbol.setNumberOfTimeDeclaredInSameScope(counter);
            this.symbolMap.put(name,symbol);
        }
    }




    @Override
    public String toString() {
        return "Scope{" +
                "id='" + id + '\'' +
                ", parent=" + parent +
                ", symbolMap=" + symbolMap +
                '}';
    }

    public void printScope() {
        System.out.println("==================Scope==================");
        String id = getParent() != null ? getParent().getId() : "_";
        System.out.println("Scope ID: " + this.getId());
        System.out.println("Parent scope ID: " + id);
        System.out.println("Symbols:");
        for(Map.Entry<String, Symbol> entry : symbolMap.entrySet()) {
            System.out.println("Symbol name: " + entry.getKey() + " :::: Symbol value: " + entry.getValue().getAsString());
        }
        System.out.println("==================Scope Ended==================");
    }

    public boolean hasVariableBeenDeclared(String variable) {
        if(symbolMap.get(variable) != null) {
            return true;
        }
        if(this.getParent() == null) return false;
        return this.getParent().hasVariableBeenDeclared(variable);
    }
    public int numberOfDeclaration(String variable, int counter) {
        if(symbolMap.get(variable) != null && this.getParent() != null) {
            return getParent().numberOfDeclaration(variable, counter + 1);
        }
        if(this.getParent() == null) {
            if(symbolMap.get(variable) != null) {
                ++counter;
            }
            return counter;
        }
        return counter;
    }

    public boolean isOpenScope() {
        return openScope;
    }

    public void setOpenScope(boolean openScope) {
        this.openScope = openScope;
    }


    public boolean belongsToSameScope(String var1, String var2) {
        return (symbolMap.get(var1) != null && symbolMap.get(var2) != null);
    }

    public Type getVariableTypeFromScope(String variableName) {
        if(symbolMap.get(variableName) != null) {
            return symbolMap.get(variableName).getType();
        }
        if(this.getParent() != null) {
            return this.getParent().getVariableTypeFromScope(variableName);
        }
        return null;
    }

    public String getVariableType(String variableName) {
        if(symbolMap.get(variableName) != null) {
            Type t = symbolMap.get(variableName).getType();
            return t != null ? t.getTypeAsString() : "";
        }
        if(this.getParent() != null) {
            return this.getParent().getVariableType(variableName);
        }
        return "";
    }


    public Symbol getVariableSymbol(String variableName) {
        return symbolMap.get(variableName);
    }
}
