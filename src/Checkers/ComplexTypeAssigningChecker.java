package Checkers;

import AST.FileHandler;
import SingleParse.Java.AllowedName;
import SymbolTable.Scope;
import SymbolTable.Symbol;
import SymbolTable.Type;

public class ComplexTypeAssigningChecker {
    public static void check(AllowedName variable, String assignerName) {
        Scope currentScope = variable.getScope();
        String assignedVarType = currentScope.getVariableType(variable.getName());
        String assignerVarType = currentScope.getVariableType(assignerName);
        if (assignerVarType == null) return;
        if (Type.hasComplexType(assignerName)) {
            if(assignedVarType == null) {
                Type t = currentScope.getVariableTypeFromScope(assignerName);
                Symbol s = currentScope.getVariableSymbol(variable.getName());
                s.setType(t);
            }
            else {
                Type leftSide = currentScope.getVariableTypeFromScope(variable.getName());
                Type rightSide = currentScope.getVariableTypeFromScope(assignerName);
                if(leftSide == null || rightSide == null) return;
                if (!Type.compareTwoTypes(leftSide, rightSide)) {
                    String fileName = "errors.txt";
                    String declarationError = "Error on line " + variable.getLine() + " on charAt " + variable.getCol() + " trying to assign different type to variable " + variable.getName() + "\n";
                    FileHandler.printToFile(fileName, declarationError);
                }
            }
        }
    }
}
