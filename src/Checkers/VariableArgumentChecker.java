package Checkers;

import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarArgument;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.VarOperator;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore.SelectCore;
import SingleParse.SQL.SqlStmtList.SqlStatement.SqlStatement;
import SymbolTable.Type;
import SymbolTable.TypeFromTableExtractor;
import javafx.util.Pair;

import java.util.ArrayList;

public class VariableArgumentChecker {

    public static void check(VarArgument varArgument) {
        String variableName = varArgument.getAllowedName().getName();
        Type variableType = new Type();
        variableType.setName(variableName);
        VarOperator varOperator = varArgument.getVarOperator();
        if (varOperator != null) {
            SqlStatement sqlStatement = VarOperator.getValueAsSqlStatement(varOperator);
            if (sqlStatement != null) {
                SelectCore selectCore = SqlStatement.getSelectCore(sqlStatement);
                if (selectCore != null) {
                    // Main columns requested like id, name ....
                    ArrayList<String> cols = SelectCore.getColumnsRequestedInThisQuery(selectCore);
                    // Complex columns requested like user.id, student.age ....
                    ArrayList<Pair<String, String>> complexCols = SelectCore.getSqlColumnsThisQuery(selectCore);
                    // Table name asked for
                    String tableName = SqlStatement.getTableName(sqlStatement);

                    // To get not a conflict with other types
                    variableType.setName(variableName + "_" + tableName);


                    // Create a new type for this variable according to columns requested
                    TypeFromTableExtractor.fillVariableTypeFromTable(variableType,
                            tableName,
                            cols,
                            complexCols,
                            varArgument.getAllowedName(),
                            selectCore);



                }
            } else {
                Pair<String, String> vt = VarArgument.getVarAndType(varArgument);
                if (vt != null) {
                    if (Type.hasComplexType(vt.getValue())) {
                        Type newType = varArgument.getAllowedName().getScope().getVariableTypeFromScope(vt.getValue());
                        varArgument.getAllowedName().getSymbol().setType(newType);
                    }
                }
            }
        }
    }
}
