package Checkers;

import AST.FileHandler;
import AST.Main;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore.SelectCore;
import SymbolTable.Type;
import javafx.util.Pair;

import java.util.ArrayList;

public class ComplexColumnsInQueryChecker {
    public static void check(SelectCore selectCore, Type table, ArrayList<Pair<String, String>> complexColumns) {
        if (table != null) {
            for (Pair<String, String> sqlC : complexColumns) {
                if (sqlC != null) {
                    //boolean complexTypeIsPartOfRequestedTableType = table.checkIfColumnIsExisted(sqlC.getKey());
                    // Ex: select address.city from user
                    // we need to check if address is part of user type as well as we need to check if city is part of address
                    //if (!complexTypeIsPartOfRequestedTableType) {
                        //String fileName = "errors.txt";
                        //String err = "Error on line " + selectCore.getLine() + " on charAt " + selectCore.getCol() + ", type " + sqlC.getKey() + " does not exists on " + table.getName() + "\n";
                        //ErrorHandler.printToFile(fileName, err);
                    //}

                    // In address.city the following line will return address
                    Type t = table.getSubTypeByName(sqlC.getKey());
                    /*if(t == null) {
                        String fileName = "errors.txt";
                        String err = "Error on line " + selectCore.getLine() + " on charAt " + selectCore.getCol() + ", check if table " + sqlC.getKey() + "\n";
                        ErrorHandler.printToFile(fileName, err);
                    }*/

                    if (t != null) {
                        Type mainType = Main.symbolTable.getTypeByName(t.getName());
                        if(mainType.getTypeAsString() != null) {
                            // Checking if city is part of address type
                            boolean isExisted = mainType.checkIfColumnIsExisted(sqlC.getValue());
                            if (!isExisted) {
                                String fileName = "errors.txt";
                                String err = "Error on line " + selectCore.getLine() + " on charAt " + selectCore.getCol() + ", column " + sqlC.getValue() + " does not exists on " + sqlC.getKey() + "\n";
                                FileHandler.printToFile(fileName, err);
                            }
                        }
                    }
                }
            }
        } else {
            String fileName = "errors.txt";
            String finalError = "";
            String err = "Error on line " + selectCore.getLine() + " on charAt " + selectCore.getCol() + ", column does not exists on null table\n";
            for (Pair<String, String> sqlC : complexColumns) {
                if (sqlC != null) {
                    finalError = finalError.concat(err);
                }
            }
            FileHandler.printToFile(fileName, finalError);
        }
    }
}
