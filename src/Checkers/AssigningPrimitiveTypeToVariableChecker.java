package Checkers;

import AST.FileHandler;
import SingleParse.Java.AllowedName;
import SymbolTable.Scope;
import SymbolTable.Symbol;
import SymbolTable.Type;

public class AssigningPrimitiveTypeToVariableChecker {
    public static void check(AllowedName variable, String assignerType) {
        if (assignerType.equals("string") || assignerType.equals("boolean") || assignerType.equals("number")) {
            Scope variableScope = variable.getScope();
            String previousVariableType = variableScope.getVariableType(variable.getName());
            if (previousVariableType == null) {
                Type t = new Type();
                t.setPrimitive(true);
                t.setName(assignerType);
                Symbol s = variableScope.getVariableSymbol(variable.getName());
                s.setType(t);
            } else if (!previousVariableType.equals(assignerType)) {
                String fileName = "errors.txt";
                String declarationError = "Error on line " + variable.getLine() + " on charAt " + variable.getCol() + " variable of type " + previousVariableType + " cannot have a value of type " + assignerType + "\n";
                FileHandler.printToFile(fileName, declarationError);
            }
        }
    }
}
