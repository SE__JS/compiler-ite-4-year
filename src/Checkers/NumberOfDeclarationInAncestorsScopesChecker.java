package Checkers;

import AST.FileHandler;
import SingleParse.Java.AllowedName;

public class NumberOfDeclarationInAncestorsScopesChecker {
    public static void check(AllowedName allowedName) {
        if (!allowedName.isIgnore() &&
                (!allowedName.getName().startsWith("\"") && !allowedName.getName().startsWith("'"))) {
            // Check declaration in ancestors scopes
            int numOfD = allowedName.getScope().numberOfDeclaration(allowedName.getName(), 0);
            if (numOfD > 1) {
                String fileName = "errors.txt";
                String declarationError = "Error on line " + allowedName.getLine() + " on charAt " + allowedName.getCol() + ", variable " + allowedName.getName() + " got declared " + numOfD + " in ancestors scopes" + "\n";
                FileHandler.printToFile(fileName, declarationError);
            }
        }

    }
}
