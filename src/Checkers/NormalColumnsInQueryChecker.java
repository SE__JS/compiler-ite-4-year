package Checkers;

import AST.FileHandler;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore.SelectCore;
import SymbolTable.Type;

import java.util.ArrayList;

public class NormalColumnsInQueryChecker {
    public static void check(SelectCore selectCore, Type table, ArrayList<String> cols) {
        if (table != null) {
            boolean allPresented = table.checkIfAllColumnsAreExisted(cols);
            if (!allPresented) {
                String fileName = "errors.txt";
                String err = "Error on line " + selectCore.getLine() + " on charAt " + selectCore.getCol() + ", Un-declared column\n";
                FileHandler.printToFile(fileName, err);
            }
        } else {
            String fileName = "errors.txt";
            String finalError = "";
            String err = "Error on line " + selectCore.getLine() + " on charAt " + selectCore.getCol() + ", column does not exists on null table\n";
            for (String c : cols) {
                finalError = finalError.concat(err);
            }
            FileHandler.printToFile(fileName, finalError);
        }
    }
}
