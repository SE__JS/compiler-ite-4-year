package Checkers;

import AST.FileHandler;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarArgument;

import java.util.ArrayList;

public class UnAssigningVariableChecker {
    public static void check(ArrayList<VarArgument> varArgumentArrayList) {
        for (int i = 0; i < varArgumentArrayList.size(); i++) {
            VarArgument argument = varArgumentArrayList.get(i);
            if (argument.getVarOperator() == null) {

                String fileName = "errors.txt";


                String declarationError = "Warning on line " + argument.getLine() + " on charAt " +
                        argument.getCol() + " variable " +
                         argument.getAllowedName().getName() +
                        " has not been assigned" + "\n";


                FileHandler.printToFile(fileName, declarationError);
            }
        }
    }
}
