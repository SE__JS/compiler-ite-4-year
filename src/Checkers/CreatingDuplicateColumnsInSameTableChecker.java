package Checkers;

import AST.FileHandler;
import SingleParse.SQL.SqlStmtList.SqlStatement.Loader.CreateTableStatement.AllowedCreateTableParam;
import SingleParse.SQL.SqlStmtList.SqlStatement.Loader.CreateTableStatement.CreateTableParams;

import java.util.ArrayList;
import java.util.HashSet;

public class CreatingDuplicateColumnsInSameTableChecker {
    public static void check(CreateTableParams createTableParams) {
        HashSet<String> hashSet = new HashSet<>();
        ArrayList<AllowedCreateTableParam> allowedCreateTableParamArrayList = createTableParams.getAllowedCreateTableParamArrayList();
        if (allowedCreateTableParamArrayList != null && allowedCreateTableParamArrayList.size() > 0) {
            for (int i = 0; i < allowedCreateTableParamArrayList.size(); i++) {
                AllowedCreateTableParam param = allowedCreateTableParamArrayList.get(i);
                if (hashSet.contains(param.getAllowedName().getName())) {
                    String fileName = "errors.txt";
                    String err = "Error on line " + param.getLine() + " on charAt " + param.getCol() + ", column " + param.getAllowedName().getName() + " already exists " + "\n";
                    FileHandler.printToFile(fileName, err);
                } else {
                    hashSet.add(param.getAllowedName().getName());
                }
            }

        }
    }
}
