package Checkers;

import AST.FileHandler;
import AST.Main;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.PossibleValue.DotBlock;
import SymbolTable.Type;

import java.util.ArrayList;

public class DotBlockChecker {
    public static void check(DotBlock dotBlock) {
        ArrayList<String> names = new ArrayList<>();
        DotBlock.chainedObjects(dotBlock, names);
        for (int i = 0; i < names.size() - 1; i++) {
            String firstName = names.get(i);
            String secondName = names.get(i + 1);
            Type firstType = Main.symbolTable.getTypeByVariableName(firstName);
            if (firstType == null) {
                String fileName = "errors.txt";
                String err = "Error on line " + dotBlock.getLine() + " on charAt " + dotBlock.getCol() + ", type " + firstName + " does not exist " + "\n";
                FileHandler.printToFile(fileName, err);
            } else {
                Type secondType = Main.symbolTable.getTypeByVariableName(secondName);
                if (secondType != null) {
                    boolean partOf = firstType.checkIfColumnIsExisted(secondName);
                    if (!partOf) {
                        String fileName = "errors.txt";
                        String err = "Error on line " + dotBlock.getLine() + " on charAt " + dotBlock.getCol() + ", type " + secondName + " is not part of " + firstName + "\n";
                        FileHandler.printToFile(fileName, err);
                    }
                }


            }
        }
    }
}
