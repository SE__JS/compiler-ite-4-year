package Checkers;

import AST.FileHandler;
import SingleParse.SQL.SqlStmtList.SqlStatement.JoinClause.JoinClauseMain;

public class OnOperatorInJoinClauseChecker {
    public static void check(JoinClauseMain joinClauseMain) {
        boolean hasOnInIt = JoinClauseMain.hasOnInJoinConstraint(joinClauseMain);
        if(!hasOnInIt) {
            String fileName = "errors.txt";
            String err = "Error on line " + joinClauseMain.getLine() + " on charAt " + joinClauseMain.getCol() + " join clause cannot have {ON} operation\n";
            FileHandler.printToFile(fileName, err);
        }
    }
}
