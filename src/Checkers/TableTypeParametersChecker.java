package Checkers;

import AST.FileHandler;
import AST.Main;
import SingleParse.SQL.SqlStmtList.SqlStatement.Loader.CreateTableStatement.TableParamType;

public class TableTypeParametersChecker {
    public static void check(TableParamType tableParamType) {
        if (tableParamType.getType() != null) {
            String typeToLookFor = tableParamType.getType();
            boolean found = Main.symbolTable.doesTypeExist(typeToLookFor);
            if (!found) {
                String fileName = "errors.txt";
                String err = "Error on line " + tableParamType.getLine() + " on charAt " + tableParamType.getCol() + ", Un-declared parameter " + typeToLookFor + "\n";
                FileHandler.printToFile(fileName, err);
            }
        }
    }
}
