package Checkers;

import AST.FileHandler;
import SingleParse.Java.AllowedName;
import SymbolTable.Scope;
import SymbolTable.Symbol;
import SymbolTable.Type;
import SymbolTable.TypeFromTableExtractor;

public class AssigningVariableToVariableChecker {
    public static void check(AllowedName variable, String assignedVariableName, String assignerVariableName) {
        if (!assignerVariableName.startsWith("\"") && !assignerVariableName.startsWith("'")
        && Type.hasComplexType(assignerVariableName)) {

            String leftSideName = variable.getName();
            Scope s = variable.getScope();

            // Getting the right side variable's type
            Type rightSideType = s.getVariableTypeFromScope(assignerVariableName);
            String leftType = s.getVariableType(assignedVariableName);
            if(!Type.hasComplexType(rightSideType.getTypeAsString())) return;

            if (leftType == null) {
                // Getting assigned variable symbol
                Symbol leftSymbol = s.getVariableSymbol(assignedVariableName);
                // Creating a type for left-hand variable
                Type leftSideType = new Type();
                // Setting variable's type name
                leftSideType.setName(rightSideType.getName());
                leftSideType.setPrimitive(false);

                // Filling the left side variable's type as it should be existed in declared types
                TypeFromTableExtractor.fillVariableTypeFromTable(leftSideType, assignerVariableName, null, null, variable, null);
                leftSideType.printColumns();
                if (leftSymbol != null) {
                    leftSymbol.setType(leftSideType);
                }
                leftSideType.printColumns();
            } else {

                if (rightSideType != null) {
                    Type leftSideType = s.getVariableTypeFromScope(assignedVariableName);
                    boolean equaled = Type.compareTwoTypes(leftSideType, rightSideType);
                    if (!equaled) {
                        String fileName = "errors.txt";
                        String declarationError = "Error on line " + variable.getLine() + " on charAt " + variable.getCol() + " trying to assign a value with different type to variable" + "\n";
                        FileHandler.printToFile(fileName, declarationError);
                    }
                } else {
                    if (Type.hasComplexType(assignerVariableName)) {
                        String fileName = "errors.txt";
                        String declarationError = "Error on line " + variable.getLine() + " on charAt " + variable.getCol() + " right side variable " + assignerVariableName + " has not type" + "\n";
                        FileHandler.printToFile(fileName, declarationError);
                    }

                }

            }

        }
    }
}
