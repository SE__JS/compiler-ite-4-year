package Checkers;

import AST.Main;
import SingleParse.Java.AllowedName;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarArgument;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarAssignment;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.VarOperator;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore.SelectCore;
import SingleParse.SQL.SqlStmtList.SqlStatement.SqlStatement;
import SymbolTable.Scope;
import SymbolTable.Type;
import SymbolTable.TypeFromTableExtractor;
import javafx.util.Pair;

import java.util.ArrayList;

public class VariableAssignmentChecker {
    public static void check(VarAssignment varAssignment) {
        AllowedName varAllowedName = varAssignment.getAllowedName();

        AssignedVariableChecker.check(varAllowedName);

        VarOperator varOperator = varAssignment.getVarOperator();
        Type variableType = Main.symbolTable.getTypeByName(varAllowedName.getName());
        if(variableType == null) {
            variableType = new Type();
        }
        if (varOperator != null) {
            SqlStatement sqlStatement = VarOperator.getValueAsSqlStatement(varOperator);
            if (sqlStatement != null) {
                SelectCore selectCore = SqlStatement.getSelectCore(sqlStatement);
                if (selectCore != null) {
                    // Main columns requested like id, name ....
                    ArrayList<String> cols = SelectCore.getColumnsRequestedInThisQuery(selectCore);
                    // Complex columns requested like user.id, student.age ....
                    ArrayList<Pair<String, String>> complexCols = SelectCore.getSqlColumnsThisQuery(selectCore);
                    // Table name asked for
                    String tableName = SqlStatement.getTableName(sqlStatement);

                    // To get not a conflict with other types
                    variableType.setName(varAllowedName.getName() + "_" + tableName);

                    ArrayList<Pair<String, String>> aggFuncs = SelectCore.getAggregationFunctionsInQuery(selectCore);
                    // Create a new type for this variable according to columns requested
                    TypeFromTableExtractor.fillVariableTypeFromTable(variableType,
                            tableName,
                            cols,
                            complexCols,
                            varAssignment.getAllowedName(),
                            selectCore);
                }
            }
        }
        if (!varAllowedName.isIgnore()) {
            Scope s = varAllowedName.getScope();
            Pair<String, String> fullVariable = VarAssignment.getVariableAndItsAssignmentType(varAssignment);
            String assignedName = fullVariable.getKey();
            String assignerName = fullVariable.getValue();

            try {
                // Checking if assigning to primitive types
                AssigningPrimitiveTypeToVariableChecker.check(varAllowedName, assignerName);


                // Assigning variable to variable checker
                AssigningVariableToVariableChecker.check(varAllowedName, assignedName, assignerName);

                // Assigning variable to variable checker
                ComplexTypeAssigningChecker.check(varAllowedName, assignerName);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }


        }
    }
}
