package Checkers;

import AST.FileHandler;
import SingleParse.Java.AllowedName;
import SymbolTable.Scope;

public class VariableDeclarationChecker {
    public static void check(AllowedName allowedName) {
        if (!allowedName.isIgnore() &&
                (!allowedName.getName().startsWith("\"") && !allowedName.getName().startsWith("'")) && !allowedName.isFunctionName()) {
            boolean gotDeclared;
            Scope scope = allowedName.getScope();
            if (scope != null) {
                gotDeclared = scope.hasVariableBeenDeclared(allowedName.getName());
                if (!gotDeclared) {
                    String fileName = "errors.txt";
                    String err = "Error on line " + allowedName.getLine() + " on charAt " + allowedName.getCol() + ", Un-declared variable name " + allowedName.getName() + "\n";
                    FileHandler.printToFile(fileName, err);
                }
            }
        }
    }
}
