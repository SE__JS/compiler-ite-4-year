package Checkers;

import AST.Main;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore.SelectCore;
import SymbolTable.Type;
import javafx.util.Pair;

import java.util.ArrayList;

public class NormalAndComplexColumnChecker {
    public static void check(SelectCore selectCore) {
        ArrayList<String> cols = SelectCore.getColumnsRequestedInThisQuery(selectCore);
        // Grabbing table name
        String tableName = SelectCore.getTableInThisQuery(selectCore);

        // Checking normal columns
        Type tableType = Main.symbolTable.getTypeByName(tableName);
        NormalColumnsInQueryChecker.check(selectCore, tableType, cols);

        // Checking complex columns
        ArrayList<Pair<String, String>> sqlColumns = SelectCore.getSqlColumnsThisQuery(selectCore);
        ComplexColumnsInQueryChecker.check(selectCore, tableType, sqlColumns);
    }
}
