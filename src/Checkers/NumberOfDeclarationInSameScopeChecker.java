package Checkers;

import AST.FileHandler;
import SingleParse.Java.AllowedName;
import SymbolTable.Symbol;

public class NumberOfDeclarationInSameScopeChecker {
    public static void check(AllowedName allowedName) {
        if (!allowedName.isIgnore() &&
                (!allowedName.getName().startsWith("\"") && !allowedName.getName().startsWith("'"))) {

            Symbol symbol = allowedName.getSymbol();
            // Check declaration in the same scope
            if (symbol != null && symbol.getNumberOfTimeDeclaredInSameScope() > 1) {
                String fileName = "errors.txt";
                String declarationError = "Error on line " + allowedName.getLine() + " on charAt " + allowedName.getCol() + ", variable " + allowedName.getName() + " got declared " + symbol.getNumberOfTimeDeclaredInSameScope() + " in the same scope" + "\n";
                FileHandler.printToFile(fileName, declarationError);
            }
        }

    }
}
