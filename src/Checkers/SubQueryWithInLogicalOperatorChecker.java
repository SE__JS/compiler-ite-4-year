package Checkers;

import AST.FileHandler;
import AST.Main;
import SingleParse.Expression.Expression;
import SingleParse.Java.AnyStatement.SingleLineStatement.TerminalNode;
import SingleParse.Java.AnyStatement.TernaryExpression.TernaryExpression;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore.SelectCore;

public class SubQueryWithInLogicalOperatorChecker {

    public static void helperCheck(SelectCore main, SelectCore sub) {
        try {
            String firstTableName = SelectCore.getTableInThisQuery(main);
            String firstColName = SelectCore.getColumnsRequestedInThisQuery(main).get(0);
            String secondTableName = SelectCore.getTableInThisQuery(sub);
            String secondColName = SelectCore.getColumnsRequestedInThisQuery(sub).get(0);

            boolean matched = Main.symbolTable.compareTwoColumns(firstColName, secondColName, firstTableName, secondTableName);
            if(!matched) {
                String fileName = "errors.txt";
                String err = "Error on line " + main.getLine() + " on charAt " + main.getCol() +  " matching " + "Error on line " + sub.getLine() + " on charAt " + sub.getCol() +  " selected columns don't have the same type  " + " \n";
                FileHandler.printToFile(fileName, err);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
    public static void check(SelectCore selectCore) {
        Expression expression = selectCore.getWhereClause().getExpression();
        if (expression != null) {

            Expression subExp = expression.getExpressionArrayList().get(1);
            TernaryExpression ternaryExpression = subExp.getTernaryExpression();

            TerminalNode trNode = null;
            if(ternaryExpression.getTerminalNode() != null) {
                trNode = ternaryExpression.getTerminalNode();
            } else {
                trNode = ternaryExpression.getTernaryExpressionArrayList().get(0).getTerminalNode();
            }
            if (trNode != null) {
                SelectCore subQuery = trNode.getSelectCoreFromTerminalNode();
                if (subQuery != null) {

                    // To check if selected column match with selected sub query column type
                    helperCheck(selectCore, subQuery);

                    NormalAndComplexColumnChecker.check(subQuery);
                    String operator = expression.getConditionOperator();
                    int numberOfColumns = SelectCore.totalCounterOfColumns(subQuery);
                    if (operator.toLowerCase().equals("in") && numberOfColumns != 1) {
                        String fileName = "errors.txt";
                        String err = "Error on line " + subQuery.getLine() + " on charAt " + subQuery.getCol() + " request number of columns is not allowed\n";
                        FileHandler.printToFile(fileName, err);
                    }
                }
            }
        }
    }
}
