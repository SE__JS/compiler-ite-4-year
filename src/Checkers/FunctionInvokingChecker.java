package Checkers;

import AST.FileHandler;
import AST.Main;
import SingleParse.Java.AllowedName;
import SingleParse.Java.Functions.Argument;
import SingleParse.Java.Functions.InvokingFunction;
import SymbolTable.Scope;
import SymbolTable.Type;

import java.util.ArrayList;

public class FunctionInvokingChecker {
    public static void check(InvokingFunction invokingFunction) {
        AllowedName functionName = invokingFunction.getAllowedName();
        ArrayList<Argument> arguments = invokingFunction.getArgumentsList();

        if (!functionName.isIgnore() &&
                (!functionName.getName().startsWith("\"") && !functionName.getName().startsWith("'"))) {
            Scope scope = functionName.getScope();
            if (scope != null) {

                Type functionType = Main.symbolTable.getTypeByVariableName(functionName.getName());
                if (functionType == null) {
                    String fileName = "errors.txt";
                    String err = "Error on line " + functionName.getLine() + " on charAt " + functionName.getCol() + ", " + functionName.getName() + " is an un-defined method" + "\n";
                    FileHandler.printToFile(fileName, err);
                } else {
                    int mainCounter = Type.getMandatoryTypesCounter(functionType);
                    // int optionalCounter = Type.getOptionalTypesCounter(functionType);
                    if (mainCounter > arguments.size()) {
                        String fileName = "errors.txt";
                        String err = "Error on line " + functionName.getLine() + " on charAt " + functionName.getCol() + ", not enough arguments for function " + functionName.getName() + "\n";
                        FileHandler.printToFile(fileName, err);
                    } else {
                        for (int i = 0; i < arguments.size(); i++) {
                            String arg = arguments.get(i).getActualValue();

                            Type argType = new Type();
                            if (arg.startsWith("\"") || arg.startsWith("'")) argType.setName("string");
                            else argType.setName(arg);
                            boolean theSame = functionType.checkIfTypeIsPartOf(argType, i);
                            if (!theSame) {
                                String fileName = "errors.txt";
                                String err = "Error on line " + arguments.get(i).getLine() + " on charAt " + arguments.get(i).getCol() + ", mis-match argument type" + "\n";
                                FileHandler.printToFile(fileName, err);
                            }
                        }
                    }

                }

//                boolean gotDeclared;
//                gotDeclared = scope.hasVariableBeenDeclared(functionName.getName());
//                if (!gotDeclared) {
//                    String fileName = "errors.txt";
//                    String err = "Error on line " + functionName.getLine() + " on charAt " + functionName.getCol() + ", " + functionName.getName() + " is an un-defined method" + "\n";
//                    ErrorHandler.printToFile(fileName, err);
//                }
            }
        }
    }
}
