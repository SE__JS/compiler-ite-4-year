package Checkers;

import AST.FileHandler;
import SingleParse.Java.AllowedName;

public class AssignedVariableChecker {
    // This check if we're assigning to
    // "example" = variable;
    // true = variable;
    // 5 = 2;
    // All above should throw an error
    public static void check(AllowedName variable) {
        String variableName = variable.getName();
        if (variableName.startsWith("\"") || variableName.startsWith("'") || variable.equals("boolean") || variableName.equals("number")) {
            String fileName = "errors.txt";
            String declarationError = "Error on line " + variable.getLine() + " on charAt " + variable.getCol() + " Expected to be a variable" + "\n";
            FileHandler.printToFile(fileName, declarationError);
        }
    }
}
