package Checkers;

import AST.FileHandler;
import AST.Main;
import SingleParse.SQL.SqlStmtList.SqlStatement.MainSql.TableName;

public class TableTypeNameChecker {
    public static void check(TableName tableName) {
        if (tableName.getAllowedName() != null) {
            String typeToLookFor = tableName.getAllowedName().getName();
            boolean found = Main.symbolTable.doesTypeExist(typeToLookFor);
            if (!found) {
                String fileName = "errors.txt";
                String err = "Error on line " + tableName.getLine() + " on charAt " + tableName.getCol() + ", table " + typeToLookFor + " does not exist" + "\n";
                FileHandler.printToFile(fileName, err);
            }
        }
    }
}
