package CG.Helper;

import javafx.util.Pair;

import java.util.ArrayList;

public class ArrayListOfKeysFromArrayListOfPairsExtractor {
    public static ArrayList<String> extract(ArrayList<Pair<String, String>> ls) {
        ArrayList<String> res = new ArrayList<>();
        for(Pair<String, String> p: ls) {
            res.add(p.getValue());
        }

        return res;

    }
}
