package CG.Helper;

public class SQLTypeToJavaTypeHandler {
    public static String getJavaType(String sqlType) {
        switch (sqlType.toLowerCase()) {
            case "number":
                return "Double";
            case "string":
                return "String";
            case "boolean":
                return "boolean";
            case "list":
                return "List";
            default:
                return "Object";
        }
    }
}
