package CG.Helper;

import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ReflectionHandler {
    public static boolean hasPrimitiveType(Class<?> type) {
        return (type.equals(double.class) ||
                type.equals(String.class) ||
                type.equals(boolean.class) ||
                type.equals(Boolean.class));
    }
    private static List<String> getFieldNamesForClass(Class<?> clazz) throws Exception {
        List<String> fieldNames = new ArrayList<String>();
        Field[] fields = clazz.getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            fieldNames.add(fields[i].getName());
        }
        return fieldNames;
    }
    private static String getT(String prop) {
        return "get" + StringUtils.capitalize(prop) + "()";
    }

    public static String propertyGetter(Object bean, String prop, String base) throws Exception {
        Class<?> someClass = bean.getClass();

        ArrayList<String> fieldsNames = (ArrayList<String>) getFieldNamesForClass(someClass);
        if(fieldsNames.contains(prop)) {
            return base + getT(prop);
        }
        else {
            for(Field field : someClass.getDeclaredFields()) {
                int modifiers = field.getModifiers();
                if (!Modifier.isStatic(modifiers) && !Modifier.isFinal(modifiers)) {
                    if (!hasPrimitiveType(field.getType())) {
                        Class c;
                        c = Class.forName(field.getType().getName());
                        Object o = c.newInstance();
                       return propertyGetter(o, prop, base + getT(field.getName()) + '.');
                    }
                }
            }
        }
        return base;
    }
    public static void populateBean(Object bean, Map<String, String> properties) throws Exception {
        Class<?> clazz = bean.getClass();
        if (clazz != null) {
            for (Field field : clazz.getDeclaredFields()) {
                int modifiers = field.getModifiers();
                if (!Modifier.isStatic(modifiers) && !Modifier.isFinal(modifiers)) {
                    Class<?> t = field.getType();
                    if (hasPrimitiveType(t)) {
                        if (properties.containsKey(field.getName())) {
                            field.setAccessible(true);
                            if (field.getType().equals(double.class)) {
                                field.set(bean, Double.parseDouble(properties.get(field.getName())));
                            }
                            if (field.getType().equals(String.class)) {
                                field.set(bean, properties.get(field.getName()));
                            }
                            if (field.getType().equals(boolean.class)) {
                                field.set(bean, Boolean.parseBoolean(properties.get(field.getName())));
                            }
                        }
                    } else {
                        Class c = Class.forName(field.getType().getName());;
                        Object o = c.newInstance();
                        populateBean(o, properties);
                        field.setAccessible(true);
                       field.set(bean, o);
                    }
                }
            }
            // clazz = clazz.getSuperclass();
        }
    }
}
