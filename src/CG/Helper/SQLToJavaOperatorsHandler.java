package CG.Helper;

public class SQLToJavaOperatorsHandler {
    public static String getJavaOperator(String sqlOperator) {
        System.out.println("In switch statement: " + sqlOperator.toLowerCase());
        switch (sqlOperator.toLowerCase()) {
            case ">":
            case "<":
            case "<=":
            case ">=":
            case "!=":
                return sqlOperator;
            case "=":

            case "is":
                return "==";
            case "<>":
            case "is not":
            case "isnot":
            case "not in":
            case "notin":
                return "!=";
            case "and":
                return "&&";
            case "or":
                return "||";
            case "like":
                return "contains";
            default:
                return "";
        }
    }
}
