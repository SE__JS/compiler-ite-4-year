package CG.Helper;

import AST.Main;
import SymbolTable.Type;

public class TableDetector {
    public static boolean isTable(String type) {
        if(!Type.hasComplexType(type)) {
            return false;
        }
        Type t = Main.symbolTable.getTypeByName(type);
        if(t != null) {
            return t.isTable;
        }
        return false;
    }
}
