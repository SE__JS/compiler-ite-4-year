package CG.Helper;

import CG.Templates.Core.TypeTemplate;
import SymbolTable.Type;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Map;

public class DataMembersFromTypeGetter {
    public static ArrayList<Pair<String, String>> get(Type t) {
        ArrayList<Pair<String, String>> dataMembers = new ArrayList<>();
        for(Map.Entry<String, Type> entry: t.getColumns().entrySet()) {
            String[] dataMembersStr = entry.getKey().split("\\.");
            boolean isTable = TableDetector.isTable(dataMembersStr[0]);
            String arrType = "";
            if (isTable) {
                arrType = TypeTemplate.getPrimitiveType(entry.getValue().getName()) + "[]";
                dataMembers.add(new Pair<>(arrType, dataMembersStr[dataMembersStr.length - 1]));
            } else {
                dataMembers.add(new Pair<>(entry.getValue().getName(), dataMembersStr[dataMembersStr.length - 1]));
            }
        }
        return dataMembers;
    }
}
