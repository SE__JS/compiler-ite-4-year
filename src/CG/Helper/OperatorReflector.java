package CG.Helper;

public class OperatorReflector {
    public static String reflect(String operator) {
        switch (operator) {
            case "<":
                return ">=";

            case "<=":
                return ">";

            case ">":
                return "<=";
            case ">=":
                return "<";

            case "=":
            case "==":
                return "!=";


            case "!=":
            case "<>":
                return "==";

            default:
                return operator;
        }
    }
}
