package CG.Helper;

import AST.Main;
import SymbolTable.Type;

import java.util.ArrayList;
import java.util.Map;

public class ColumnsNameFromTypeExtractor {
    public static ArrayList<String> getColumnsNames(Type t) {
        ArrayList<String> ans = new ArrayList<>();
        for(Map.Entry<String, Type> e: t.getColumns().entrySet()) {
            ans.add(e.getKey());
        }
        return ans;
    }
}
