package CG.Templates.SQL;

import CG.Helper.OperatorReflector;
import CG.Helper.SQLToJavaOperatorsHandler;
import CG.Mappers.SQLAGGTOJAVACONVERTER;
import SingleParse.Expression.SingleExpression;
import com.sun.xml.internal.ws.util.StringUtils;

public class SingleExpressionTemplate {
    public static String getSingleExpressionTemplate(String tableName, SingleExpression singleExpression, String blueprint) {
        String l = singleExpression.getLeftSide();

        if (l.startsWith(tableName)) {
            l = l.substring(tableName.length() + 1);
        }
        String[] slicedName = l.split("\\.");


        if (singleExpression.getOperatorInBetween().equals("agg_f")) {
            String javaM = SQLAGGTOJAVACONVERTER.getMethodName(singleExpression.getLeftSide());
            javaM = StringUtils.capitalize(javaM);
            String r = singleExpression.getRightSide();
            r = StringUtils.capitalize(r);
            String reflectedOperator = OperatorReflector.reflect(singleExpression.getAggOperator());
            return "double v = entry\n" +
                    "                    .getValue()\n" +
                    "                    .stream()\n" +
                    "                    .map(e -> e.get" + r + "())\n" +
                    "                    .mapToDouble(Double::doubleValue)\n" +
                    "                    .summaryStatistics().get" + javaM + "();\n" +
                    "\n" +
                    "            if((v " + reflectedOperator + " " + singleExpression.getAggValue() + ")) {\n " +
                    "                entry.setValue(new ArrayList<>());\n" +
                    "            };";
        }


        ChainedGetterTemplate.getTemplate(slicedName, blueprint);
        l = ChainedGetterTemplate.getTemplate(slicedName, blueprint);
        String r = singleExpression.getRightSide();

        String opr = SQLToJavaOperatorsHandler.getJavaOperator(singleExpression.getOperatorInBetween());
        r = r.replace("'", "\"");

        if (r.startsWith("\"") || r.startsWith("'")) {
            if (singleExpression.getOperatorInBetween().toLowerCase().equals("like")) {
                if (r.startsWith("\"%")) {
                    opr = ".startsWith";
                    r = r.replace("%", "");
                    r = opr.replace(" ", "") + "(" + r + ")";
                } else if (r.endsWith("%\"") || r.endsWith("%'")) {
                    opr = ".endsWith";
                    r = r.replace("%", "");
                    r = opr.replace(" ", "") + "(" + r + ")";
                } else {
                    r = r.replace("%", "");
                    r = '.' + opr.replace(" ", "") + "(" + r + ")";
                }
            } else if (singleExpression.getOperatorInBetween().toLowerCase().equals("=")) {
                opr = ".equals";
                r = opr.replace(" ", "") + "(" + r + ")";
            }
        } else {
            System.out.println("RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR: " + r);
            try {
                r = opr + " " + Double.parseDouble(r);
            } catch (Exception e) {
                r = opr + " " + blueprint + ".get" + StringUtils.capitalize(r) + "()";
            }
        }

        return "( " + l + " " + r + ")";
    }


    public static String getSingleExpressionTemplateForInnerJoin(SingleExpression singleExpression, String
            leftBlueprint, String rightBlueprint) {
        String l = singleExpression.getLeftSide();


        String[] slicedName = l.split("\\.");

        if (slicedName[0].equals(rightBlueprint)) {
            String temp = leftBlueprint;
            leftBlueprint = rightBlueprint;
            rightBlueprint = temp;
        }

        String[] needed = new String[slicedName.length - 1];
        for (int i = 1; i < slicedName.length; i++) {
            needed[i - 1] = slicedName[i];
            System.out.println("needed: " + needed[i - 1]);
        }

        ChainedGetterTemplate.getTemplate(needed, leftBlueprint);
        l = ChainedGetterTemplate.getTemplate(needed, leftBlueprint);
        String r = singleExpression.getRightSide();


        System.out.println("before rrrrrrrrrrr: " + r);
        String[] ttt = r.split("\\.");
        r = ttt[ttt.length - 1];
        System.out.println("after rrrrrrrrrrr: " + r);

        String amazing = singleExpression.getOperatorInBetween();
        System.out.println("AMAZINGGGGGGGGGGGGGGGGG: " + amazing);
        String opr = SQLToJavaOperatorsHandler.getJavaOperator(amazing);

        if (r.startsWith("\"") || r.startsWith("'")) {
            if (singleExpression.getOperatorInBetween().toLowerCase().equals("like")) {
                if (r.startsWith("\"%")) {
                    opr = ".startsWith";
                    r = r.replace("%", "");
                    r = opr.replace(" ", "") + "(" + r + ")";
                } else if (r.endsWith("%\"") || r.endsWith("%'")) {
                    opr = ".endsWith";
                    r = r.replace("%", "");
                    r = opr.replace(" ", "") + "(" + r + ")";
                } else {
                    r = r.replace("%", "");
                    r = '.' + opr.replace(" ", "") + "(" + r + ")";
                }
            } else if (singleExpression.getOperatorInBetween().toLowerCase().equals("=")) {
                opr = ".equals";
                r = opr.replace(" ", "") + "(" + r + ")";
            }
        } else {
            try {
                r = opr + " " + Double.parseDouble(r);
            } catch (Exception e) {
                r = opr + " " + rightBlueprint + ".get" + StringUtils.capitalize(r) + "()";
            }
        }

        return "( " + l + " " + r + ")";
    }
}
