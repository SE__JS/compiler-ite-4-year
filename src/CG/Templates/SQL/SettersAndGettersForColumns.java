package CG.Templates.SQL;

import CG.Helper.TableDetector;
import CG.Templates.Core.TypeTemplate;
import com.sun.xml.internal.ws.util.StringUtils;
import javafx.util.Pair;

import java.util.ArrayList;

public class SettersAndGettersForColumns {
    public static String generateSG(String objName,
                                    ArrayList<String> cols,
                                    String bluePrint) {
        StringBuilder setters = new StringBuilder();
        for (String s : cols) {
            String[] slicedName = s.split("\\.");
            String last = StringUtils.capitalize(slicedName[slicedName.length - 1]);
            String getter = ChainedGetterTemplate.getTemplate(slicedName, bluePrint);
            String setter = objName + ".set" + last + "(" + getter + ");\n";
            setters.append(setter);

        }
        return setters.toString();
    }

    public static String generateSettersAndGettersForAllColumns(String objName,
                                                                ArrayList<Pair<String, String>> cols,
                                                                String blueprint) {
        StringBuilder setters = new StringBuilder();
        for (Pair<String, String> aPair : cols) {
            String p = aPair.getValue();
            String[] slicedName = p.split("\\.");
            String base = slicedName[0];
            String last = StringUtils.capitalize(slicedName[slicedName.length - 1]);
            if (TableDetector.isTable(base)) {
                String sqlType = aPair.getKey().replace("[]", "");
                sqlType = TypeTemplate.getPrimitiveType(sqlType);
                String varName = "u_" + base + "_" + last;
                String arrayName = base + "_" + last + "_dummyArray";
                String[] neededChainedGetter = new String[slicedName.length - 1];
                for (int i = 1; i < slicedName.length; i++) {
                    neededChainedGetter[i - 1] = slicedName[i];
                    System.out.println("neededChainedGetter[" + (i - 1) + "]: " + neededChainedGetter[i - 1]);

                }

                String getter = ChainedGetterTemplate.getTemplate(neededChainedGetter, "dummy");


                String forT = sqlType + "[]" + arrayName + "= new " + sqlType + "[" + blueprint + ".get" + StringUtils.capitalize(base) + "().length" + "];\n" +
                        "int " + varName + "  = 0;" +
                        "for(" + base + " dummy: " + blueprint + ".get" + StringUtils.capitalize(base) + "()) {\n" +
                        arrayName + "[" + varName + "++] = " + getter + ";\n" +
                        "}\n" +
                        objName + ".set" + last + "(" + arrayName + ");\n";
                setters.append(forT);
            } else {
                String getterT = ChainedGetterTemplate.getTemplate(slicedName, blueprint);
                String aSetter = objName + ".set" + StringUtils.capitalize(slicedName[slicedName.length - 1]) + "(" + getterT + ");\n";
                setters.append(aSetter);
            }

        }

        return setters.toString();
    }
}
