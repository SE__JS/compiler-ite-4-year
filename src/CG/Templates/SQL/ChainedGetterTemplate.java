package CG.Templates.SQL;

import com.sun.xml.internal.ws.util.StringUtils;

public class ChainedGetterTemplate {
    public static String getTemplate(String[] chainedName, String fromObject) {
        int sz = chainedName.length;
        StringBuilder aGetter = new StringBuilder(fromObject + ".get");
        for(int i = 0; i < chainedName.length; i++) {
            if(i + 1 == sz) {
                aGetter.append(StringUtils.capitalize(chainedName[i])).append("()");
            }
            else {
                aGetter.append(StringUtils.capitalize(chainedName[i])).append("().get");
            }
        }

        return aGetter.toString();
    }
}
