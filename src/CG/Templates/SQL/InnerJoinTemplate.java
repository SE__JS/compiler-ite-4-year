package CG.Templates.SQL;

import AST.Main;
import CG.Templates.Core.CreatingObjectTemplate;
import SingleParse.Expression.SingleExpression;
import SymbolTable.Type;

import java.util.ArrayList;

public class InnerJoinTemplate {
    public static String getTemplate(String Base1,
                                     String Base2,
                                     String blueprint,
                                     ArrayList<String> cols1,
                                     ArrayList<String> cols2,
                                     ArrayList<SingleExpression> singleExpressionArrayList,
                                     ArrayList<String> oprs) {
        String twoT = "\t\t";
        String fourT = twoT + twoT;


        Type tt1 = Main.symbolTable.getTypeByName(Base1);
        if (tt1 == null) {
            tt1 = Main.symbolTable.getTypeFromProvidedSymbols(Base1);
            if (tt1 == null) {
            } else {
                Base1 = tt1.getName();
            }
        } else {
            Base1 = tt1.getName();
        }


        Type tt2 = Main.symbolTable.getTypeByName(Base2);
        if (tt2 == null) {
            tt2 = Main.symbolTable.getTypeFromProvidedSymbols(Base2);
            if (tt2 == null) {
            } else {
                Base2 = tt2.getName();
            }
        } else {
            Base2 = tt2.getName();
        }


        String objectName = blueprint.substring(0, blueprint.length() > 1 ? blueprint.length() / 2 : blueprint.length());
        String constructTable = CreatingObjectTemplate.getCreatingObjectTemplate(blueprint, objectName);


        String firstArrayList = Base1 + '.' + Base1 + "sArrayList";
        String secondArrayList = Base2 + '.' + Base2 + "sArrayList";

        // String sg1 = SettersAndGettersForColumns.generateSG(objectName, cols1, "b1");
        String sg2 = SettersAndGettersForColumns.generateSG(objectName, cols2, "b2");

        System.out.println("IN: " + singleExpressionArrayList);
        WhereTemplate.WhereInstance whereT = WhereTemplate.getFilterForJoin(Base1, Base2, singleExpressionArrayList, oprs, "f", "s");


//        return "List<" + Base1 + "> joiningList = " + firstArrayList + ".stream()\n" +
//                "                    .filter(f -> " + secondArrayList + ".stream()\n" +
//                "                            .anyMatch(s -> f.getFacultyName().equals(s.getName())))\n" +
//                "                    .collect(Collectors.toList());";

        if(whereT.condition == null || whereT.condition.equals("")) {
            return "";
        }
        String resultName = Base1 + "_joining_" + Base2 + "_list";
        return "List<" + Base1 + ">" + resultName + " = " + firstArrayList + ".stream()\n" +
                "                    .filter(f -> " + secondArrayList + ".stream()\n" +
                "                            .anyMatch(s -> " + whereT.condition + "))\n" +
                "                    .collect(Collectors.toList());\n" +
                "" +
                firstArrayList + " = (ArrayList<" + Base1 + ">) " + resultName + ";";

    }
}
