package CG.Templates.SQL;


import CG.Helper.SQLTypeToJavaTypeHandler;
import SingleParse.SQL.SqlStmtList.SqlStatement.AggregationFunction.AggFunctionBluePrint;

public class AggregationFunctionTemplate {

    public static String getTemplate(AggFunctionBluePrint bluePrint) {

        StringBuilder template = new StringBuilder();

        String methodHeader = "public static Object " + bluePrint.getAggregationFunctionName() + "(ArrayList<?> values) throws Exception {\n";
        template.append(methodHeader);


        StringBuilder paramsTypesArray = new StringBuilder();

        int i = 0;
        for (String sqlType : bluePrint.getArgs()) {
            if (i + 1 == bluePrint.getArgs().size()) {
                paramsTypesArray.append(SQLTypeToJavaTypeHandler.getJavaType(sqlType)).append(".class");
            }
            else paramsTypesArray.append(SQLTypeToJavaTypeHandler.getJavaType(sqlType)).append(".class, ");
            i++;
        }

        String JarPath = "String JarPath = " + "\"" + bluePrint.getJarPath() + "\"" + ";\n";
        String ClassName = "String ClassName = " + "\"" + bluePrint.getClassName() + "\"" + ";\n";
        String MethodName = "String MethodName = " + "\"" + bluePrint.getMethodName().toUpperCase() + "\"" + ";\n";

        template.append(JarPath);
        template.append(ClassName);
        template.append(MethodName);


        String classLoader = "URLClassLoader myClassLoader = new URLClassLoader(new URL[]{new File(JarPath).toURI().toURL()}, Main.class.getClassLoader());\n";
        template.append(classLoader);

        // Loading the class
        String classInstance = "Class<?> myClass = Class.forName(ClassName, true, myClassLoader);\n";
        template.append(classInstance);


        // Getting instance
        String singleton = "Method mySingeltonGetterMethod = myClass.getMethod(\"get\" + ClassName, null);\n";
        template.append(singleton);


        String obj = "Object myObject = mySingeltonGetterMethod.invoke(null);\n";
        template.append(obj);

        String print = "\t\t// for(Method m : myClass.getDeclaredMethods())\n\t\t// System.out.println (m);\n";
        template.append(print);

        // Calling method
        String var = "\nreturn myObject.getClass().getDeclaredMethod(MethodName, " + paramsTypesArray.toString() + ").invoke(myObject, values);\n";
        template.append(var);


        template.append("\n}");

        return template.toString();

    }
}
