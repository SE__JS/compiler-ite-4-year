package CG.Templates.SQL;

import CG.Helper.SQLToJavaOperatorsHandler;
import SingleParse.Expression.SingleExpression;

import java.util.ArrayList;

public class WhereTemplate {
    public static class WhereInstance {
        public String condition;
        public String fullWhereTemplate;
        public boolean aggFunctionInHaving;
    }

    public static WhereTemplate.WhereInstance getWhereTemplate(
            String tableName,
            ArrayList<SingleExpression> ses,
            ArrayList<String> oprs,
            String leftBluePrint,
            String rightBluePrint) {

        WhereTemplate.WhereInstance instance = new WhereInstance();

        if ((ses == null || oprs == null)) return instance;
        if (ses.size() == 0 && oprs.size() == 0) {
            return instance;
        }


        StringBuilder whereTemplate = new StringBuilder();
        int i = 0;
        boolean ok = false;
        for (i = 0; i < ses.size() - 1; i += 2) {
            SingleExpression l = ses.get(i);
            SingleExpression r = ses.get(i + 1);
            String opr = SQLToJavaOperatorsHandler.getJavaOperator(oprs.get(i));


            // [ <, ==, && ]
            // [age, 2, id, 5, mark, 6 ]
            // age < 2
            String lTemplate = SingleExpressionTemplate.getSingleExpressionTemplate(tableName, l, leftBluePrint);

            String rTemplate = SingleExpressionTemplate.getSingleExpressionTemplate(tableName, r, rightBluePrint);

            whereTemplate.append(lTemplate);
            whereTemplate.append(opr);
            whereTemplate.append(rTemplate);

            if (i + 1 < oprs.size() && (i + 1 % 2 != 0)) {
                opr = SQLToJavaOperatorsHandler.getJavaOperator(oprs.get(i + 1));
                whereTemplate.append(opr);
                ok = false;
            } else {
                ok = true;
            }
        }

        if (!ok) {
            SingleExpression s = ses.get(ses.size() - 1);
            System.out.println("Not ok: ");
            System.out.println(s);

            String sTemplate = SingleExpressionTemplate.getSingleExpressionTemplate(tableName, s, rightBluePrint);
            if(sTemplate.startsWith("double")) {
                instance.aggFunctionInHaving = true;
            }
            whereTemplate.append(sTemplate);
        }

        instance.fullWhereTemplate = "if (" + whereTemplate.toString() + ")\n";
        instance.condition = whereTemplate.toString();
        return instance;
    }


    public static WhereTemplate.WhereInstance getFilterForJoin(
            String Base1,
            String Base2,
            ArrayList<SingleExpression> ses,
            ArrayList<String> oprs,
            String leftBluePrint,
            String rightBluePrint) {


        WhereTemplate.WhereInstance instance = new WhereInstance();

        if ((ses == null || oprs == null)) return instance;
        if (ses.size() == 0 && oprs.size() == 0) {
            return instance;
        }


        StringBuilder whereTemplate = new StringBuilder();
        int i = 0;
        boolean ok = false;
        for (i = 0; i < ses.size() - 1; i += 2) {
            SingleExpression l = ses.get(i);
            SingleExpression r = ses.get(i + 1);
            String opr = SQLToJavaOperatorsHandler.getJavaOperator(oprs.get(i));


            // [ <, ==, && ]
            // [age, 2, id, 5, mark, 6 ]
            // age < 2
            String lTemplate = SingleExpressionTemplate.getSingleExpressionTemplate(Base1, l, leftBluePrint);

            String rTemplate = SingleExpressionTemplate.getSingleExpressionTemplate(Base1, r, rightBluePrint);

            whereTemplate.append(lTemplate);
            whereTemplate.append(opr);
            whereTemplate.append(rTemplate);

            if (i + 1 < oprs.size() && (i + 1 % 2 != 0)) {
                opr = SQLToJavaOperatorsHandler.getJavaOperator(oprs.get(i + 1));
                whereTemplate.append(opr);
                ok = false;
            } else {
                ok = true;
            }
        }

        if (!ok) {
            SingleExpression s = ses.get(ses.size() - 1);
            String sTemplate = SingleExpressionTemplate.getSingleExpressionTemplateForInnerJoin(s, leftBluePrint, rightBluePrint);
            System.out.println("sTemplate: " + sTemplate);
            whereTemplate.append(sTemplate);
        }

        instance.fullWhereTemplate = "if (" + whereTemplate.toString() + ")\n";
        instance.condition = whereTemplate.toString();
        return instance;
    }
}
