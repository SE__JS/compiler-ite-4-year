package CG.Templates.SQL;

import CG.Helper.ArrayListOfKeysFromArrayListOfPairsExtractor;
import CG.Mappers.AggregationFunctionMapper;
import CG.Mappers.GroupByMapper;
import CG.Templates.Core.CreatingObjectTemplate;
import CG.Templates.Core.MainMethodTemplate;
import javafx.util.Pair;

import java.util.ArrayList;

public class AssigningSelectResultToVariableTemplate {

    public static String getTemplate(String mainType,
                                     String subType,
                                     ArrayList<Pair<String, String>> cols,
                                     WhereTemplate.WhereInstance whereT,
                                     AggregationFunctionMapper.AGG_FUNC_MAPPER agg_func_mapper,
                                     GroupByMapper.GroupByInstance groupByMapper
    ) {
        String oneT = "\t";
        String twoT = "\t\t";
        String threeT = oneT + twoT;
        String fourT = "\t\t\t\t";
        String eightT = fourT + fourT;
        // Creating kinda randomized name for object to be constructed with
        String objectName = subType.substring(0, subType.length() > 1 ? subType.length() / 2 : subType.length());
        String constructTable = CreatingObjectTemplate.getCreatingObjectTemplate(subType, objectName);



        // generate all setters and getters for all columns presented in query
        String sg = SettersAndGettersForColumns.generateSettersAndGettersForAllColumns(objectName, cols, "ele");

        MainMethodTemplate.StaticMainClassBody.append(groupByMapper.groupByOne != null ? groupByMapper.groupByOne : "\n");
        MainMethodTemplate.StaticMainClassBody.append(groupByMapper.groupByTwo != null ? groupByMapper.groupByTwo : "\n");

        String printingTheMap = groupByMapper.mapName  + ".forEach((key, value) -> {\n" +
                "            System.out.println(\"====================================\");\n" +
                "            System.out.println(\"Key: \" + key);\n" +
                "            System.out.println(\"Value: \" + value);\n" +
                "            System.out.println(\"====================================\");\n" +
                "        });\n";

        String printingElement = "System.out.println(\"====================================\");\n" +
                "            System.out.println(\"Key which grouped by: \" + grMainType.getKey());\n";

        if (groupByMapper.groupByTwo != null || groupByMapper.groupByOne != null) {
            return "\n" +
                    groupByMapper.mapDeclaration +
                    groupByMapper.havingPredicate +
                    printingTheMap +
                    twoT + "for(Map.Entry<String, List<" + mainType + ">> grMainType: " + groupByMapper.mapName + ".entrySet()) {\n" +
                    printingElement +
                    agg_func_mapper.arrayListForAggFunc + "\n" +
                    fourT + subType + " " + objectName + " = null;\n" +
                    threeT + "for(" + mainType + " ele: grMainType.getValue()) {\n" +
                    fourT + objectName + " = new " + subType + "();\n" +
                    fourT + sg +
                    fourT + subType + "." + subType + "sArrayList.add(" + objectName + ");\n" +
                    agg_func_mapper.applyingDataToArrayList +
                    threeT + "\n}\n" +
                    agg_func_mapper.tcStatements +
                    twoT + "\n}\n";

        } else {
            String c = whereT != null && whereT.fullWhereTemplate != null ? whereT.fullWhereTemplate : "\n";
            return "\n" + agg_func_mapper.arrayListForAggFunc + "\n" +
                    twoT + "for(" + mainType + " ele: " + mainType + "." + mainType + "sArrayList) {\n" +
                    threeT + c +
                    threeT + "{\n" +
                    // fourT + groupByMapper.body +
                    fourT + constructTable +
                    fourT + sg +
                    fourT + subType + "." + subType + "sArrayList.add(" + objectName + ");\n" +
                    agg_func_mapper.applyingDataToArrayList +
                    threeT + "\n}\n" +
                    twoT + "}\n" +
                    agg_func_mapper.tcStatements;
        }
    }
}
