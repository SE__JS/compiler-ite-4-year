package CG.Templates.FileParsers;


public class CSVParserTemplate {

    public static String parseCSVFileTemplate(String className) {
        return "Path path = Paths.get(filePath + \".\" + fileType);\n" +
                "\t\t\tBufferedReader reader = Files.newBufferedReader(path);\n" +
                "\t\t\tIterable<CSVRecord> records = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(reader);\n" +
                "\t\t\tfor (CSVRecord record : records) {\n" +
                "\t\t\t\t" + className + " aClass = new " + className + "();\n" +
                "\t\t\t\tReflectionHandler.populateBean(aClass, record.toMap());\n" +
                "\t\t\t\t" + className + "sArrayList.add(aClass);\n" +
                "\n\t\t\t}\n";
    }

}
