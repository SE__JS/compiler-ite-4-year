package CG.Templates.FileParsers;
public class LoadDataMethodTemplate {
    public static String getLoadDataMethodTemplate(String accessModifier,
                                                   String typeToBeParsedAccordingly) {


        String jsonParserTemplate = JsonParserTemplate.parseJsonFromFileTemplate(typeToBeParsedAccordingly);
        String csvParserTemplate = CSVParserTemplate.parseCSVFileTemplate(typeToBeParsedAccordingly);

        return "\n\n\t" + accessModifier + " static void loadDataFromFile(String filePath, String fileType) throws Exception {\n" +
                "\t\tif (fileType.toLowerCase().equals(\"json\")) {\n" +
                "\t\t\t" + jsonParserTemplate + "\n" + // if body
                "\t\t}" +

                " else if (fileType.toLowerCase().equals(\"csv\")) {\n" +
                "\t\t\t" + csvParserTemplate + "\n" + // else if body
                "\t\t}" +

                " else {\n" +
                "\t\t\tthrow new Exception(\"File's type is in-correct\");\n" + // else body
                "\t\t}\n" +

                "\t}\n\n";
    }
}
