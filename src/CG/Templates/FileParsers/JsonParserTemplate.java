package CG.Templates.FileParsers;

public class JsonParserTemplate {
    public static String parseJsonFromFileTemplate(String type) {
        return "Gson gson = new Gson();\n" +
                "\t\t\tJsonReader reader = new JsonReader(new FileReader(filePath + '.' + fileType));\n" +
                "\t\t\tJsonObject jsonObject = gson.fromJson(reader, JsonObject.class);\n"+
                "\t\t\tJsonElement jsonElement = jsonObject.get(\"" + type + "\");\n"+
                "\t\t\t" + type +"[] data = gson.fromJson(jsonElement, " + type + "[].class);\n" +
                "\t\t\t"+ type +"sArrayList = new ArrayList<>(Arrays.asList(data));\n";
    }
}
