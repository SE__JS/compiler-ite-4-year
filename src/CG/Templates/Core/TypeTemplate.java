package CG.Templates.Core;

public class TypeTemplate {
    public static String getPrimitiveType(String type) {
        if(type.toLowerCase().equals("number")) return "double";
        if(type.toLowerCase().equals("boolean")) return "boolean";
        if(type.toLowerCase().equals("string")) return "String";
        return type;
    }
}
