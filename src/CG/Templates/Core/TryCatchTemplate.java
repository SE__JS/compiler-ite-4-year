package CG.Templates.Core;

public class TryCatchTemplate {
    public static String getTryCatchTemplate(String tryBody) {
        String twoT = "\t\t";
        String threeT = twoT + "\t";

        return twoT + "try {\n" +
                threeT + tryBody +
                "\n" + twoT + "} catch (Exception e) {\n" +
                threeT + "System.out.println(e.getMessage());\n"
                + twoT + "}\n";
    }
}
