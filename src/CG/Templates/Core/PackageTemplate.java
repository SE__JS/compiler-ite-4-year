package CG.Templates.Core;

public class PackageTemplate {
    public static String getPackageTemplate(String packageName) {
        return "package " + packageName + ";\n\n\n";
    }
}
