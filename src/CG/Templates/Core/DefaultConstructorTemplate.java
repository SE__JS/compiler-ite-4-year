package CG.Templates.Core;

public class DefaultConstructorTemplate {
    public static String getDefaultConstructorTemplate(String accessModifier, String className) {
        return "\n\t" + accessModifier + " " + className + "() {\n" +
                "\t}\n\n";
    }
}
