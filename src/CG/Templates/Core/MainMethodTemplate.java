package CG.Templates.Core;

public class MainMethodTemplate {
    public static StringBuilder StaticMainBody = new StringBuilder();
    public static StringBuilder StaticMainClassBody = new StringBuilder();

    public static String getMainTemplate() {
        StringBuilder StaticMainTemplate = new StringBuilder();


        // Package name
        String packageTemplate = PackageTemplate.getPackageTemplate("CG.GeneratedClasses");
        StaticMainTemplate.append(packageTemplate);

        // Libraries
        String libraryName = ImportLibraryTemplate.getImportLibraryTemplate("java.util.*");
        StaticMainTemplate.append(libraryName);

        libraryName = ImportLibraryTemplate.getImportLibraryTemplate("SingleParse.SQL.SqlStmtList.SqlStatement.AggregationFunction.AggFunctionBluePrint");
        StaticMainTemplate.append(libraryName);

        libraryName = ImportLibraryTemplate.getImportLibraryTemplate("java.io.File");
        StaticMainTemplate.append(libraryName);

        libraryName = ImportLibraryTemplate.getImportLibraryTemplate("java.lang.reflect.Method");
        StaticMainTemplate.append(libraryName);

        libraryName = ImportLibraryTemplate.getImportLibraryTemplate("java.net.URL");
        StaticMainTemplate.append(libraryName);

        libraryName = ImportLibraryTemplate.getImportLibraryTemplate("java.net.URLClassLoader");
        StaticMainTemplate.append(libraryName);

        libraryName = ImportLibraryTemplate.getImportLibraryTemplate("java.util.stream.Collectors");
        StaticMainTemplate.append(libraryName);

        // Class header
        // ex: public class someClass {
        String header = "public class Main {\n";
        StaticMainTemplate.append(header);


        String mainMethod = "\tpublic static void main(String[] args) {\n" +
                StaticMainBody.toString() +
                "\n\t}\n";

        StaticMainTemplate.append(mainMethod);

        StaticMainTemplate.append(PrintArrayListTemplate.getPrintingArrayListTemplate());

        StaticMainTemplate.append(StaticMainClassBody.toString());

        StaticMainTemplate.append("\n}\n");
        return StaticMainTemplate.toString();
    }
}
