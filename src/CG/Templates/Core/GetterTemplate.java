package CG.Templates.Core;

import com.sun.xml.internal.ws.util.StringUtils;

public class GetterTemplate {
    public static String getGetterTemplate(String accessModifier, String dataMember, String type) {
        return "\t"+accessModifier + " " + type + " " + "get" + StringUtils.capitalize(dataMember) + "() {\n"
                + "\t\treturn this." + dataMember + ";"
                +"\n\t}\n\n";
    }
}
