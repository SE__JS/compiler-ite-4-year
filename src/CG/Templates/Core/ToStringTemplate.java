package CG.Templates.Core;

import java.util.ArrayList;

public class ToStringTemplate {
    public static String getToStringTemplate(ArrayList<String> args) {
        StringBuilder toStringBuilder = new StringBuilder();
        String header = "\t@Override public String toString() {\n";
        toStringBuilder.append(header);
        toStringBuilder.append("\t\treturn ");
        toStringBuilder.append(" \"{ \" + ");
        for (int i = 0; i < args.size(); i++) {
            if(i + 1 == args.size()) {
                toStringBuilder.append("\"").append(args.get(i)).append(": \" + ").append("this.").append(args.get(i));
                toStringBuilder.append(" + ");
            } else {
                toStringBuilder.append("\"").append(args.get(i)).append(": \" + ").append("this.").append(args.get(i));
                toStringBuilder.append(" + \", \" + ");
            }

        }
        toStringBuilder.append(" \" } \";");

        toStringBuilder.append("\n\t}\n");
        return toStringBuilder.toString();
    }

}
