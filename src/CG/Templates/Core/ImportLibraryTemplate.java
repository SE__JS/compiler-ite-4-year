package CG.Templates.Core;

public class ImportLibraryTemplate {
    public static String getImportLibraryTemplate(String libraryName) {
        return "import " + libraryName + ";\n\n";
    }
}
