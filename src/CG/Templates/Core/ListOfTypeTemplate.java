package CG.Templates.Core;

public class ListOfTypeTemplate {
    public static String ArrayListTemplate(String type) {
        return "\tpublic static ArrayList<" + type + "> " + type + "sArrayList = new ArrayList<>();\n\n";
    }
}
