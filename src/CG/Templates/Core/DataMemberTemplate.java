package CG.Templates.Core;

public class DataMemberTemplate {
    public static String getDataMemberTemplate(String accessModifier, String dataMember, String type, String defaultValue) {
        if(defaultValue!= null) {
            return "\t" + accessModifier + " " + type + " " + dataMember + " = " + defaultValue + ";\n";
        }
        return "\t" + accessModifier + " " + type + " " + dataMember + ";\n";
    }
}
