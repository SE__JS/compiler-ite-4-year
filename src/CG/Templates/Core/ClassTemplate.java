package CG.Templates.Core;

import AST.Main;
import CG.Helper.TableDetector;
import CG.Templates.FileParsers.LoadDataMethodTemplate;
import SymbolTable.SymbolTable;
import SymbolTable.Type;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class ClassTemplate {

    public static String getClassTemplate(String accessModifier,
                                          String className,
                                          ArrayList<Pair<String, String>> dataMembers,
                                          boolean isClass
    ) {
        StringBuilder setters = new StringBuilder();
        StringBuilder getters = new StringBuilder();
        StringBuilder classDataMembers = new StringBuilder();
        StringBuilder classTemplate = new StringBuilder();

        // Package name
        String packageTemplate = PackageTemplate.getPackageTemplate("CG.GeneratedClasses");
        classTemplate.append(packageTemplate);


        // Libraries names
        String libraryName = ImportLibraryTemplate.getImportLibraryTemplate("java.util.ArrayList");
        classTemplate.append(libraryName);

        String gson = ImportLibraryTemplate.getImportLibraryTemplate("com.google.gson.Gson");
        classTemplate.append(gson);

        String gsonReader = ImportLibraryTemplate.getImportLibraryTemplate("com.google.gson.stream.JsonReader");
        classTemplate.append(gsonReader);


        String ioLibrary = ImportLibraryTemplate.getImportLibraryTemplate("java.io.*");
        classTemplate.append(ioLibrary);

        String arrayListLibrary = ImportLibraryTemplate.getImportLibraryTemplate("java.util.Arrays");
        classTemplate.append(arrayListLibrary);

        String filesLibrary = ImportLibraryTemplate.getImportLibraryTemplate("java.nio.file.Files");
        classTemplate.append(filesLibrary);

        String PathLibrary = ImportLibraryTemplate.getImportLibraryTemplate("java.nio.file.Path");
        classTemplate.append(PathLibrary);

        String PathsLib = ImportLibraryTemplate.getImportLibraryTemplate("java.nio.file.Paths");
        classTemplate.append(PathsLib);

        String CSVFormatLib = ImportLibraryTemplate.getImportLibraryTemplate("org.apache.commons.csv.CSVFormat");
        classTemplate.append(CSVFormatLib);


        String CSVRecordLib = ImportLibraryTemplate.getImportLibraryTemplate("org.apache.commons.csv.CSVRecord");
        classTemplate.append(CSVRecordLib);

        String StringUtilsLib = ImportLibraryTemplate.getImportLibraryTemplate("com.sun.xml.internal.ws.util.StringUtils");
        classTemplate.append(StringUtilsLib);

        String MapLib = ImportLibraryTemplate.getImportLibraryTemplate("java.util.Map");
        classTemplate.append(MapLib);

        String mthRefLib = ImportLibraryTemplate.getImportLibraryTemplate("java.lang.reflect.Method");
        classTemplate.append(mthRefLib);


        String ReflectionHandlerLib = ImportLibraryTemplate.getImportLibraryTemplate("CG.Helper.ReflectionHandler");
        classTemplate.append(ReflectionHandlerLib);


        String JsonElementLib = ImportLibraryTemplate.getImportLibraryTemplate("com.google.gson.JsonElement");
        classTemplate.append(JsonElementLib);


        String JsonObjectLib = ImportLibraryTemplate.getImportLibraryTemplate("com.google.gson.JsonObject");
        classTemplate.append(JsonObjectLib);


        // Class header
        // ex: public class someClass {
        String header = accessModifier + " class " + className + " {\n";
        classTemplate.append(header);


        // Creating list of the same class type
        // so we can load a list of data to this class
        String listOfTheSameType = ListOfTypeTemplate.ArrayListTemplate(className);
        classTemplate.append(listOfTheSameType);


        // IS TABLE DATA MEMBER
        String isTableTemplate = DataMemberTemplate.getDataMemberTemplate("public", "isTable", "boolean", isClass ? "true" : "false");
        // String isTableSetter = SetterTemplate.getSetterTemplate("public", "isTable", "boolean");
        // String isTableGetter = GetterTemplate.getGetterTemplate("public", "isTable", "boolean");
        // setters.append(isTableSetter);
        // getters.append(isTableGetter);
        classDataMembers.append(isTableTemplate);



        // data members, setters & getters
        for (Pair<String, String> member : dataMembers) {
            String type = TypeTemplate.getPrimitiveType(member.getKey());

            // This is for passing table inside a table
            if(TableDetector.isTable(member.getKey())) {
                type = type + "[]";
            }

            String varName = member.getValue();
            String dataMemberTemplate = DataMemberTemplate.getDataMemberTemplate("private", varName, type, null);
            String memberSetter = SetterTemplate.getSetterTemplate("public", varName, type);
            String memberGetter = GetterTemplate.getGetterTemplate("public", varName, type);
            setters.append(memberSetter);
            getters.append(memberGetter);
            classDataMembers.append(dataMemberTemplate);
        }

        // Append data members to class
        classTemplate.append(classDataMembers);

        // Instantiate a default constructor
        String defaultConstructorTemplate = DefaultConstructorTemplate.getDefaultConstructorTemplate("public", className);
        classTemplate.append(defaultConstructorTemplate);

        // Instantiate a constructor
        // String constructorTemplate = ConstructorTemplate.getConstructor("public", className, dataMembers);
        // classTemplate.append(constructorTemplate);



        // Checking if the created class belongs to table or a normal type
        if (isClass) {
            // Load data from file method
            String loadDataMethod = LoadDataMethodTemplate.getLoadDataMethodTemplate("public", className);

            // Appending load data method
            classTemplate.append(loadDataMethod);
        }


        // Append Setters & Getters to class
        classTemplate.append(setters);
        classTemplate.append(getters);


       ArrayList<String> args = (ArrayList<String>) dataMembers.stream().map(Pair::getValue).collect(Collectors.toList());
       classTemplate.append(ToStringTemplate.getToStringTemplate(args));

        // Class tail
        classTemplate.append("\n}\n");
        return classTemplate.toString();
    }

}
