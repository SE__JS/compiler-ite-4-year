package CG.Templates.Core;


public class PrintArrayListTemplate {
    public static String getPrintingArrayListTemplate() {
        return "\tpublic static void printArrayList(ArrayList<?> arrayList) {\n" +
                "\t\t\tarrayList.forEach(System.out::println);\n" +
                "\t}\n";
    }
}
