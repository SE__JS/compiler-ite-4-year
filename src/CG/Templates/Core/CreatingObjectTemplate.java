package CG.Templates.Core;

import java.util.ArrayList;

public class CreatingObjectTemplate {
    public static String getCreatingObjectTemplate(String className, String objectName) {
        return className + " " + objectName + " = " + "new " + className + "();\n";
    }
    public static String getCreatingObjectTemplate(String className, String objectName, ArrayList<String> conParams) {
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0; i < conParams.size(); i++) {
            if(i + 1 == conParams.size()) {
                stringBuilder.append(conParams.get(i));
            } else {
                stringBuilder.append(conParams.get(i)).append(", ");
            }
        }
        return className + " " + objectName + " = " + "new " + className + "(" + stringBuilder.toString() + ");\n";
    }
}
