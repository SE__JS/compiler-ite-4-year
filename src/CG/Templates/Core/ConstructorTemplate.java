package CG.Templates.Core;

import javafx.util.Pair;

import java.util.ArrayList;

public class ConstructorTemplate {
    public static String getConstructor(String accessModifier, String className, ArrayList<Pair<String, String>> params) {

        StringBuilder paramsBuilder = new StringBuilder();
        StringBuilder paramsBodyBuilder = new StringBuilder();

        int i = 0;
        for (Pair<String, String> p : params
        ) {
            String type = TypeTemplate.getPrimitiveType(p.getKey());
            String name = p.getValue();
            String aP;
            i++;
            if (i >= params.size()) {
                aP = type + " " + name;
            } else {
                aP = type + " " + name + ", ";
            }
            String varInConstructor = "\t\tthis." + name + " = " + name + ";\n";
            paramsBodyBuilder.append(varInConstructor);
            paramsBuilder.append(aP);
        }
        return "\n\t" + accessModifier + " " + className + "(" + paramsBuilder.toString() + ") {\n" +
                paramsBodyBuilder.toString() +
                "\t}\n\n";

    }
}
