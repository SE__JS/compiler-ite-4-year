package CG.Templates.Core;

import com.sun.xml.internal.ws.util.StringUtils;

public class SetterTemplate {
    public static String getSetterTemplate(String accessModifier, String dataMember, String type) {
        return "\t"+accessModifier + " void " + "set" + StringUtils.capitalize(dataMember) + "(" + type + " " + dataMember + ") {\n"
                + "\t\tthis." + dataMember + " = " + dataMember + ";"
                +"\n\t}\n\n";
    }
}
