package CG.Mappers;

import AST.FileHandler;
import AST.Main;
import CG.Helper.TableDetector;
import CG.Templates.Core.GetterTemplate;
import CG.Templates.Core.TryCatchTemplate;
import CG.Templates.Core.TypeTemplate;
import CG.Templates.SQL.ChainedGetterTemplate;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore.SelectCore;
import SymbolTable.AggregationFunctionSymbol;
import javafx.util.Pair;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;

public class AggregationFunctionMapper {
    public static class AGG_FUNC_MAPPER {
        public String arrayListForAggFunc;
        public String applyingDataToArrayList;
        public String tcStatements;
    }

    public static AggregationFunctionMapper.AGG_FUNC_MAPPER mapAggregationFunction(
            SelectCore selectCore,
            String baseTable,
            String subTable) {

        AggregationFunctionMapper.AGG_FUNC_MAPPER instance = new AggregationFunctionMapper.AGG_FUNC_MAPPER();

        ArrayList<Pair<String, String>> funcs = SelectCore.getAggregationFunctionsInQuery(selectCore);

        StringBuilder arrayListForAggFunc = new StringBuilder();
        StringBuilder applyingDataToArrayList = new StringBuilder();
        StringBuilder tcStatements = new StringBuilder();

        boolean hasGroupBy = false;
        if (selectCore.getGroupByClause() != null && selectCore.getGroupByClause().getExpressions() != null && selectCore.getGroupByClause().getExpressions().size() > 0) {
            hasGroupBy = true;
        }
        for (Pair<String, String> p : funcs) {
            AggregationFunctionSymbol agg = Main.symbolTable.getAggFunc(p.getKey());
            boolean doesExist = Main.symbolTable.checkAggFunction(p.getKey());
            if (!doesExist) {
                String err = "Error on " + selectCore.getLine() + " charAt " + selectCore.getCol() + " aggregation function " + p.getKey() + " is undefined";
                System.out.println(err);
                FileHandler.printToFile("errors.txt", err);
            } else {
                String arrayListName = p.getKey() + "_" + baseTable + "_" + subTable;

                String bArrayList = "ArrayList<Object> " + arrayListName + " = new ArrayList<>();\n";
                arrayListForAggFunc.append(bArrayList);

                if (p.getValue().equals("*")) {
                    String fBody = arrayListName + ".add(1);\n";
                    applyingDataToArrayList.append(fBody);
                } else {
                    // String fHeader = "for(" + baseTable + " b_table_var : " + baseTable + '.' + baseTable + "sArrayList) {\n";
                    // aggBuilder.append(fHeader);
                    String getter = "";
                    String[] str = p.getValue().split("\\.");

                    String baseType = str[0];
                    String requiredC = str[str.length - 1];
                    if (TableDetector.isTable(baseType)) {
                        String fBody = arrayListName + ".add(" + "sss.get" + StringUtils.capitalize(requiredC) + "());\n";
                        String ff = "for(" + baseType + " sss: ele.get" + StringUtils.capitalize(baseType) + "()) {\n" +
                                fBody +
                                "}\n";
                        applyingDataToArrayList.append(ff);
                    } else {
                        getter = ChainedGetterTemplate.getTemplate(str, "ele");
                        String fBody = arrayListName + ".add(" + getter + ");\n";
                        applyingDataToArrayList.append(fBody);
                    }
                    // String getter = "b_table_var.get" + StringUtils.capitalize(p.getValue()) + "()";

                    // aggBuilder.append("\n}\n");
                }


                // Last part, invoking part
                String javaType = TypeTemplate.getPrimitiveType(agg.getReturnType());
                String callingAggFunction = javaType + " o = (" + javaType + ") " + p.getKey() + "(" + arrayListName + ");\n";
                String objectName = subTable.substring(0, subTable.length() > 1 ? subTable.length() / 2 : subTable.length());


                String settingDataInMainClassIfGroupByPresented = objectName + ".set" + StringUtils.capitalize(p.getKey()) + "(o);\n";
                String settingDataInMainClassInNormalCases = subTable + "." + subTable + "sArrayList.forEach(anObject -> anObject.set" + StringUtils.capitalize(p.getKey()) + "(o));\n";

                String settingData = hasGroupBy ? settingDataInMainClassIfGroupByPresented : settingDataInMainClassInNormalCases;
                String printingRes = "System.out.println(\"" + p.getKey() + ": \" + o);\n";


                String tcTemplate = TryCatchTemplate.getTryCatchTemplate(callingAggFunction + settingData + printingRes);
                tcStatements.append(tcTemplate);
            }
        }

        instance.arrayListForAggFunc = arrayListForAggFunc.toString();
        instance.applyingDataToArrayList = applyingDataToArrayList.toString();
        instance.tcStatements = tcStatements.toString();
        return instance;
    }
}
