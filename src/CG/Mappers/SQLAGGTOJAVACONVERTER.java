package CG.Mappers;

public class SQLAGGTOJAVACONVERTER {
    public static String getMethodName(String sqlOrJava) {

        switch (sqlOrJava.toLowerCase()) {
            case "avg":
                return "average";
            case "max":
            case "min":
            case "sum":
            case "count":
            case "average":
                return sqlOrJava.toLowerCase();

            default:
                return "count";

        }
    }

}
