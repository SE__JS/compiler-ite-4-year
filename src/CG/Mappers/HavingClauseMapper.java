package CG.Mappers;

import CG.Templates.SQL.WhereTemplate;
import SingleParse.Expression.SingleExpression;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore.HavingClause;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore.SelectCore;

import java.util.ArrayList;
import java.util.Collections;

public class HavingClauseMapper {
    public static class HavingClauseInstance {
        public String condition;
        public boolean isAgg;
    }

    public static HavingClauseInstance mapHavingClauseToJava(String tableName, SelectCore selectCore) {

        HavingClauseInstance instance = new HavingClauseInstance();

        if (selectCore.getHavingClause() == null) return instance;
        HavingClause havingClause = selectCore.getHavingClause();
        if (havingClause == null || havingClause.getExpression() == null) return instance;


        ArrayList<SingleExpression> ses = new ArrayList<>();
        ArrayList<String> oprs = new ArrayList<>();

        havingClause.getExpression().getSingleExpressionsForHavingClause(ses, oprs);

        ses.forEach(e -> System.out.println("E: " + e));
        for (String opr : oprs) {
            System.out.println("O: " + opr);
        }


        Collections.reverse(oprs);
        try {
            if (oprs.size() == ses.size()) oprs.remove(oprs.size() - 1);
        } catch (Exception ee) {
            System.out.println(ee.getMessage());
        }


        WhereTemplate.WhereInstance whereInstance = WhereTemplate.getWhereTemplate(tableName, ses, oprs, "e", "e");
        instance.condition = whereInstance.condition;
        instance.isAgg = whereInstance.aggFunctionInHaving;
        return instance;
    }
}
