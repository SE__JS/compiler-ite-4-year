package CG.Mappers;

import CG.Templates.SQL.ChainedGetterTemplate;
import CG.Templates.SQL.WhereTemplate;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore.GroupByClause;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore.SelectCore;

import java.util.ArrayList;

public class GroupByMapper {
    public static class GroupByInstance {
        public String mapDeclaration;
        public String body;
        public String groupByOne;
        public String groupByTwo;
        public String mapName;
        public String havingPredicate;

    }


    public static GroupByMapper.GroupByInstance mapGroupByToJava(String tableName,
                                                                 String varName,
                                                                 SelectCore selectCore,
                                                                 WhereTemplate.WhereInstance predicate,
                                                                 HavingClauseMapper.HavingClauseInstance havingPredicate) {
        GroupByMapper.GroupByInstance instance = new GroupByInstance();
        String twoT = "\t\t";
        String threeT = twoT + "\t";
        String fourT = twoT + twoT;


        String mapName = "groupedBy_" + varName + "_" + tableName;

        instance.mapName = mapName;

        // Group by
        ArrayList<String> exprs = new ArrayList<>();
        if (selectCore.getGroupByClause() != null) {
            GroupByClause groupByClause = selectCore.getGroupByClause();
            exprs = GroupByClause.getGroupByExpressions(groupByClause);
        }

        if (exprs.size() == 0) {
            return instance;
        }

        String filter = "";
        if (predicate != null && predicate.condition != null && !predicate.condition.equals("")) {
            filter = ".filter(ele -> " + predicate.condition + ")";
        }

        String havingFilter = "";

        if (havingPredicate != null &&
                havingPredicate.condition != null && !havingPredicate.equals("")) {

            String body = "";
            if (havingPredicate.isAgg) {
                body = havingPredicate.condition;
            } else {
                body = "            List<" + tableName + "> ls = entry\n" +
                        "                    .getValue()\n" +
                        "                    .stream()\n" +
                        "                    .filter(e -> " + havingPredicate.condition + ")\n" +
                        "                    .collect(Collectors.toList());\n" +
                        "            entry.setValue(ls);\n";
            }
            havingFilter = "for(Map.Entry<String, List<" + tableName + ">> entry: " + mapName + ".entrySet()) {\n" +
                    body +
                    "        }\n";
        }

        instance.havingPredicate = havingFilter;

        if (exprs.size() > 1) {
            String by1 = exprs.get(0);
            String by2 = exprs.get(1);
            System.out.println("BY1: " + by1);
            System.out.println("TABLE N: " + tableName);
            if (by1.startsWith(tableName)) {
                by1 = by1.substring(tableName.length() + 1);
            }
            String[] str1 = by1.split("\\.");
            String[] str2 = by2.split("\\.");

            String firstGetter = ChainedGetterTemplate.getTemplate(str1, "f");
            String secondGetter = ChainedGetterTemplate.getTemplate(str2, "s");


            String functionName = "groupByTwoKeysFor" + tableName + "()";

            instance.mapDeclaration = "Map<String, Map<String, List<" + tableName + ">>> " + mapName + " = " + functionName + ";\n";

            instance.groupByTwo = "public static Map<String, Map<String, List<" + tableName + ">>>" + functionName + "{\n" +
                    "return " + tableName + "." + tableName + "sArrayList" +
                    ".stream()" + filter + ".collect(Collectors.groupingBy(f -> String.valueOf(" + firstGetter + "),Collectors.groupingBy(s -> String.valueOf(" + secondGetter + "))))" +
                    ";" +
                    "\n}\n";
        } else {
            String by1 = exprs.get(0);
            if (by1.startsWith(tableName)) {
                by1 = by1.substring(tableName.length() + 1);
            }
            String[] str1 = by1.split("\\.");
            String getter = ChainedGetterTemplate.getTemplate(str1, "f");

            String functionName = "groupByOneKeyFor" + tableName + "_" + varName + "()";
            instance.mapDeclaration = "Map<String, List<" + tableName + ">> " + mapName + " = " + functionName + ";\n";


            instance.groupByOne = "public static Map<String, List<" + tableName + ">> " + functionName + "{\n" +
                    "return " + tableName + "." + tableName + "sArrayList" +
                    ".stream()" + filter + ".collect(Collectors.groupingBy(f -> String.valueOf(" + getter + ")));" +
                    "\n}\n";
        }

        return instance;
    }
}
