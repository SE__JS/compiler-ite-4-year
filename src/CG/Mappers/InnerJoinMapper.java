package CG.Mappers;

import CG.Helper.ColumnsNameFromTypeExtractor;
import CG.Templates.SQL.InnerJoinTemplate;
import SingleParse.SQL.SqlStmtList.SqlStatement.JoinClause.JoiningTableData;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore.SelectCore;
import SymbolTable.Type;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class InnerJoinMapper {

    private static Pair<ArrayList<String>, ArrayList<String>> getSeparatedColumns(JoiningTableData j,
                                                                                  ArrayList<String> cs) {
        ArrayList<String> res1 = new ArrayList<>();
        ArrayList<String> res2 = new ArrayList<>();
        String firstType = j.getFirstTableName();
        String secondType = j.getSecondTableName();
        for(String temp: j.getAllColumns()) {
            String[] str = temp.split("\\.");
            String bName = str[0];
            StringBuilder cName = new StringBuilder();
            for (int i = 1; i < str.length; i++) {
                if(i + 1 == str.length) cName.append(str[i]);
                else cName.append(str[i]).append('.');
            }
            String fName = cName.toString();
            boolean ok = false;

            for(String pp: cs) {
                if(pp.toLowerCase().equals(fName.toLowerCase())) {
                    ok = true;
                    break;
                }
            }
            if(!ok) continue;

            if(bName.toLowerCase().equals(firstType.toLowerCase())) {
                res1.add(fName);
            }
            else if(bName.toLowerCase().equals(secondType.toLowerCase())) {
                res2.add(fName);
            }
        }

        return new Pair<>(res1, res2);
    }
    public static String innerJoinMapper(Type variableType,
                                           SelectCore selectCore) {
        JoiningTableData j = SelectCore.getJoiningData(selectCore);


        if (j != null && j.getFirstTableName() != null && j.getSecondTableName() != null) {


//            List<Faculty> joiningList = Faculty.FacultysArrayList.stream()
//                    .filter(f -> Place.PlacesArrayList.stream()
//                            .anyMatch(s -> f.getFacultyName().equals(s.getName())))
//                    .collect(Collectors.toList());


            ArrayList<String> ccs = ColumnsNameFromTypeExtractor.getColumnsNames(variableType);
            System.out.println("ALL COLUMNS: " + ccs);
            if(ccs.size() == 0) {
                ccs = j.getAllColumns();
            }

            Pair<ArrayList<String>, ArrayList<String>> ans = getSeparatedColumns(j, ccs);

            System.out.println("PAIR OF C: " + ans);
            return InnerJoinTemplate.getTemplate(
                    j.getFirstTableName(),
                    j.getSecondTableName(),
                    variableType.getName(),
                    ans.getKey(),
                    ans.getValue(),
                    j.getSingleExpressions(),
                    j.getOperationsInBetween());
        }
        return "";
    }
}
