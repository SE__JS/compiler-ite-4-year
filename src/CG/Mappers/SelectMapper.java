package CG.Mappers;

import CG.Templates.Core.MainMethodTemplate;
import CG.Templates.SQL.AssigningSelectResultToVariableTemplate;
import CG.Templates.SQL.WhereTemplate;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore.GroupByClause;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore.HavingClause;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore.SelectCore;
import javafx.util.Pair;
import SymbolTable.*;

import java.util.ArrayList;

public class SelectMapper {
    public static void assigningSelectToClass(String tableName,
                                              Type variableType,
                                              SelectCore selectCore,
                                              ArrayList<Pair<String, String>> fullColsNameWithTypes) {
        // If select query contains where clause
        // we need to map it to if
        WhereTemplate.WhereInstance whereInstance = WhereMapper.mapWhereToIf(tableName, selectCore);


        // Inner join
         String innerJoinTemplate = InnerJoinMapper.innerJoinMapper(variableType, selectCore);
         MainMethodTemplate.StaticMainBody.append(innerJoinTemplate);

        AggregationFunctionMapper.AGG_FUNC_MAPPER aggregationMapper = AggregationFunctionMapper.mapAggregationFunction(
                selectCore,
                tableName,
                variableType.getName());


        // Group by & Having

        HavingClauseMapper.HavingClauseInstance havingInstance = HavingClauseMapper.mapHavingClauseToJava(tableName, selectCore);


        GroupByMapper.GroupByInstance groupByMapper = GroupByMapper.mapGroupByToJava(
                tableName,
                variableType.getName(),
                selectCore,
                whereInstance,
                havingInstance);


        // Mapping query to for loop with where template
        String t = AssigningSelectResultToVariableTemplate.getTemplate(
                tableName,
                variableType.getName(),
                fullColsNameWithTypes,
                whereInstance,
                aggregationMapper,
                groupByMapper);


        // Append final result to main body
        MainMethodTemplate.StaticMainBody.append(t);


        String arrayList = variableType.getName() + "." + variableType.getName() + "sArrayList";
        String printingResult = "System.out.println(\"====================================\");\n" +
                "System.out.println(\"Result for " + variableType.getName() + "\");\n" +
                "        printArrayList(" + arrayList + ");\n";
        MainMethodTemplate.StaticMainBody.append(printingResult);
    }
}
