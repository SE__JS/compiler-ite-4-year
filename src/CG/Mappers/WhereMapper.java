package CG.Mappers;

import CG.Templates.SQL.WhereTemplate;
import SingleParse.Expression.Expression;
import SingleParse.Expression.SingleExpression;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore.SelectCore;

import java.util.ArrayList;
import java.util.Collections;

public class WhereMapper {
    public static  WhereTemplate.WhereInstance mapWhereToIf(String tableName, SelectCore selectCore) {
        ArrayList<String> oprs = new ArrayList<>();
        ArrayList<SingleExpression> ses = new ArrayList<SingleExpression>();

        if (selectCore != null &&
                selectCore.getWhereClause() != null &&
                selectCore.getWhereClause().getExpression() != null) {
            Expression e = selectCore.getWhereClause().getExpression();

            e.getSingleExpressions(ses, oprs);
            Collections.reverse(oprs);

            ses.forEach(System.out::println);
            oprs.forEach(System.out::println);
            return WhereTemplate.getWhereTemplate(tableName, ses, oprs, "ele", "ele");
        }
        return new WhereTemplate.WhereInstance();
    }
}
