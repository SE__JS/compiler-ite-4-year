package AST;

import CG.Templates.Core.ClassTemplate;
import CG.Templates.Core.MainMethodTemplate;
import CG.Templates.Core.TryCatchTemplate;
import CG.Templates.SQL.AggregationFunctionTemplate;
import Checkers.*;
import GrammarRules.Parse;
import SingleParse.Expression.*;
import SingleParse.Java.AllowedName;
import SingleParse.Java.AnyStatement.*;
import SingleParse.Java.AnyStatement.TernaryExpression.TernaryComparasion;
import SingleParse.Java.AnyStatement.TernaryExpression.TernaryExpression;
import SingleParse.Java.AnyStatement.IfElse2.ElseStatements;
import SingleParse.Java.AnyStatement.IfElse2.IfElse;
import SingleParse.Java.AnyStatement.IfElse2.IfStatements;
import SingleParse.Java.AnyStatement.LoopStatement.DoWhile;
import SingleParse.Java.AnyStatement.LoopStatement.ForEachStatement;
import SingleParse.Java.AnyStatement.LoopStatement.ForStatement;
import SingleParse.Java.AnyStatement.LoopStatement.WhileStatement;
import SingleParse.Java.AnyStatement.SingleLineStatement.*;
import SingleParse.Java.AnyStatement.SingleLineStatement.TernatyStetement.*;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarArgument;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarAssignment;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarDefinition;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.ArrayAssignment.ArrayAssignment;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.ArrayAssignment.ArrayDefinition;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.ArrayAssignment.ArrayInvoking;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.PossibleValue.*;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.VarOperator;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarStatement;
import SingleParse.Java.AnyStatement.SwitchCase.BreakStatement;
import SingleParse.Java.AnyStatement.SwitchCase.CaseBlock;
import SingleParse.Java.AnyStatement.SwitchCase.DefaultCase;
import SingleParse.Java.AnyStatement.SwitchCase.SwitchStatement;
import SingleParse.Java.FunctionDefinition;
import SingleParse.Java.Functions.*;
import SingleParse.SQL.SqlStmtList.SqlStatement.AggregationFunction.*;
import SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.*;
import SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnDefinitionConstraint.ColumnConstraint.*;
import SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnDefinitionConstraint.ColumnConstraint.ColumnDefault.ColumnDefault;
import SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnDefinitionConstraint.ColumnConstraint.ColumnDefault.ColumnDefaultList;
import SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnDefinitionConstraint.ColumnConstraint.ColumnDefault.ColumnDefaultMain;
import SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnDefinitionConstraint.ColumnConstraintTypeName;
import SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnDefinitionConstraint.ColumnDefinitionConstraint;
// import SingleParse.SQL.SqlStmtList.SqlStatement.CreateTableStatement.CreateTableStatement;
import SingleParse.SQL.SqlStmtList.SqlStatement.CreateTableStatement.CreateTableStmtMain;
import SingleParse.SQL.SqlStmtList.SqlStatement.CreateTableStatement.SingleTableConstraintOrColumnDef;
import SingleParse.SQL.SqlStmtList.SqlStatement.DeleteStatement.DeleteStatement;
import SingleParse.SQL.SqlStmtList.SqlStatement.DeleteStatement.QualifiedTableName.IndexedOrNot;
import SingleParse.SQL.SqlStmtList.SqlStatement.DeleteStatement.QualifiedTableName.IndexedQualifiedTableName;
import SingleParse.SQL.SqlStmtList.SqlStatement.DeleteStatement.QualifiedTableName.NotIndexedQualifiedTableName;
import SingleParse.SQL.SqlStmtList.SqlStatement.DeleteStatement.QualifiedTableName.QualifiedTableName;
import SingleParse.SQL.SqlStmtList.SqlStatement.DropTableStatement.DropTableStatement;
import SingleParse.SQL.SqlStmtList.SqlStatement.FactoredSelectStatement.FactoredMain;
import SingleParse.SQL.SqlStmtList.SqlStatement.FactoredSelectStatement.FactoredSelectStatement;
import SingleParse.SQL.SqlStmtList.SqlStatement.InsertStatement.*;
import SingleParse.SQL.SqlStmtList.SqlStatement.JoinClause.*;
import SingleParse.SQL.SqlStmtList.SqlStatement.Loader.CreateTableStatement.*;
import SingleParse.SQL.SqlStmtList.SqlStatement.Loader.CreateTypeStatement.CreateTypeStatement;
import SingleParse.SQL.SqlStmtList.SqlStatement.MainSql.*;
import SingleParse.SQL.SqlStmtList.SqlStatement.ResultColumn.ColumnAlias;
import SingleParse.SQL.SqlStmtList.SqlStatement.ResultColumn.ResultColumn;
import SingleParse.SQL.SqlStmtList.SqlStatement.ResultColumn.ResultColumnExpr;
import SingleParse.SQL.SqlStmtList.SqlStatement.ResultColumn.ResultColumnStar;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore.*;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectStatement.SelectStatement;
import SingleParse.SQL.SqlStmtList.SqlStatement.SqlStatement;
import SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.*;
import SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey.ForeignColumnName;
import SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey.ForeignColumnNameList;
import SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey.ForeignKeyClause.ForeignKeyClause;
import SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey.ForeignKeyClause.ForeignKeyClauseList;
import SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey.ForeignKeyClause.ForeignKeyClauseMain.ForeignKeyClauseMain;
import SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey.ForeignKeyClause.ForeignKeyClauseMain.ForeignTable;
import SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey.ForeignKeyClause.ForeignKeyClauseMain.ForeignTargetColumnName;
import SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey.ForeignKeyClause.ForeignKeyClauseMain.ForeignTargetColumnNameList;
import SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey.ForeignKeyClause.ForeignKeyClausePart;
import SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey.TableConstraintForeignKey;
import SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey.TableConstraintForeignKeyBody;
import SingleParse.SQL.SqlStmtList.SqlStatement.UpdateStatement.UpdateStatement;
import SingleParse.SQL.SqlStmtList.SqlStatement.UpdateStatement.UpdateStatementMain;
import SingleParse.SQL.SqlStmtList.SqlStmtList;
import SingleParse.*;
import javafx.util.Pair;
import SymbolTable.*;

import java.util.*;

public class MyASTVisitor implements ASTVisitor {
    @Override
    public void visit(DoWhile doWhile) {
        // System.out.println("AST_VISITOR -->  doWhile");
    }

    @Override
    public void visit(Parse parse) {
        // System.out.println("AST_VISITOR -->  ast Parse");
    }

    @Override
    public void visit(SingleParse singleParse) {
        // System.out.println("AST_VISITOR -->  ast SingleParse");
    }

    @Override
    public void visit(AnyStatement anyStatement) {
        // System.out.println("AST_VISITOR -->  ast AnyStatement");
    }

    @Override
    public void visit(SingleLineStatement singleLineStatement) {
        // System.out.println("AST_VISITOR -->  ast SingleLineStatement");
    }

    @Override
    public void visit(WhileStatement whileStatement) {
        // System.out.println("AST_VISITOR -->  whileStatement");
    }

    @Override
    public void visit(ForStatement forStatement) {
        // System.out.println("AST_VISITOR -->  forStatement");
    }

    @Override
    public void visit(ForEachStatement forEachStatement) {
        // System.out.println("AST_VISITOR -->  forEachStatement");
    }

    @Override
    public void visit(ScopeBlock scopeBlock) {
        // System.out.println("AST_VISITOR -->  scopeBlock");
    }

    @Override
    public void visit(SwitchStatement switchStatement) {
        // System.out.println("AST_VISITOR -->  switchStatement");
    }

    @Override
    public void visit(IfElse ifElse) {
        // System.out.println("AST_VISITOR -->  ifElse");
    }

    @Override
    public void visit(IfStatements ifStatements) {
        // System.out.println("AST_VISITOR -->  ifStatements");
    }

    @Override
    public void visit(ElseStatements elseStatements) {
        // System.out.println("AST_VISITOR -->  elseStatements");
    }

    @Override
    public void visit(VarStatement varStatement) {
        // System.out.println("AST_VISITOR -->  varStatement");
    }

    @Override
    public void visit(PrintStatement printStatement) {
        // System.out.println("AST_VISITOR -->  ast PrintStatement");
        if (printStatement.getAllowedName() != null) {
            VariableDeclarationChecker.check(printStatement.getAllowedName());
        }
    }

    @Override
    public void visit(InvokingFunction invokingFunction) {
        // System.out.println("AST_VISITOR -->  invokingFunction");
        FunctionInvokingChecker.check(invokingFunction);

    }

    @Override
    public void visit(TernaryStatement ternaryStatement) {
        // System.out.println("AST_VISITOR -->  ternaryStatement");
    }

    @Override
    public void visit(DotBlock dotBlock) {
        // System.out.println("AST_VISITOR -->  dotBlock");
        // DotBlockChecker.check(dotBlock);
    }

    @Override
    public void visit(ReturnStatement returnStatement) {
        // System.out.println("AST_VISITOR -->  returnStatement");
    }

    @Override
    public void visit(TerminalNode terminalNode) {
        // System.out.println("AST_VISITOR -->  terminalNode");
    }

    @Override
    public void visit(Expression expression) {
        // System.out.println("AST_VISITOR -->  expression");
    }

//  @Override
//  public void visit(KeyValuePair keyValuePair) {
//    // System.out.println("AST_VISITOR -->  keyValuePair");
//  }

    @Override
    public void visit(VarArgument varArgument) {
        // System.out.println("AST_VISITOR -->  varArgument");
        VariableArgumentChecker.check(varArgument);
    }


    @Override
    public void visit(FunctionDefinition functionDefinition) {
        // System.out.println("AST_VISITOR -->  functionDefinition");
    }

    @Override
    public void visit(Argument argument) {
        // System.out.println("AST_VISITOR -->  argument");
    }

    @Override
    public void visit(HigherOrderFunction higherOrderFunction) {
        // System.out.println("AST_VISITOR -->  higherOrderFunction");
    }

    @Override
    public void visit(LeftTernaryStatement leftTernaryStatement) {
        // System.out.println("AST_VISITOR -->  leftTernaryStatement");
    }

    @Override
    public void visit(RightTernaryStatement rightTernaryStatement) {
        // System.out.println("AST_VISITOR -->  rightTernaryStatement");
    }

    @Override
    public void visit(SinglePlusMinus singlePlusMinus) {
        // System.out.println("AST_VISITOR -->  singlePlusMinus");
    }

    @Override
    public void visit(LeftTernaryRightSide leftTernaryRightSide) {
        // System.out.println("AST_VISITOR -->  leftTernaryRightSide");
    }

    @Override
    public void visit(RightTernaryRightSide rightTernaryRightSide) {
        // System.out.println("AST_VISITOR -->  rightTernaryRightSide");
    }

    @Override
    public void visit(ConditionalExpression conditionalExpression) {
        // System.out.println("AST_VISITOR -->  conditionalExpression");
    }

    @Override
    public void visit(ConditionalParams conditionalParams) {
        // System.out.println("AST_VISITOR -->  conditionalParams");
    }

    @Override
    public void visit(CheckingBlock checkingBlock) {
        // System.out.println("AST_VISITOR -->  checkingBlock");
    }

    @Override
    public void visit(NextConditionalExpr nextConditionalExpr) {
        // System.out.println("AST_VISITOR -->  nextConditionalExpr");
    }

    @Override
    public void visit(CaseBlock caseBlock) {
        // System.out.println("AST_VISITOR -->  caseBlock");
    }

    @Override
    public void visit(DefaultCase defaultCase) {
        // System.out.println("AST_VISITOR -->  defaultCase");
    }

    @Override
    public void visit(Parameter parameter) {
        // System.out.println("AST_VISITOR -->  parameter");
    }

    @Override
    public void visit(OptionalParameter optionalParameter) {
        // System.out.println("AST_VISITOR -->  optionalParameter");
    }

    @Override
    public void visit(ArrayInvoking arrayInvoking) {
        // System.out.println("AST_VISITOR -->  arrayInvoking");
    }

    @Override
    public void visit(BitwiseOperator bitwiseOperator) {
        // System.out.println("AST_VISITOR -->  bitwiseOperator");
    }

    @Override
    public void visit(LogicalOperator logicalOperator) {
        // System.out.println("AST_VISITOR -->  logicalOperator");
    }

    @Override
    public void visit(MathFirstPriorty mathFirstPriorty) {
        // System.out.println("AST_VISITOR -->  mathFirstPriorty");
    }

    @Override
    public void visit(MathSecondPriorty mathSecondPriorty) {
        // System.out.println("AST_VISITOR -->  mathSecondPriorty");
    }

    @Override
    public void visit(Or or) {
        // System.out.println("AST_VISITOR -->  or");
    }

    @Override
    public void visit(SqlAndOr sqlAndOr) {
        // System.out.println("AST_VISITOR -->  sqlAndOr");
    }

    @Override
    public void visit(SqlColumnName sqlColumnName) {
        // System.out.println("AST_VISITOR -->  sqlColumnName");
    }

    @Override
    public void visit(SqlLogicalOperator sqlLogicalOperator) {
        // System.out.println("AST_VISITOR -->  sqlLogicalOperator");
    }

    @Override
    public void visit(UnaryOperator unaryOperator) {
        // System.out.println("AST_VISITOR -->  unaryOperator");
    }

    @Override
    public void visit(SingleParseError singleParseError) {
        // System.out.println("AST_VISITOR -->  singleParseError");
    }

    @Override
    public void visit(BreakStatement breakStatement) {
        // System.out.println("AST_VISITOR -->  ast BreakStatement");
    }

    @Override
    public void visit(TernaryExpression ternaryExpression) {
        // System.out.println("AST_VISITOR -->  ternaryExpression");
    }

    @Override
    public void visit(VarAssignment varAssignment) {
        // System.out.println("AST_VISITOR -->  varAssignment");
        VariableAssignmentChecker.check(varAssignment);
    }

    @Override
    public void visit(VarDefinition varDefinition) {
        // System.out.println("AST_VISITOR -->  varDefinition");
        ArrayList<VarArgument> varArgumentArrayList = varDefinition.getVarArguments();
        UnAssigningVariableChecker.check(varArgumentArrayList);
    }

    @Override
    public void visit(AllowedName allowedName) {
        // System.out.println("AST_VISITOR -->  allowedName");
        VariableDeclarationChecker.check(allowedName);
        NumberOfDeclarationInSameScopeChecker.check(allowedName);
        NumberOfDeclarationInAncestorsScopesChecker.check(allowedName);
    }

    @Override
    public void visit(VarOperator varOperator) {
        // System.out.println("AST_VISITOR -->  varOperator");
    }

    @Override
    public void visit(ArrayAssignment arrayAssignment) {
        // System.out.println("AST_VISITOR -->  arrayAssignment");
    }

    @Override
    public void visit(PossibleValue possibleValue) {
        // System.out.println("AST_VISITOR -->  possibleValue");

    }

    @Override
    public void visit(ArrayDefinition arrayDefinition) {
        // System.out.println("AST_VISITOR -->  arrayDefinition");
    }

    @Override
    public void visit(NummericValue nummericValue) {
        // System.out.println("AST_VISITOR -->  nummericValue");
    }

    @Override
    public void visit(BooleanValue booleanValue) {
        // System.out.println("AST_VISITOR -->  booleanValue");
    }

    @Override
    public void visit(NullValue nullValue) {
        // System.out.println("AST_VISITOR -->  nullValue");
    }

    @Override
    public void visit(JsonObject jsonObject) {
        // System.out.println("AST_VISITOR -->  jsonObject");
    }

    // ========================== SQL ==============================

    @Override
    public void visit(TableOrSubQueryMain tableOrSubQueryMain) {
        // System.out.println("AST_VISITOR -->  tableOrSubQueryMain");
    }

    @Override
    public void visit(TableOrSubQueryLast tableOrSubQueryLast) {
        // System.out.println("AST_VISITOR -->  tableOrSubQueryLast");
    }


    @Override
    public void visit(DatabaseName databaseName) {
        // System.out.println("AST_VISITOR -->  databaseName");
    }

    @Override
    public void visit(LiteralValue literalValue) {
        // System.out.println("AST_VISITOR -->  literalValue");
    }

    @Override
    public void visit(NewTableName newTableName) {
        // System.out.println("AST_VISITOR -->  newTableName");
    }

    @Override
    public void visit(SourceTableName sourceTableName) {
        // System.out.println("AST_VISITOR -->  sourceTableName");
    }

    @Override
    public void visit(SqlStmtList sqlStmtList) {
        // System.out.println("AST_VISITOR -->  sqlStmtList");
    }

    @Override
    public void visit(AlterTableAdd alterTableAdd) {
        // System.out.println("AST_VISITOR -->  alterTableAdd");
    }

    @Override
    public void visit(AlterTableAddConstraint alterTableAddConstraint) {
        // System.out.println("AST_VISITOR -->  alterTableAddConstraint");
    }

    @Override
    public void visit(AlterTableStatement alterTableStatement) {
        // System.out.println("AST_VISITOR -->  alterTableStatement");
    }

    @Override
    public void visit(CollationName collationName) {
        // System.out.println("AST_VISITOR -->  collationName");
    }

    @Override
    public void visit(CompoundOperator compoundOperator) {
        // System.out.println("AST_VISITOR -->  compoundOperator");
    }

    @Override
    public void visit(IndexColumn indexColumn) {
        // System.out.println("AST_VISITOR -->  indexColumn");
    }

    @Override
    public void visit(IndexName indexName) {
        // System.out.println("AST_VISITOR -->  indexName");
    }

    @Override
    public void visit(OrderingTerm orderingTerm) {
        // System.out.println("AST_VISITOR -->  orderingTerm");
    }

    @Override
    public void visit(SqlStatement sqlStatement) {
        // System.out.println("AST_VISITOR -->  sqlStatement");
    }

    @Override
    public void visit(TableAlias tableAlias) {
        // System.out.println("AST_VISITOR -->  tableAlias");
    }

    @Override
    public void visit(TableName tableName) {
        // System.out.println("AST_VISITOR -->  tableName");
        // System.out.println(tableName.getAllowedName().getName());
        TableTypeNameChecker.check(tableName);
    }

    @Override
    public void visit(TableOrSubQuery parse) {
        // System.out.println("AST_VISITOR -->  parse");
    }

    @Override
    public void visit(SignedNumberAllowedName signedNumberAllowedName) {
        // System.out.println("AST_VISITOR -->  signedNumberAllowedName");
    }

    @Override
    public void visit(SignedNumber signedNumber) {
        // System.out.println("AST_VISITOR -->  signedNumber");
    }

    @Override
    public void visit(ColumnName columnName) {
        // System.out.println("AST_VISITOR -->  columnName");

    }

    @Override
    public void visit(ColumnDefinition columnDefinition) {
        // System.out.println("AST_VISITOR -->  columnDefinition");
    }

    @Override
    public void visit(ColumnDefaultValue columnDefaultValue) {
        // System.out.println("AST_VISITOR -->  columnDefaultValue");
    }

    @Override
    public void visit(ColumnDefinitionConstraint columnDefinitionConstraint) {
        // System.out.println("AST_VISITOR -->  columnDefinitionConstraint");
    }

    @Override
    public void visit(ColumnConstraintTypeName columnConstraintTypeName) {
        // System.out.println("AST_VISITOR -->  columnConstraintTypeName");
    }

    @Override
    public void visit(ColumnConstraintPrimaryKey columnConstraintPrimaryKey) {
        // System.out.println("AST_VISITOR -->  columnConstraintPrimaryKey");
    }

    @Override
    public void visit(ColumnConstraintNull columnConstraintNull) {
        // System.out.println("AST_VISITOR -->  columnConstraintNull");
    }

    @Override
    public void visit(ColumnConstraintNotNull columnConstraintNotNull) {
        // System.out.println("AST_VISITOR -->  columnConstraintNotNull");
    }

    @Override
    public void visit(ColumnConstraintForeignKey columnConstraintForeignKey) {
        // System.out.println("AST_VISITOR -->  columnConstraintForeignKey");
    }

    @Override
    public void visit(ColumnConstraint columnConstraint) {
        // System.out.println("AST_VISITOR -->  columnConstraint");
    }

    @Override
    public void visit(ColumnDefaultMain columnDefaultMain) {
        // System.out.println("AST_VISITOR -->  columnDefaultMain");
    }

    @Override
    public void visit(ColumnDefaultList columnDefaultList) {
        // System.out.println("AST_VISITOR -->  columnDefaultList");
    }

    @Override
    public void visit(ColumnDefault columnDefault) {
        // System.out.println("AST_VISITOR -->  columnDefault");
    }

  /*@Override
  public void visit(CreateTableStatement createTableStatement) {
    // System.out.println("AST_VISITOR -->  createTableStatement");
  }*/

    @Override
    public void visit(CreateTableStmtMain createTableStmtMain) {
        // System.out.println("AST_VISITOR -->  createTableStmtMain");
    }

    @Override
    public void visit(SingleTableConstraintOrColumnDef singleTableConstraintOrColumnDef) {
        // System.out.println("AST_VISITOR -->  singleTableConstraintOrColumnDef");
    }

    @Override
    public void visit(DeleteStatement deleteStatement) {
        // System.out.println("AST_VISITOR -->  deleteStatement");
    }

    @Override
    public void visit(QualifiedTableName qualifiedTableName) {
        // System.out.println("AST_VISITOR -->  qualifiedTableName");
    }

    @Override
    public void visit(NotIndexedQualifiedTableName notIndexedQualifiedTableName) {
        // System.out.println("AST_VISITOR -->  notIndexedQualifiedTableName");
    }

    @Override
    public void visit(IndexedQualifiedTableName indexedQualifiedTableName) {
        // System.out.println("AST_VISITOR -->  indexedQualifiedTableName");
    }

    @Override
    public void visit(IndexedOrNot indexedOrNot) {
        // System.out.println("AST_VISITOR -->  indexedOrNot");
    }

    @Override
    public void visit(DropTableStatement dropTableStatement) {
        // System.out.println("AST_VISITOR -->  dropTableStatement");
    }

    @Override
    public void visit(FactoredMain factoredMain) {
        // System.out.println("AST_VISITOR -->  factoredMain");
    }

    @Override
    public void visit(FactoredSelectStatement factoredSelectStatement) {
        // System.out.println("AST_VISITOR -->  factoredSelectStatement");
    }

    @Override
    public void visit(InsertStmtExprRight insertStmtExprRight) {
        // System.out.println("AST_VISITOR -->  insertStmtExprRight");
    }

    @Override
    public void visit(InsertStmtExprLeft insertStmtExprLeft) {
        // System.out.println("AST_VISITOR -->  insertStmtExprLeft");
    }

    @Override
    public void visit(InsertStatementMain insertStatementMain) {
        // System.out.println("AST_VISITOR -->  insertStatementMain");
    }

    @Override
    public void visit(InsertStatementExpression insertStatementExpression) {
        // System.out.println("AST_VISITOR -->  insertStatementExpression");
    }

    @Override
    public void visit(InsertStatementDefault insertStatementDefault) {
        // System.out.println("AST_VISITOR -->  insertStatementDefault");
    }

    @Override
    public void visit(InsertStatement insertStatement) {
        // System.out.println("AST_VISITOR -->  insertStatement");
    }

    @Override
    public void visit(JoinOperator joinOperator) {
        // System.out.println("AST_VISITOR -->  joinOperator");
    }

    @Override
    public void visit(JoinConstraint joinConstraint) {
        // System.out.println("AST_VISITOR -->  joinConstraint");
    }

    @Override
    public void visit(JoinClauseMain joinClauseMain) {
        // System.out.println("AST_VISITOR -->  joinClauseMain");
        OnOperatorInJoinClauseChecker.check(joinClauseMain);
    }

    @Override
    public void visit(JoinClause joinClause) {
        // System.out.println("AST_VISITOR -->  joinClause");
    }

    @Override
    public void visit(ResultColumnStar resultColumnStar) {
        // System.out.println("AST_VISITOR -->  resultColumnStar");
    }

    @Override
    public void visit(ResultColumnExpr resultColumnExpr) {
        // System.out.println("AST_VISITOR -->  resultColumnExpr");
    }

    @Override
    public void visit(ResultColumn resultColumn) {
        // System.out.println("AST_VISITOR -->  resultColumn");
    }

    @Override
    public void visit(ColumnAlias columnAlias) {
        // System.out.println("AST_VISITOR -->  columnAlias");
    }

    @Override
    public void visit(SelectCoreFirst selectCoreFirst) {
        // System.out.println("AST_VISITOR -->  selectCoreFirst");
    }

    @Override
    public void visit(WhereClause whereClause) {
        // System.out.println("WhereCLause");
    }

    @Override
    public void visit(GroupByClause groupByClause) {
        // System.out.println("GroupByClause");


    }

    @Override
    public void visit(HavingClause havingClause) {
        // System.out.println("HavingClause");
    }

    @Override
    public void visit(SelectCore selectCore) {
        // System.out.println("AST_VISITOR -->  selectCore");
        // ArrayList<Pair<String, String>> res = SelectCore.getAggregationFunctionsInQuery(selectCore);

        /*ArrayList<String> exprs = new ArrayList<>();
        if(selectCore.getGroupByClause() != null) {
            GroupByClause groupByClause = selectCore.getGroupByClause();
            exprs = GroupByClause.getGroupByExpressions(groupByClause);
        }*/

        if (selectCore.getWhereClause() != null) {
            if (selectCore.getWhereClause().hasSubQueryInIt())
                SubQueryWithInLogicalOperatorChecker.check(selectCore);
            else NormalAndComplexColumnChecker.check(selectCore);
        } else {
            NormalAndComplexColumnChecker.check(selectCore);
        }
    }

    @Override
    public void visit(SelectStatement selectStatement) {
        // System.out.println("AST_VISITOR -->  selectStatement");
    }

    @Override
    public void visit(TableConstraintUnique tableConstraintUnique) {
        // System.out.println("AST_VISITOR -->  tableConstraintUnique");
    }

    @Override
    public void visit(TableConstraintPrimaryKey tableConstraintPrimaryKey) {
        // System.out.println("AST_VISITOR -->  tableConstraintPrimaryKey");
    }

    @Override
    public void visit(TableConstraintKey tableConstraintKey) {
        // System.out.println("AST_VISITOR -->  tableConstraintKey");
    }

    @Override
    public void visit(TableConstraint tableConstraint) {
        // System.out.println("AST_VISITOR -->  tableConstraint");
    }

    @Override
    public void visit(IndexedColumnList indexedColumnList) {
        // System.out.println("AST_VISITOR -->  indexedColumnList");
    }

    @Override
    public void visit(AllowedNameIndexColumn allowedNameIndexColumn) {
        // System.out.println("AST_VISITOR -->  allowedNameIndexColumn");
    }

    @Override
    public void visit(TableConstraintForeignKeyBody tableConstraintForeignKeyBody) {
        // System.out.println("AST_VISITOR -->  tableConstraintForeignKeyBody");
    }

    @Override
    public void visit(TableConstraintForeignKey tableConstraintForeignKey) {
        // System.out.println("AST_VISITOR -->  tableConstraintForeignKey");
    }

    @Override
    public void visit(ForeignColumnNameList foreignColumnNameList) {
        // System.out.println("AST_VISITOR -->  foreignColumnNameList");
    }

    @Override
    public void visit(ForeignColumnName foreignColumnName) {
        // System.out.println("AST_VISITOR -->  foreignColumnName");
    }

    @Override
    public void visit(ForeignKeyClausePart foreignKeyClausePart) {
        // System.out.println("AST_VISITOR -->  foreignKeyClausePart");
    }

    @Override
    public void visit(ForeignKeyClauseList foreignKeyClauseList) {
        // System.out.println("AST_VISITOR -->  foreignKeyClauseList");
    }

    @Override
    public void visit(ForeignKeyClause foreignKeyClause) {
        // System.out.println("AST_VISITOR -->  foreignKeyClause");
    }

    @Override
    public void visit(ForeignTargetColumnNameList foreignTargetColumnNameList) {
        // System.out.println("AST_VISITOR -->  foreignTargetColumnNameList");
    }

    @Override
    public void visit(ForeignTargetColumnName foreignTargetColumnName) {
        // System.out.println("AST_VISITOR -->  foreignTargetColumnName");
    }

    @Override
    public void visit(ForeignKeyClauseMain foreignKeyClauseMain) {
        // System.out.println("AST_VISITOR -->  foreignKeyClauseMain");
    }

    @Override
    public void visit(ForeignTable foreignTable) {
        // System.out.println("AST_VISITOR -->  foreignTable");
    }

    @Override
    public void visit(UpdateStatementMain updateStatementMain) {
        // System.out.println("AST_VISITOR -->  updateStatementMain");
    }

    @Override
    public void visit(UpdateStatement updateStatement) {
        // System.out.println("AST_VISITOR -->  updateStatement");
    }


    @Override
    public void visit(AggregationFunction aggregationFunction) {
        // System.out.println("AST_VISITOR -->  AggregationFunction");

        // Extracting data received from AST object
        String jarPath = aggregationFunction.getJarPath().getAllowedName().getName();
        String className = aggregationFunction.getClassName().getAllowedName().getName();
        String methodName = aggregationFunction.getMethodName().getAllowedName().getName();
        String aggFunctionName = aggregationFunction.getFunctionName().getName();
        String returnType = aggregationFunction.getReturnType().getType();


        ArrayList<Object> params = new ArrayList<>();
        // =========================================================
        AggFunctionBluePrint aggFunctionBluePrint = new AggFunctionBluePrint();
        aggFunctionBluePrint.setJarPath(jarPath);
        aggFunctionBluePrint.setClassName(className);
        aggFunctionBluePrint.setMethodName(methodName);
        aggFunctionBluePrint.setAggregationFunctionName(aggFunctionName);
        ArrayList<ReturnType> args = aggregationFunction.getAggregationFunctionParameterArray().getReturnTypeArrayList();
        for (ReturnType arg : args) {
            params.add(arg.getType());
            aggFunctionBluePrint.addArg(arg.getType());
        }
        aggFunctionBluePrint.setReturnType(returnType);
        // =========================================================


        // =========================================================
        AggregationFunctionSymbol aggregationFunctionSymbol = new AggregationFunctionSymbol();
        aggregationFunctionSymbol.setJarPath(jarPath);
        aggregationFunctionSymbol.setClassName(className);
        aggregationFunctionSymbol.setMethodName(methodName);
        aggregationFunctionSymbol.setAggregationFunctionName(aggFunctionName);
        aggregationFunctionSymbol.setReturnType(returnType);
        aggregationFunctionSymbol.setParams(params);
        Main.symbolTable.getDeclaredAggregationFunctionSymbol().add(aggregationFunctionSymbol);
        // =========================================================


        // Generating template
        String t = AggregationFunctionTemplate.getTemplate(aggFunctionBluePrint);
        MainMethodTemplate.StaticMainClassBody.append(t);

        System.out.println(aggFunctionBluePrint);
    }

    @Override
    public void visit(JarPath jarPath) {
        // System.out.println("AST_VISITOR -->  JarPath");
    }

    @Override
    public void visit(ClassName className) {
        // System.out.println("AST_VISITOR -->  ClassName");
    }

    @Override
    public void visit(MethodName methodName) {
        // System.out.println("AST_VISITOR -->  MethodName");
    }

    @Override
    public void visit(ReturnType returnType) {
        // System.out.println("AST_VISITOR -->  ReturnType");
    }

    @Override
    public void visit(AggregationFunctionParameterArray aggregationFunctionParameterArray) {
        // System.out.println("AST_VISITOR -->  AggregationFunctionParameterArray");
    }

    @Override
    public void visit(TernaryComparasion ternaryComparasion) {
        // System.out.println("AST_VISITOR -->  Ternary Comparision");
    }

    @Override
    public void visit(CreateTableStatement createTableStatement) {
        // System.out.println("AST_VISITOR -->  CreateTableStatement");

        String className = createTableStatement.getTableName().getName();
        ArrayList<Pair<String, String>> params = createTableStatement.getParams();

        try {
            String fileType = createTableStatement.getFileType().getType();
            String filePath = createTableStatement.getFilePath().getPath();

            fileType = fileType.substring(fileType.indexOf('"'), fileType.lastIndexOf('"') + 1);
            filePath = filePath.substring(filePath.indexOf('"'), filePath.lastIndexOf('"') + 1);

            
            String classTemplate = ClassTemplate.getClassTemplate("public", className, params, true);
            FileHandler.writeJavaFile(className + ".java", classTemplate);


            String loadData = className + ".loadDataFromFile(" + filePath + ", " + fileType + ");\n";

            String mainB = loadData +
                    "\t\t\tprintArrayList(" + className + '.' + className + "sArrayList);\n";
            String tryCatchTemplate = TryCatchTemplate.getTryCatchTemplate(mainB);

            MainMethodTemplate.StaticMainBody.append(tryCatchTemplate);
        } catch (Exception e) {
            System.out.println("NOTE: Please provide correct data, an error on line " + createTableStatement.getLine() + " charAt " + createTableStatement.getCol() + " is detected");
        }
    }

    @Override
    public void visit(CreateTableParams createTableParams) {
        // System.out.println("AST_VISITOR -->  CreateTableParams");
        CreatingDuplicateColumnsInSameTableChecker.check(createTableParams);


    }

    @Override
    public void visit(AllowedCreateTableParam allowedCreateTableParam) {
        // System.out.println("AST_VISITOR -->  Allowed Params");


    }

    @Override
    public void visit(FilePath filePath) {
        // System.out.println("AST_VISITOR -->  FilePath");
    }

    @Override
    public void visit(FileType fileType) {
        // System.out.println("AST_VISITOR -->  FileType");
    }

    @Override
    public void visit(TableParamType tableParamType) {
        // System.out.println("AST_VISITOR -->  TableParamType");
        TableTypeParametersChecker.check(tableParamType);

    }


    @Override
    public void visit(CreateTypeStatement createTypeStatement) {
        // System.out.println("AST_VISITOR --> CreateTypeStatement");
        String className = createTypeStatement.getColumnName().getAllowedName().getName();
        ArrayList<Pair<String, String>> params = createTypeStatement.getParams();
        String classTemplate = ClassTemplate.getClassTemplate("public", className, params, false);
        FileHandler.writeJavaFile(className + ".java", classTemplate);

    }

    @Override
    public void visit(ListOfValues listOfValues) {

    }
}
