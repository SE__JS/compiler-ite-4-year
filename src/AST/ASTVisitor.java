package AST;

import GrammarRules.Parse;
import SingleParse.Expression.*;
import SingleParse.Java.AllowedName;
import SingleParse.Java.AnyStatement.*;
import SingleParse.Java.AnyStatement.TernaryExpression.TernaryComparasion;
import SingleParse.Java.AnyStatement.TernaryExpression.TernaryExpression;
import SingleParse.Java.AnyStatement.IfElse2.ElseStatements;
import SingleParse.Java.AnyStatement.IfElse2.IfElse;
import SingleParse.Java.AnyStatement.IfElse2.IfStatements;
import SingleParse.Java.AnyStatement.LoopStatement.DoWhile;
import SingleParse.Java.AnyStatement.LoopStatement.ForEachStatement;
import SingleParse.Java.AnyStatement.LoopStatement.ForStatement;
import SingleParse.Java.AnyStatement.LoopStatement.WhileStatement;
import SingleParse.Java.AnyStatement.SingleLineStatement.*;
import SingleParse.Java.AnyStatement.SingleLineStatement.TernatyStetement.*;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarArgument;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarAssignment;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarDefinition;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.ArrayAssignment.ArrayAssignment;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.ArrayAssignment.ArrayDefinition;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.ArrayAssignment.ArrayInvoking;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.PossibleValue.*;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.VarOperator;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarStatement;
import SingleParse.Java.AnyStatement.SwitchCase.BreakStatement;
import SingleParse.Java.AnyStatement.SwitchCase.CaseBlock;
import SingleParse.Java.AnyStatement.SwitchCase.DefaultCase;
import SingleParse.Java.AnyStatement.SwitchCase.SwitchStatement;
import SingleParse.Java.FunctionDefinition;
import SingleParse.Java.Functions.*;
import SingleParse.SQL.SqlStmtList.SqlStatement.AggregationFunction.*;
import SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.*;
import SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnDefinitionConstraint.ColumnConstraint.*;
import SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnDefinitionConstraint.ColumnConstraint.ColumnDefault.ColumnDefault;
import SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnDefinitionConstraint.ColumnConstraint.ColumnDefault.ColumnDefaultList;
import SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnDefinitionConstraint.ColumnConstraint.ColumnDefault.ColumnDefaultMain;
import SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnDefinitionConstraint.ColumnConstraintTypeName;
import SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnDefinitionConstraint.ColumnDefinitionConstraint;
// import SingleParse.SQL.SqlStmtList.SqlStatement.CreateTableStatement.CreateTableStatement;
import SingleParse.SQL.SqlStmtList.SqlStatement.CreateTableStatement.CreateTableStmtMain;
import SingleParse.SQL.SqlStmtList.SqlStatement.CreateTableStatement.SingleTableConstraintOrColumnDef;
import SingleParse.SQL.SqlStmtList.SqlStatement.DeleteStatement.DeleteStatement;
import SingleParse.SQL.SqlStmtList.SqlStatement.DeleteStatement.QualifiedTableName.IndexedOrNot;
import SingleParse.SQL.SqlStmtList.SqlStatement.DeleteStatement.QualifiedTableName.IndexedQualifiedTableName;
import SingleParse.SQL.SqlStmtList.SqlStatement.DeleteStatement.QualifiedTableName.NotIndexedQualifiedTableName;
import SingleParse.SQL.SqlStmtList.SqlStatement.DeleteStatement.QualifiedTableName.QualifiedTableName;
import SingleParse.SQL.SqlStmtList.SqlStatement.DropTableStatement.DropTableStatement;
import SingleParse.SQL.SqlStmtList.SqlStatement.FactoredSelectStatement.FactoredMain;
import SingleParse.SQL.SqlStmtList.SqlStatement.FactoredSelectStatement.FactoredSelectStatement;
import SingleParse.SQL.SqlStmtList.SqlStatement.InsertStatement.*;
import SingleParse.SQL.SqlStmtList.SqlStatement.JoinClause.JoinClause;
import SingleParse.SQL.SqlStmtList.SqlStatement.JoinClause.JoinClauseMain;
import SingleParse.SQL.SqlStmtList.SqlStatement.JoinClause.JoinConstraint;
import SingleParse.SQL.SqlStmtList.SqlStatement.JoinClause.JoinOperator;
import SingleParse.SQL.SqlStmtList.SqlStatement.Loader.CreateTableStatement.*;
import SingleParse.SQL.SqlStmtList.SqlStatement.Loader.CreateTypeStatement.CreateTypeStatement;
import SingleParse.SQL.SqlStmtList.SqlStatement.MainSql.*;
import SingleParse.SQL.SqlStmtList.SqlStatement.ResultColumn.ColumnAlias;
import SingleParse.SQL.SqlStmtList.SqlStatement.ResultColumn.ResultColumn;
import SingleParse.SQL.SqlStmtList.SqlStatement.ResultColumn.ResultColumnExpr;
import SingleParse.SQL.SqlStmtList.SqlStatement.ResultColumn.ResultColumnStar;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore.*;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectStatement.SelectStatement;
import SingleParse.SQL.SqlStmtList.SqlStatement.SqlStatement;
import SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.*;
import SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey.ForeignColumnName;
import SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey.ForeignColumnNameList;
import SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey.ForeignKeyClause.ForeignKeyClause;
import SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey.ForeignKeyClause.ForeignKeyClauseList;
import SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey.ForeignKeyClause.ForeignKeyClauseMain.ForeignKeyClauseMain;
import SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey.ForeignKeyClause.ForeignKeyClauseMain.ForeignTable;
import SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey.ForeignKeyClause.ForeignKeyClauseMain.ForeignTargetColumnName;
import SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey.ForeignKeyClause.ForeignKeyClauseMain.ForeignTargetColumnNameList;
import SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey.ForeignKeyClause.ForeignKeyClausePart;
import SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey.TableConstraintForeignKey;
import SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey.TableConstraintForeignKeyBody;
import SingleParse.SQL.SqlStmtList.SqlStatement.UpdateStatement.UpdateStatement;
import SingleParse.SQL.SqlStmtList.SqlStatement.UpdateStatement.UpdateStatementMain;
import SingleParse.SQL.SqlStmtList.SqlStmtList;
import SingleParse.SingleParse;
import SingleParse.SingleParseError;

public interface ASTVisitor {

    void visit(Parse parse);

    void visit(SingleParse singleParse);

    void visit(AnyStatement anyStatement);

    void visit(SingleLineStatement singleLineStatement);

    void visit(WhileStatement whileStatement);

    void visit(ForStatement forStatement);

    void visit(ForEachStatement forEachStatement);

    void visit(ScopeBlock scopeBlock);

    void visit(SwitchStatement switchStatement);

    void visit(IfElse ifElse);

    void visit(IfStatements ifStatements);

    void visit(ElseStatements elseStatements);

    // Single Line Statement Content

    void visit(VarStatement varStatement);


    void visit(PrintStatement printStatement);

    void visit(InvokingFunction invokingFunction);

    void visit(TernaryStatement ternaryStatement);

    void visit(DotBlock dotBlock);

    void visit(ReturnStatement returnStatement);

    void visit(BreakStatement breakStatement);

    void visit(TernaryExpression ternaryExpression);

    //Single Line Statement || var_statement
    void visit(VarAssignment varAssignment);

    void visit(VarDefinition varDefinition);

    //Single Line Statement || var_statement || var_assignment
    void visit(AllowedName allowedName);

    void visit(VarOperator varOperator);

    //Single Line Statement || var_statement || var_assignment || var_operator
    void visit(ArrayAssignment arrayAssignment);

    void visit(PossibleValue possibleValue);

    //Single Line Statement || var_statement || var_assignment || var_operator || array_assignment
    void visit(ArrayDefinition arrayDefinition);

    void visit(DoWhile doWhile);

    void visit(TerminalNode terminalNode);

    //Single Line Statement || var_statement || var_assignment || var_operator || possible_value
    void visit(NummericValue nummericValue);

    void visit(BooleanValue booleanValue);

    void visit(NullValue nullValue);

    void visit(JsonObject jsonObject);

    void visit(Expression expression);

    //possible_value || json_object
    // void visit(KeyValuePair keyValuePair);

    //Single Line Statement || var_statement || var_definition
    void visit(VarArgument varArgument);

    void visit(FunctionDefinition functionDefinition);

    void visit(Argument argument);

    void visit(HigherOrderFunction higherOrderFunction);

    void visit(LeftTernaryStatement leftTernaryStatement);

    void visit(RightTernaryStatement rightTernaryStatement);

    void visit(SinglePlusMinus singlePlusMinus);

    void visit(LeftTernaryRightSide leftTernaryRightSide);

    void visit(RightTernaryRightSide rightTernaryRightSide);

    void visit(ConditionalExpression conditionalExpression);

    void visit(ConditionalParams conditionalParams);

    void visit(CheckingBlock checkingBlock);

    void visit(NextConditionalExpr nextConditionalExpr);

    void visit(CaseBlock caseBlock);

    void visit(DefaultCase defaultCase);

    void visit(Parameter parameter);

    void visit(OptionalParameter optionalParameter);

    void visit(ArrayInvoking arrayInvoking);

    void visit(SingleParseError singleParseError);

    // ==========================================
    void visit(BitwiseOperator bitwiseOperator);

    void visit(LogicalOperator logicalOperator);

    void visit(MathFirstPriorty mathFirstPriorty);

    void visit(MathSecondPriorty mathSecondPriorty);

    void visit(Or or);

    void visit(SqlAndOr sqlAndOr);

    void visit(SqlColumnName sqlColumnName);

    void visit(SqlLogicalOperator sqlLogicalOperator);

    void visit(UnaryOperator unaryOperator);

    // ================ SQL =====================
    void visit(DatabaseName databaseName);

    void visit(LiteralValue literalValue);

    void visit(NewTableName newTableName);

    void visit(SourceTableName sourceTableName);

    void visit(SqlStmtList sqlStmtList);

    void visit(AlterTableAdd alterTableAdd);

    void visit(AlterTableAddConstraint alterTableAddConstraint);

    void visit(AlterTableStatement alterTableStatement);

    void visit(CollationName collationName);

    void visit(CompoundOperator compoundOperator);

    void visit(IndexColumn indexColumn);

    void visit(IndexName indexName);

    void visit(OrderingTerm orderingTerm);

    void visit(SqlStatement sqlStatement);

    void visit(TableAlias tableAlias);

    void visit(TableName tableName);

    void visit(TableOrSubQuery parse);

    void visit(SignedNumberAllowedName signedNumberAllowedName);

    void visit(SignedNumber signedNumber);

    void visit(ColumnName columnName);

    void visit(ColumnDefinition columnDefinition);

    void visit(ColumnDefaultValue columnDefaultValue);

    void visit(ColumnDefinitionConstraint columnDefinitionConstraint);

    void visit(ColumnConstraintTypeName columnConstraintTypeName);

    void visit(ColumnConstraintPrimaryKey columnConstraintPrimaryKey);

    void visit(ColumnConstraintNull columnConstraintNull);

    void visit(ColumnConstraintNotNull columnConstraintNotNull);

    void visit(ColumnConstraintForeignKey columnConstraintForeignKey);

    void visit(ColumnConstraint columnConstraint);

    void visit(ColumnDefaultMain columnDefaultMain);

    void visit(ColumnDefaultList columnDefaultList);

    void visit(ColumnDefault columnDefault);

    // void visit(CreateTableStatement createTableStatement);
    void visit(CreateTableStmtMain createTableStmtMain);

    void visit(SingleTableConstraintOrColumnDef singleTableConstraintOrColumnDef);

    void visit(DeleteStatement deleteStatement);

    void visit(QualifiedTableName qualifiedTableName);

    void visit(NotIndexedQualifiedTableName notIndexedQualifiedTableName);

    void visit(IndexedQualifiedTableName indexedQualifiedTableName);

    void visit(IndexedOrNot indexedOrNot);

    void visit(DropTableStatement dropTableStatement);

    void visit(FactoredMain factoredMain);

    void visit(FactoredSelectStatement factoredSelectStatement);

    void visit(InsertStmtExprRight insertStmtExprRight);

    void visit(InsertStmtExprLeft insertStmtExprLeft);

    void visit(InsertStatementMain insertStatementMain);

    void visit(InsertStatementExpression insertStatementExpression);

    void visit(InsertStatementDefault insertStatementDefault);

    void visit(InsertStatement insertStatement);

    void visit(JoinOperator joinOperator);

    void visit(JoinConstraint joinConstraint);

    void visit(JoinClauseMain joinClauseMain);

    void visit(JoinClause joinClause);

    void visit(ResultColumnStar resultColumnStar);

    void visit(ResultColumnExpr resultColumnExpr);

    void visit(ResultColumn resultColumn);

    void visit(ColumnAlias columnAlias);

    void visit(SelectCoreFirst selectCoreFirst);

    void visit(WhereClause whereClause);

    void visit(GroupByClause groupByClause);

    void visit(HavingClause havingClause);

    void visit(SelectCore selectCore);

    void visit(SelectStatement selectStatement);

    void visit(TableConstraintUnique tableConstraintUnique);

    void visit(TableConstraintPrimaryKey tableConstraintPrimaryKey);

    void visit(TableConstraintKey tableConstraintKey);

    void visit(TableConstraint tableConstraint);

    void visit(IndexedColumnList indexedColumnList);

    void visit(AllowedNameIndexColumn allowedNameIndexColumn);

    void visit(TableConstraintForeignKeyBody tableConstraintForeignKeyBody);

    void visit(TableConstraintForeignKey tableConstraintForeignKey);

    void visit(ForeignColumnNameList foreignColumnNameList);

    void visit(ForeignColumnName foreignColumnName);

    void visit(ForeignKeyClausePart foreignKeyClausePart);

    void visit(ForeignKeyClauseList foreignKeyClauseList);

    void visit(ForeignKeyClause foreignKeyClause);

    void visit(ForeignTargetColumnNameList foreignTargetColumnNameList);

    void visit(ForeignTargetColumnName foreignTargetColumnName);

    void visit(ForeignKeyClauseMain foreignKeyClauseMain);

    void visit(ForeignTable foreignTable);

    void visit(UpdateStatementMain updateStatementMain);

    void visit(UpdateStatement updateStatement);

    void visit(TableOrSubQueryMain tableOrSubQueryMain);

    void visit(TableOrSubQueryLast tableOrSubQueryLast);

    void visit(AggregationFunction aggregationFunction);

    void visit(JarPath jarPath);

    void visit(ClassName className);

    void visit(MethodName methodName);

    void visit(ReturnType returnType);

    void visit(AggregationFunctionParameterArray aggregationFunctionParameterArray);

    void visit(TernaryComparasion ternaryComparasion);


    void visit(CreateTableStatement createTableStatement);

    void visit(CreateTableParams createTableParams);

    void visit(AllowedCreateTableParam allowedCreateTableParam);

    void visit(FilePath filePath);

    void visit(FileType fileType);

    void visit(TableParamType tableParamType);


    void visit(CreateTypeStatement createTypeStatement);
    void visit(ListOfValues listOfValues);
}
