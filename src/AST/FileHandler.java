package AST;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class FileHandler {
    public static void printToFile(String fileName, String err) {
        try {
            FileWriter fw = new FileWriter(fileName, true);
            fw.write(err);
            fw.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void writeJavaFile(String fileName, String classTemplate) {
        try {
            File f = new File("src/CG/GeneratedClasses", fileName);
            if(!f.exists()) {
                f.createNewFile();
            }
            FileWriter fw = new FileWriter(f);
            fw.write(classTemplate);
            fw.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static String readJavaFile(String fileName) {
        try {
            File f = new File("src/CG/GeneratedClasses", fileName);
            FileReader fileReader = new FileReader(f);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            StringBuilder fileContent = new StringBuilder();

            while(bufferedReader.readLine() != null) {
                fileContent.append(bufferedReader.readLine());
            }

            return fileContent.toString();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return "";
    }
}
