// Generated from D:/SoftwareEngineering/4Th/SecSem/Compiler/compiler-ite-4-year\Sql_final.g4 by ANTLR 4.8
package AutoGenerated;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link Sql_finalParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface Sql_finalVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#parse}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParse(Sql_finalParser.ParseContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#single_parse}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSingle_parse(Sql_finalParser.Single_parseContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#error}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitError(Sql_finalParser.ErrorContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#sql_stmt_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSql_stmt_list(Sql_finalParser.Sql_stmt_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#sql_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSql_stmt(Sql_finalParser.Sql_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#factored_select_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFactored_select_stmt(Sql_finalParser.Factored_select_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#factored_main}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFactored_main(Sql_finalParser.Factored_mainContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#select_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelect_stmt(Sql_finalParser.Select_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#expr_or}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr_or(Sql_finalParser.Expr_orContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#expr_math_first_priority_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr_math_first_priority_expr(Sql_finalParser.Expr_math_first_priority_exprContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#expr_math_second_priority}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr_math_second_priority(Sql_finalParser.Expr_math_second_priorityContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#expr_bitwise_operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr_bitwise_operator(Sql_finalParser.Expr_bitwise_operatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#expr_logical_operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr_logical_operator(Sql_finalParser.Expr_logical_operatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#expr_sql_logical_operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr_sql_logical_operator(Sql_finalParser.Expr_sql_logical_operatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#expr_sql_andor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr_sql_andor(Sql_finalParser.Expr_sql_andorContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#listOfValue}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitListOfValue(Sql_finalParser.ListOfValueContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr(Sql_finalParser.ExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#terminal_node}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTerminal_node(Sql_finalParser.Terminal_nodeContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#sql_column_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSql_column_name(Sql_finalParser.Sql_column_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#array_invoking}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArray_invoking(Sql_finalParser.Array_invokingContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#ordering_term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrdering_term(Sql_finalParser.Ordering_termContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#result_column_star}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitResult_column_star(Sql_finalParser.Result_column_starContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#result_column}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitResult_column(Sql_finalParser.Result_columnContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#result_column_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitResult_column_expr(Sql_finalParser.Result_column_exprContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#table_or_subquery}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_or_subquery(Sql_finalParser.Table_or_subqueryContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#table_or_subquery_last}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_or_subquery_last(Sql_finalParser.Table_or_subquery_lastContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#table_or_subquery_main}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_or_subquery_main(Sql_finalParser.Table_or_subquery_mainContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#join_clause_main}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJoin_clause_main(Sql_finalParser.Join_clause_mainContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#join_clause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJoin_clause(Sql_finalParser.Join_clauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#join_operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJoin_operator(Sql_finalParser.Join_operatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#join_constraint}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJoin_constraint(Sql_finalParser.Join_constraintContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#whereClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhereClause(Sql_finalParser.WhereClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#groupByClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGroupByClause(Sql_finalParser.GroupByClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#havingClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHavingClause(Sql_finalParser.HavingClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#select_core}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelect_core(Sql_finalParser.Select_coreContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#select_core_first}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelect_core_first(Sql_finalParser.Select_core_firstContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#compound_operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompound_operator(Sql_finalParser.Compound_operatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#unary_operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnary_operator(Sql_finalParser.Unary_operatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#column_alias}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_alias(Sql_finalParser.Column_aliasContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#keyword}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKeyword(Sql_finalParser.KeywordContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#function_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_name(Sql_finalParser.Function_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#database_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDatabase_name(Sql_finalParser.Database_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#table_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_name(Sql_finalParser.Table_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#column_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_name(Sql_finalParser.Column_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#collation_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCollation_name(Sql_finalParser.Collation_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#index_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndex_name(Sql_finalParser.Index_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#table_alias}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_alias(Sql_finalParser.Table_aliasContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#allowed_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAllowed_name(Sql_finalParser.Allowed_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#var_operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar_operator(Sql_finalParser.Var_operatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#var_arg}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar_arg(Sql_finalParser.Var_argContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#var_definition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar_definition(Sql_finalParser.Var_definitionContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#var_assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar_assignment(Sql_finalParser.Var_assignmentContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#var_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar_statement(Sql_finalParser.Var_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#boolean_value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolean_value(Sql_finalParser.Boolean_valueContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#null_value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNull_value(Sql_finalParser.Null_valueContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#nummeric_value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNummeric_value(Sql_finalParser.Nummeric_valueContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#possible_value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPossible_value(Sql_finalParser.Possible_valueContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#comparasion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComparasion(Sql_finalParser.ComparasionContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#condition_params}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCondition_params(Sql_finalParser.Condition_paramsContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#checking_block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitChecking_block(Sql_finalParser.Checking_blockContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#next_conditional_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNext_conditional_expr(Sql_finalParser.Next_conditional_exprContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#logical_opration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogical_opration(Sql_finalParser.Logical_oprationContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#any_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAny_statement(Sql_finalParser.Any_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#single_line_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSingle_line_statement(Sql_finalParser.Single_line_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#create_type_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreate_type_stmt(Sql_finalParser.Create_type_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#tableParamType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTableParamType(Sql_finalParser.TableParamTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#allowedCreateTableParam}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAllowedCreateTableParam(Sql_finalParser.AllowedCreateTableParamContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#createTableParams}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreateTableParams(Sql_finalParser.CreateTableParamsContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#fileType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFileType(Sql_finalParser.FileTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#filePath}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFilePath(Sql_finalParser.FilePathContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#createTableStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreateTableStatement(Sql_finalParser.CreateTableStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#jarPath}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJarPath(Sql_finalParser.JarPathContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#className}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassName(Sql_finalParser.ClassNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#methodName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethodName(Sql_finalParser.MethodNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#returnType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturnType(Sql_finalParser.ReturnTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#aggregationFunctionParameterArray}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAggregationFunctionParameterArray(Sql_finalParser.AggregationFunctionParameterArrayContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#aggregationFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAggregationFunction(Sql_finalParser.AggregationFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#do_while}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDo_while(Sql_finalParser.Do_whileContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#break_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBreak_stmt(Sql_finalParser.Break_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#return_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturn_stmt(Sql_finalParser.Return_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#scope}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitScope(Sql_finalParser.ScopeContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#print_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrint_statement(Sql_finalParser.Print_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#single_plus_minus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSingle_plus_minus(Sql_finalParser.Single_plus_minusContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#ternary_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTernary_stmt(Sql_finalParser.Ternary_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#leftTernary_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLeftTernary_stmt(Sql_finalParser.LeftTernary_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#leftTernaryRightSide}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLeftTernaryRightSide(Sql_finalParser.LeftTernaryRightSideContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#rightTernary_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRightTernary_stmt(Sql_finalParser.RightTernary_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#rightTernaryRightSide}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRightTernaryRightSide(Sql_finalParser.RightTernaryRightSideContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#ternary_oprator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTernary_oprator(Sql_finalParser.Ternary_opratorContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#for_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFor_statement(Sql_finalParser.For_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#initial_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInitial_statement(Sql_finalParser.Initial_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#single_loop_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSingle_loop_statement(Sql_finalParser.Single_loop_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#conditional_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConditional_expr(Sql_finalParser.Conditional_exprContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#rightSideComparasion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRightSideComparasion(Sql_finalParser.RightSideComparasionContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#enhance_for}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEnhance_for(Sql_finalParser.Enhance_forContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#for_each_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFor_each_statement(Sql_finalParser.For_each_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#while_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhile_statement(Sql_finalParser.While_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#if_else}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_else(Sql_finalParser.If_elseContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#if_statements}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_statements(Sql_finalParser.If_statementsContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#else_statements}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElse_statements(Sql_finalParser.Else_statementsContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#caseBlock}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCaseBlock(Sql_finalParser.CaseBlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#defaultStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefaultStatement(Sql_finalParser.DefaultStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#switchStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSwitchStatement(Sql_finalParser.SwitchStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#dotBlock}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDotBlock(Sql_finalParser.DotBlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#dotBlockRightSide}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDotBlockRightSide(Sql_finalParser.DotBlockRightSideContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#function_definition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_definition(Sql_finalParser.Function_definitionContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#arg_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArg_list(Sql_finalParser.Arg_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#arg}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArg(Sql_finalParser.ArgContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#invoking_function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInvoking_function(Sql_finalParser.Invoking_functionContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#param}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParam(Sql_finalParser.ParamContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#optional_param}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOptional_param(Sql_finalParser.Optional_paramContext ctx);
	/**
	 * Visit a parse tree produced by {@link Sql_finalParser#trExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTrExpression(Sql_finalParser.TrExpressionContext ctx);
}