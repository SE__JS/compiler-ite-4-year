package GrammarRules;

import AST.ASTVisitor;

public abstract class Node {
    private int line;
    private int col;

    public Node(int line, int col) {
        this.line = line;
        this.col = col;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public void setCol(int col) {
        this.col = col;
    }


    public int getCol() {
        return col;
    }

    public abstract void accept(ASTVisitor astVisitor);
}
