package GrammarRules;

import AST.ASTVisitor;
import SingleParse.SingleParse;

import java.util.ArrayList;

public class Parse extends Node {

  ArrayList<SingleParse> singleParseList;

  public ArrayList<SingleParse> getSingleParseList() {
    return singleParseList;
  }

  public void setSingleParseList(ArrayList<SingleParse> singleParseList) {
    this.singleParseList = singleParseList;
  }

  public Parse(int line, int col) {
    super(line, col);
  }

  @Override
  public void accept(ASTVisitor astVisitor) {

    astVisitor.visit(this);


    for (int i = 0; i < singleParseList.size(); i++) {
      singleParseList.get(i).accept(astVisitor);
    }
  }
}
