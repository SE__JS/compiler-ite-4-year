package SingleParse.SQL.SqlStmtList;

import AST.ASTVisitor;
import SingleParse.Java.AnyStatement.AnyStatement;
import SingleParse.SQL.SqlStmtList.SqlStatement.SqlStatement;
import SingleParse.SingleParse;

import java.util.ArrayList;

public class SqlStmtList extends AnyStatement {
    ArrayList<SqlStatement> sqlStatements = new ArrayList<>();
    public SqlStmtList(int line, int col) {
        super(line, col);
    }

    public ArrayList<SqlStatement> getSqlStatements() {
        return sqlStatements;
    }

    public void setSqlStatements(ArrayList<SqlStatement> sqlStatements) {
        this.sqlStatements = sqlStatements;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(sqlStatements != null)
        {
            for(SqlStatement sqlStatement: sqlStatements)
            {
                if(sqlStatement != null) {
                    sqlStatement.accept(astVisitor);
                }
            }
        }

    }
}