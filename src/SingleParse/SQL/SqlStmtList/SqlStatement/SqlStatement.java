package SingleParse.SQL.SqlStmtList.SqlStatement;

import AST.ASTVisitor;
import SingleParse.SQL.SqlStmtList.SqlStatement.FactoredSelectStatement.FactoredSelectStatement;
import SingleParse.SQL.SqlStmtList.SqlStatement.MainSql.TableOrSubQuery;
import SingleParse.SQL.SqlStmtList.SqlStatement.MainSql.TableOrSubQueryLast;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore.SelectCore;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore.SelectCoreFirst;
import SingleParse.SQL.SqlStmtList.SqlStmtList;

public class SqlStatement extends SqlStmtList {
    public SqlStatement(int line, int col) {
        super(line, col);
    }
    public static SelectCore getSelectCore(SqlStatement sqlStatement) {
        FactoredSelectStatement factoredSelectStatement = (FactoredSelectStatement) sqlStatement;
        if(factoredSelectStatement != null) {
            return factoredSelectStatement.getSelectCore();
        }
        return null;
    }
    public static String getTableName(SqlStatement sqlStatement) {
            SelectCore selectCore = getSelectCore(sqlStatement);
            if (selectCore != null) {
                SelectCoreFirst selectCoreFirst = selectCore.getSelectCoreFirst();
                if (selectCoreFirst != null) {
                    try {
                        for (int i = 0; i < selectCoreFirst.getTableOrSubQueries().size(); i++) {
                            TableOrSubQuery tableOrSubQuery = selectCoreFirst.getTableOrSubQueries().get(i);
                            if (tableOrSubQuery != null) {
                                if(tableOrSubQuery.getTableName() != null)
                                    return tableOrSubQuery.getTableName().getAllowedName().getName();
                            }
                        }
                    } catch (Exception eee) {
                        System.out.println(eee);
                        return selectCoreFirst.getJoinClause().getTableOrSubQuery().getTableName().getAllowedName().getName();
                    }
                }
            }
        return "";

    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
    }
}
