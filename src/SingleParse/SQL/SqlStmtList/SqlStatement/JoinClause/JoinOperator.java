package SingleParse.SQL.SqlStmtList.SqlStatement.JoinClause;

import AST.ASTVisitor;
import GrammarRules.Node;

public class JoinOperator extends Node {
    String content;
    public JoinOperator(int line, int col) {
        super(line, col);
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        System.out.println(content);
    }
}
