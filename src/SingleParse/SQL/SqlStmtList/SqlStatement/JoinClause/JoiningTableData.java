package SingleParse.SQL.SqlStmtList.SqlStatement.JoinClause;

import SingleParse.Expression.SingleExpression;

import java.util.ArrayList;

public class JoiningTableData {

    ArrayList<String> allColumns;
    ArrayList<SingleExpression> singleExpressions;
    ArrayList<String> operationsInBetween;

    String firstTableName;
    String secondTableName;

    public ArrayList<String> getAllColumns() {
        return allColumns;
    }

    public void setAllColumns(ArrayList<String> allColumns) {
        this.allColumns = allColumns;
    }

    public ArrayList<SingleExpression> getSingleExpressions() {
        return singleExpressions;
    }

    public void setSingleExpressions(ArrayList<SingleExpression> singleExpressions) {
        this.singleExpressions = singleExpressions;
    }


    public String getFirstTableName() {
        return firstTableName;
    }

    public void setFirstTableName(String firstTableName) {
        this.firstTableName = firstTableName;
    }

    public String getSecondTableName() {
        return secondTableName;
    }

    public void setSecondTableName(String secondTableName) {
        this.secondTableName = secondTableName;
    }

    public ArrayList<String> getOperationsInBetween() {
        return operationsInBetween;
    }

    public void setOperationsInBetween(ArrayList<String> operationsInBetween) {
        this.operationsInBetween = operationsInBetween;
    }

    @Override
    public String toString() {
        return "JoiningTableData{" +
                "allColumns=" + allColumns +
                ", singleExpressions=" + singleExpressions +
                ", operationsInBetween=" + operationsInBetween +
                ", firstTableName='" + firstTableName + '\'' +
                ", secondTableName='" + secondTableName + '\'' +
                '}';
    }
}
