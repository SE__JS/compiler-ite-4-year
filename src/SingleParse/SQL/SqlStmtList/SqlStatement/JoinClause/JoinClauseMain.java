package SingleParse.SQL.SqlStmtList.SqlStatement.JoinClause;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.SQL.SqlStmtList.SqlStatement.MainSql.TableOrSubQuery;

public class JoinClauseMain extends Node {
    JoinOperator joinOperator;
    TableOrSubQuery tableOrSubQuery;
    JoinConstraint joinConstraint;
    public JoinClauseMain(int line, int col) {
        super(line, col);
    }

    public JoinOperator getJoinOperator() {
        return joinOperator;
    }

    public void setJoinOperator(JoinOperator joinOperator) {
        this.joinOperator = joinOperator;
    }

    public TableOrSubQuery getTableOrSubQuery() {
        return tableOrSubQuery;
    }

    public void setTableOrSubQuery(TableOrSubQuery tableOrSubQuery) {
        this.tableOrSubQuery = tableOrSubQuery;
    }

    public JoinConstraint getJoinConstraint() {
        return joinConstraint;
    }

    public void setJoinConstraint(JoinConstraint joinConstraint) {
        this.joinConstraint = joinConstraint;
    }



    public static boolean hasOnInJoinConstraint(JoinClauseMain joinClauseMain) {
        JoinConstraint joinConstraint = joinClauseMain.getJoinConstraint();
        if(joinConstraint != null) {
            return joinConstraint.getON() != null;
        }
        return false;
    }
    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(joinOperator != null)
        {
            joinOperator.accept(astVisitor);
        }
        if(tableOrSubQuery != null)
        {
            tableOrSubQuery.accept(astVisitor);
        }
        if(joinConstraint != null)
        {
            joinConstraint.accept(astVisitor);
        }
    }
}
