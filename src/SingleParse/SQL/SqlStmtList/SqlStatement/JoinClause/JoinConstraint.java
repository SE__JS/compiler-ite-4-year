package SingleParse.SQL.SqlStmtList.SqlStatement.JoinClause;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.Expression.Expression;

public class JoinConstraint extends Node {
    Expression expression;
    String ON;
    public JoinConstraint(int line, int col) {
        super(line, col);
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);

        if(expression != null)
        {
            expression.accept(astVisitor);
        }
        if(ON != null) {
            System.out.println("AST_VISITOR -> ON is presented in JoinConstraint");
        }
    }

    public String getON() {
        return ON;
    }

    public void setON(String ON) {
        this.ON = ON;
    }
}
