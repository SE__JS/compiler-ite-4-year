package SingleParse.SQL.SqlStmtList.SqlStatement.JoinClause;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.SQL.SqlStmtList.SqlStatement.MainSql.TableOrSubQuery;

import java.util.ArrayList;

public class JoinClause extends Node {
    ArrayList<JoinClauseMain> joinClauseMains;
    TableOrSubQuery tableOrSubQuery;
    public JoinClause(int line, int col) {
        super(line, col);
    }

    public TableOrSubQuery getTableOrSubQuery() {
        return tableOrSubQuery;
    }

    public void setTableOrSubQuery(TableOrSubQuery tableOrSubQuery) {
        this.tableOrSubQuery = tableOrSubQuery;
    }

    public ArrayList<JoinClauseMain> getJoinClauseMains() {
        return joinClauseMains;
    }

    public void setJoinClauseMains(ArrayList<JoinClauseMain> joinClauseMains) {
        this.joinClauseMains = joinClauseMains;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(tableOrSubQuery != null)
        {
            tableOrSubQuery.accept(astVisitor);
        }


        if(joinClauseMains != null)
        {
            for(JoinClauseMain joinClauseMain: joinClauseMains)
            {
                joinClauseMain.accept(astVisitor);

            }

        }

    }
}
