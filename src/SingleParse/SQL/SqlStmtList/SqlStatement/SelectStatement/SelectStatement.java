package SingleParse.SQL.SqlStmtList.SqlStatement.SelectStatement;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.Expression.Expression;
import SingleParse.SQL.SqlStmtList.SqlStatement.MainSql.OrderingTerm;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore.SelectCore;

import java.util.ArrayList;

public class SelectStatement extends Node {
    SelectCore selectCore;
    ArrayList<Expression> expressions;
    ArrayList<OrderingTerm> orderingTerms;
    public SelectStatement(int line, int col) {
        super(line, col);
    }

    public SelectCore getSelectCore() {
        return selectCore;
    }

    public void setSelectCore(SelectCore selectCore) {
        this.selectCore = selectCore;
    }

    public ArrayList<Expression> getExpressions() {
        return expressions;
    }

    public void setExpressions(ArrayList<Expression> expressions) {
        this.expressions = expressions;
    }

    public ArrayList<OrderingTerm> getOrderingTerms() {
        return orderingTerms;
    }

    public void setOrderingTerms(ArrayList<OrderingTerm> orderingTerms) {
        this.orderingTerms = orderingTerms;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(selectCore != null)
        {
            selectCore.accept(astVisitor);
        }
        if(orderingTerms != null)
        {
            for(OrderingTerm orderingTerm: orderingTerms)
            {
                orderingTerm.accept(astVisitor);

            }

        }
        if(expressions != null)
        {
            for(Expression expression: expressions)
            {
                expression.accept(astVisitor);

            }

        }
    }
}
