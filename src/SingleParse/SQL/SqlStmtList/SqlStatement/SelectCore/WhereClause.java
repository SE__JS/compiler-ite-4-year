package SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.Expression.Expression;
import SingleParse.Java.AnyStatement.SingleLineStatement.TerminalNode;
import SingleParse.Java.AnyStatement.TernaryExpression.TernaryExpression;

public class WhereClause extends Node {
    Expression expression;

    public WhereClause(int line, int col) {
        super(line, col);
    }

    @Override
    public int getLine() {
        return super.getLine();
    }

    @Override
    public void setLine(int line) {
        super.setLine(line);
    }

    @Override
    public void setCol(int col) {
        super.setCol(col);
    }

    @Override
    public int getCol() {
        return super.getCol();
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if (expression != null) {
            expression.accept(astVisitor);
        }
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    public boolean hasSubQueryInIt() {
        Expression mainExpr = getExpression();
        if (mainExpr != null) {
            try {
                Expression subExpr = mainExpr.getExpressionArrayList().get(1);
                TernaryExpression ternaryExpression = subExpr.getTernaryExpression();
                TerminalNode trNode = null;
                if(ternaryExpression.getTerminalNode() != null) {
                    trNode = ternaryExpression.getTerminalNode();
                } else {
                    trNode = ternaryExpression.getTernaryExpressionArrayList().get(0).getTerminalNode();
                }
                if(trNode.getSqlStatement() != null) return true;
                return false;
            } catch (Exception e) {
                return false;
            }
        }
        return false;
    }
}
