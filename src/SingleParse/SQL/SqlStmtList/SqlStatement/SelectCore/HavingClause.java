package SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.Expression.Expression;

public class HavingClause extends Node {
    Expression expression;
    public HavingClause(int line, int col) {
        super(line, col);
    }

    @Override
    public int getLine() {
        return super.getLine();
    }

    @Override
    public void setLine(int line) {
        super.setLine(line);
    }

    @Override
    public void setCol(int col) {
        super.setCol(col);
    }

    @Override
    public int getCol() {
        return super.getCol();
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(expression != null) {
            expression.accept(astVisitor);
        }
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }
}
