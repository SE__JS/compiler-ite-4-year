package SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.SQL.SqlStmtList.SqlStatement.JoinClause.JoinClause;
import SingleParse.SQL.SqlStmtList.SqlStatement.MainSql.TableOrSubQuery;

import java.util.ArrayList;

public class SelectCoreFirst extends Node {
    ArrayList<TableOrSubQuery> tableOrSubQueries;
    JoinClause joinClause;
    public SelectCoreFirst(int line, int col) {
        super(line, col);
    }

    public ArrayList<TableOrSubQuery> getTableOrSubQueries() {
        return tableOrSubQueries;
    }

    public void setTableOrSubQueries(ArrayList<TableOrSubQuery> tableOrSubQueries) {
        this.tableOrSubQueries = tableOrSubQueries;
    }

    public JoinClause getJoinClause() {
        return joinClause;
    }

    public void setJoinClause(JoinClause joinClause) {
        this.joinClause = joinClause;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(tableOrSubQueries != null)
        {
            for(TableOrSubQuery tableOrSubQuery: tableOrSubQueries)
            {
                if(tableOrSubQuery != null)
                {
                    tableOrSubQuery.accept(astVisitor);
                }
            }
        }
        if(joinClause != null)
        {
            joinClause.accept(astVisitor);
        }
    }
}
