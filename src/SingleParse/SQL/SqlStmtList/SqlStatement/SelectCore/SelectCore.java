package SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore;

import AST.ASTVisitor;
import AST.Main;
import GrammarRules.Node;
import SingleParse.Expression.Expression;
import SingleParse.Expression.SingleExpression;
import SingleParse.Expression.SqlColumnName;
import SingleParse.Java.AllowedName;
import SingleParse.SQL.SqlStmtList.SqlStatement.FactoredSelectStatement.FactoredSelectStatement;
import SingleParse.SQL.SqlStmtList.SqlStatement.InsertStatement.InsertStmtExprLeft;
import SingleParse.SQL.SqlStmtList.SqlStatement.InsertStatement.InsertStmtExprRight;
import SingleParse.SQL.SqlStmtList.SqlStatement.JoinClause.JoinClause;
import SingleParse.SQL.SqlStmtList.SqlStatement.JoinClause.JoinClauseMain;
import SingleParse.SQL.SqlStmtList.SqlStatement.JoinClause.JoinConstraint;
import SingleParse.SQL.SqlStmtList.SqlStatement.JoinClause.JoiningTableData;
import SingleParse.SQL.SqlStmtList.SqlStatement.MainSql.TableOrSubQuery;
import SingleParse.SQL.SqlStmtList.SqlStatement.ResultColumn.ResultColumn;
import SingleParse.SQL.SqlStmtList.SqlStatement.ResultColumn.ResultColumnExpr;
import SingleParse.SQL.SqlStmtList.SqlStatement.SqlStatement;
import SymbolTable.Type;
import com.sun.tracing.dtrace.FunctionName;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class SelectCore extends Node {
    SelectCoreFirst selectCoreFirst;
    ArrayList<ResultColumn> resultColumns;
    WhereClause whereClause;
    GroupByClause groupByClause;
    HavingClause havingClause;

    //ArrayList<Expression> expressions;
    //InsertStmtExprLeft insertStmtExprLeft;
    //ArrayList<InsertStmtExprRight> insertStmtExprRights;
    public static String getTableInThisQuery(SelectCore selectCore) {
        SelectCoreFirst selectCoreFirst = selectCore.getSelectCoreFirst();
        if (selectCoreFirst != null) {
            ArrayList<TableOrSubQuery> tableOrSubQueries = selectCoreFirst.getTableOrSubQueries();
            if (tableOrSubQueries != null) {
                for (int i = 0; i < tableOrSubQueries.size(); i++) {
                    TableOrSubQuery tableOrSubQuery = tableOrSubQueries.get(i);
                    if (tableOrSubQuery.getTableName() != null) {
                        return tableOrSubQuery.getTableName().getAllowedName().getName();
                    }

                }
            }
        }
        return "";
    }

    public static ArrayList<Pair<String, String>> getAggregationFunctionsInQuery(SelectCore selectCore) {
        ArrayList<Pair<String, String>> res = new ArrayList<>();
        if (selectCore.getResultColumns() != null) {
            for (int i = 0; i < selectCore.getResultColumns().size(); i++) {
                Pair<String, String> p = null;
                ResultColumn resultColumn = selectCore.getResultColumns().get(i);
                if (resultColumn != null) {
                    ResultColumnExpr resultColumnExpr = resultColumn.getResultColumnExpr();
                    if (resultColumnExpr != null) {
                        Expression expression = resultColumnExpr.getExpression();
                        if (expression != null) {
                            if (expression.getFunctionName() != null) {
                                String functionName = expression.getFunctionName().getName();
                                try {
                                    StringBuilder dataName = new StringBuilder();
                                    Expression subExpr = expression.getExpressionArrayList().get(0);
                                    if (subExpr.getSqlColumnName() != null) {
                                        if (subExpr.getSqlColumnName().getTableName() != null) {
                                            dataName.append(subExpr.getSqlColumnName().getTableName().getAllowedName().getName());
                                            dataName.append('.');
                                        }
                                        dataName.append(subExpr.getSqlColumnName().getColumnName().getAllowedName().getName());
                                    }
                                    p = new Pair<>(functionName, dataName.toString());

                                } catch (Exception e) {
                                    System.out.println("Exception: " + e);
                                    p = new Pair<>(functionName, "*");
                                }
                            }

                        }
                    }
                }
                if (p != null) {
                    res.add(p);
                }
            }
        }
        return res;
    }

    public static ArrayList<Pair<String, String>> getSqlColumnsThisQuery(SelectCore selectCore) {
        ArrayList<Pair<String, String>> sqlColumns = new ArrayList<>();
        if (selectCore.getResultColumns() != null) {
            for (int i = 0; i < selectCore.getResultColumns().size(); i++) {
                Pair<String, String> p = null;
                ResultColumn resultColumn = selectCore.getResultColumns().get(i);
                if (resultColumn != null) {
                    ResultColumnExpr resultColumnExpr = resultColumn.getResultColumnExpr();
                    if (resultColumnExpr != null) {
                        Expression expression = resultColumnExpr.getExpression();
                        if (expression != null) {
                            SqlColumnName sqlColumnName = expression.getSqlColumnName();
                            if (sqlColumnName != null) {
                                String tName = sqlColumnName.getTableName().getAllowedName().getName();
                                String cName = sqlColumnName.getColumnName().getAllowedName().getName();
                                p = new Pair<>(tName, cName);
                            }
                        }

                    }
                }
                if (p != null) {
                    sqlColumns.add(p);
                }
            }
        }
        return sqlColumns;
    }

    public static ArrayList<String> getColumnsRequestedInThisQuery(SelectCore selectCore) {
        ArrayList<String> cols = new ArrayList<>();
        if (selectCore.getResultColumns() != null) {
            for (int i = 0; i < selectCore.getResultColumns().size(); i++) {
                ResultColumn resultColumn = selectCore.getResultColumns().get(i);
                if (resultColumn.getAllowedName() != null) {
                    cols.add(resultColumn.getAllowedName().getName());
                }
            }
        }
        return cols;
    }

    public static ArrayList<String> getColumnsFromType(String type) throws Exception {

        Type tt = Main.symbolTable.getTypeByName(type);
        if (tt == null) {
            tt = Main.symbolTable.getTypeFromProvidedSymbols(type);
            if (tt == null) {
                throw new Exception("Un-supported type: " + type);
            }
        }


        ArrayList<String> res = new ArrayList<>();
        LinkedHashMap<String, Type> cols = (LinkedHashMap<String, Type>) tt.getColumns();

        for (Map.Entry<String, Type> col : cols.entrySet()) {
            if(col.getValue().isAgg) continue;
            if (col.getValue().isPrimitive()) {
                res.add(tt.getName() + '.' + col.getKey());
            } else {
                Type stt = Main.symbolTable.getTypeByName(col.getValue().getName());
                LinkedHashMap<String, Type> flas = stt.flat(col.getKey());
                for (Map.Entry<String, Type> fla : flas.entrySet()) {
                    String[] str = fla.getKey().split("\\.");
                    res.add(tt.getName() + '.' + fla.getKey());
                }
            }
        }

        System.out.println("DONE: " + res);
        return res;
    }

    public static JoiningTableData getJoiningData(SelectCore selectCore) {
        JoiningTableData joiningTableData = new JoiningTableData();
        if (selectCore.getSelectCoreFirst() != null) {
            SelectCoreFirst scf = selectCore.getSelectCoreFirst();
            if (scf.getJoinClause() != null) {
                JoinClause jc = scf.getJoinClause();
                if (jc.getJoinClauseMains() != null) {
                    JoinClauseMain jcm = jc.getJoinClauseMains().get(0);
                    try {
                        String firstTableName = jc.getTableOrSubQuery().getTableName().getAllowedName().getName();
                        String secondTableName = jcm.getTableOrSubQuery().getTableName().getAllowedName().getName();

                        System.out.println("firstTableName: " + firstTableName);
                        System.out.println("secondTableName: " + secondTableName);

                        // 2
                        joiningTableData.setFirstTableName(firstTableName);

                        // 2
                        joiningTableData.setSecondTableName(secondTableName);

                        System.out.println("OWELEEEEEEEEEEEEEEEEEEEEE");
                        // 3
                        ArrayList<String> firstTableColumns = getColumnsFromType(firstTableName);
                        ArrayList<String> secondTableColumns = getColumnsFromType(secondTableName);

                        
                        System.out.println("firstTableColumns: " + secondTableColumns);
                        System.out.println("secondTableColumns: " + secondTableColumns);

                        firstTableColumns.addAll(secondTableColumns);
                        joiningTableData.setAllColumns(firstTableColumns);



                        JoinConstraint joinConstraint = jcm.getJoinConstraint();
                        if (jcm.getJoinConstraint() != null && jcm.getJoinConstraint().getExpression() != null) {
                            Expression expr = joinConstraint.getExpression();

                            ArrayList<SingleExpression> ses = new ArrayList<>();
                            ArrayList<String> oprs = new ArrayList<>();
                            expr.getSingleExpressions(ses, oprs);

                            // 4
                            joiningTableData.setSingleExpressions(ses);
                            // 5
                            joiningTableData.setOperationsInBetween(oprs);

                        }
                    } catch (Exception ee) {
                        System.out.println("Error: " + ee);
                        return null;
                    }

                }
            }
        }
        return joiningTableData;
    }

    public static int totalCounterOfColumns(SelectCore selectCore) {
        if (selectCore.getResultColumns() != null) {
            int sz = selectCore.getResultColumns().size();
            if (sz == 1) {
                if (selectCore.getResultColumns().get(0).getResultColumnStar() != null) {
                    return 0;
                }
            }
            return sz;
        }
        return 0;
    }

    public SelectCore(int line, int col) {
        super(line, col);
    }

    public SelectCoreFirst getSelectCoreFirst() {
        return selectCoreFirst;
    }

    public void setSelectCoreFirst(SelectCoreFirst selectCoreFirst) {
        this.selectCoreFirst = selectCoreFirst;
    }

    public ArrayList<ResultColumn> getResultColumns() {
        return resultColumns;
    }

    public void setResultColumns(ArrayList<ResultColumn> resultColumns) {
        this.resultColumns = resultColumns;
    }

//    public ArrayList<Expression> getExpressions() {
//        return expressions;
//    }
//
//    public void setExpressions(ArrayList<Expression> expressions) {
//        this.expressions = expressions;
//    }
//
//    public InsertStmtExprLeft getInsertStmtExprLeft() {
//        return insertStmtExprLeft;
//    }
//
//    public void setInsertStmtExprLeft(InsertStmtExprLeft insertStmtExprLeft) {
//        this.insertStmtExprLeft = insertStmtExprLeft;
//    }
//
//    public ArrayList<InsertStmtExprRight> getInsertStmtExprRights() {
//        return insertStmtExprRights;
//    }
//
//    public void setInsertStmtExprRights(ArrayList<InsertStmtExprRight> insertStmtExprRights) {
//        this.insertStmtExprRights = insertStmtExprRights;
//    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if (resultColumns != null) {
            for (ResultColumn resultColumn : resultColumns) {
                if (resultColumn != null) {
                    resultColumn.accept(astVisitor);
                }


            }

        }
        if (selectCoreFirst != null) {
            selectCoreFirst.accept(astVisitor);
        }

        if (whereClause != null) {
            whereClause.accept(astVisitor);
        }

        if (groupByClause != null) {
            groupByClause.accept(astVisitor);
        }
        if (havingClause != null) {
            havingClause.accept(astVisitor);
        }
//        if(expressions != null)
//        {
//            for(Expression expression: expressions)
//            {
//                expression.accept(astVisitor);
//            }
//        }
//        if(insertStmtExprLeft != null)
//        {
//            insertStmtExprLeft.accept(astVisitor);
//        }
//        if(insertStmtExprRights != null)
//        {
//            for(InsertStmtExprRight insertStmtExprRight: insertStmtExprRights)
//            {
//                insertStmtExprRight.accept(astVisitor);
//            }
//        }
    }

    public WhereClause getWhereClause() {
        return whereClause;
    }

    public void setWhereClause(WhereClause whereClause) {
        this.whereClause = whereClause;
    }

    public GroupByClause getGroupByClause() {
        return groupByClause;
    }

    public void setGroupByClause(GroupByClause groupByClause) {
        this.groupByClause = groupByClause;
    }

    public HavingClause getHavingClause() {
        return havingClause;
    }

    public void setHavingClause(HavingClause havingClause) {
        this.havingClause = havingClause;
    }
}
