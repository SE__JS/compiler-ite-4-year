package SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.Expression.Expression;
import SingleParse.Expression.SqlColumnName;

import java.util.ArrayList;

public class GroupByClause extends Node {
    ArrayList<Expression> expressions;

    public GroupByClause(int line, int col) {
        super(line, col);
    }

    @Override
    public int getLine() {
        return super.getLine();
    }

    @Override
    public void setLine(int line) {
        super.setLine(line);
    }

    @Override
    public void setCol(int col) {
        super.setCol(col);
    }

    @Override
    public int getCol() {
        return super.getCol();
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if (expressions != null && expressions.size() > 0) {
            for (Expression expression : expressions) {
                expression.accept(astVisitor);
            }
        }
    }

    public ArrayList<Expression> getExpressions() {
        return expressions;
    }

    public void setExpressions(ArrayList<Expression> expressions) {
        this.expressions = expressions;
    }

    public static ArrayList<String> getGroupByExpressions(GroupByClause groupByClause) {
        ArrayList<String> res = new ArrayList<>();
        if(groupByClause != null) {
            if(groupByClause.expressions != null) {
                ArrayList<Expression> exprs = groupByClause.expressions;
                for(Expression expr: exprs) {
                    if(expr.getSqlColumnName() != null) {
                        SqlColumnName sqlColumnName = expr.getSqlColumnName();
                        String toAdd = "";
                        if(sqlColumnName.getTableName() != null) {
                            toAdd+= sqlColumnName.getTableName().getAllowedName().getName() + '.';
                        }
                        if(sqlColumnName.getColumnName() != null) {
                            toAdd += sqlColumnName.getColumnName().getAllowedName().getName();
                        }
                        res.add(toAdd);
                    }
                }
            }
        }

        return res;
    }
}
