package SingleParse.SQL.SqlStmtList.SqlStatement.Loader.CreateTableStatement;

import AST.ASTVisitor;
import GrammarRules.Node;

import java.util.ArrayList;

public class CreateTableParams extends Node {

    ArrayList<AllowedCreateTableParam> allowedCreateTableParamArrayList;

    public CreateTableParams(int line, int col) {
        super(line, col);
    }

    @Override
    public int getLine() {
        return super.getLine();
    }

    @Override
    public void setLine(int line) {
        super.setLine(line);
    }

    @Override
    public void setCol(int col) {
        super.setCol(col);
    }

    @Override
    public int getCol() {
        return super.getCol();
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(allowedCreateTableParamArrayList != null && allowedCreateTableParamArrayList.size() > 0) {
            for(int i = 0; i < allowedCreateTableParamArrayList.size(); i++) {
                allowedCreateTableParamArrayList.get(i).accept(astVisitor);
            }
        }
    }

    public ArrayList<AllowedCreateTableParam> getAllowedCreateTableParamArrayList() {
        return allowedCreateTableParamArrayList;
    }

    public void setAllowedCreateTableParamArrayList(ArrayList<AllowedCreateTableParam> allowedCreateTableParamArrayList) {
        this.allowedCreateTableParamArrayList = allowedCreateTableParamArrayList;
    }
}
