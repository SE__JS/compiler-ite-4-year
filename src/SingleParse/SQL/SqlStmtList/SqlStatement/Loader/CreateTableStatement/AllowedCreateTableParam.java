package SingleParse.SQL.SqlStmtList.SqlStatement.Loader.CreateTableStatement;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.Java.AllowedName;

public class AllowedCreateTableParam  extends Node {
    AllowedName allowedName;
    TableParamType tableParamType;

    public AllowedCreateTableParam(int line, int col) {
        super(line, col);
    }

    @Override
    public int getLine() {
        return super.getLine();
    }

    @Override
    public void setLine(int line) {
        super.setLine(line);
    }

    @Override
    public void setCol(int col) {
        super.setCol(col);
    }

    @Override
    public int getCol() {
        return super.getCol();
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(allowedName != null) {
            allowedName.accept(astVisitor);
        }
        if(tableParamType != null) {
            tableParamType.accept(astVisitor);
        }
    }

    public AllowedName getAllowedName() {
        return allowedName;
    }

    public void setAllowedName(AllowedName allowedName) {
        this.allowedName = allowedName;
    }

    public TableParamType getTableParamType() {
        return tableParamType;
    }

    public void setTableParamType(TableParamType tableParamType) {
        this.tableParamType = tableParamType;
    }
}
