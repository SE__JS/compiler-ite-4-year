package SingleParse.SQL.SqlStmtList.SqlStatement.Loader.CreateTableStatement;

import AST.ASTVisitor;
import GrammarRules.Node;

public class FilePath extends Node {
    String path;
    public FilePath(int line, int col) {
        super(line, col);
    }

    @Override
    public int getLine() {
        return super.getLine();
    }

    @Override
    public void setLine(int line) {
        super.setLine(line);
    }

    @Override
    public void setCol(int col) {
        super.setCol(col);
    }

    @Override
    public int getCol() {
        return super.getCol();
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(path != null) {
            System.out.println("AST_VISITOR --> Path: " + path);
        }
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
