package SingleParse.SQL.SqlStmtList.SqlStatement.Loader.CreateTableStatement;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.Java.AllowedName;
import SingleParse.Java.AnyStatement.SingleLineStatement.SingleLineStatement;
import javafx.util.Pair;

import java.util.ArrayList;

public class CreateTableStatement  extends SingleLineStatement {
    AllowedName tableName;
    CreateTableParams createTableParams;
    FileType fileType;
    FilePath filePath;
    public CreateTableStatement(int line, int col) {
        super(line, col);
    }

    @Override
    public int getLine() {
        return super.getLine();
    }

    @Override
    public void setLine(int line) {
        super.setLine(line);
    }

    @Override
    public void setCol(int col) {
        super.setCol(col);
    }

    @Override
    public int getCol() {
        return super.getCol();
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(createTableParams != null) {
            createTableParams.accept(astVisitor);
        }
        if(tableName != null) {
            tableName.accept(astVisitor);
        }
        if(filePath != null) {
            filePath.accept(astVisitor);
        }
        if(fileType != null) {
            fileType.accept(astVisitor);
        }
    }

    public AllowedName getTableName() {
        return tableName;
    }

    public void setTableName(AllowedName tableName) {
        this.tableName = tableName;
    }

    public CreateTableParams getCreateTableParams() {
        return createTableParams;
    }

    public void setCreateTableParams(CreateTableParams createTableParams) {
        this.createTableParams = createTableParams;
    }

    public FileType getFileType() {
        return fileType;
    }

    public void setFileType(FileType fileType) {
        this.fileType = fileType;
    }

    public FilePath getFilePath() {
        return filePath;
    }

    public void setFilePath(FilePath filePath) {
        this.filePath = filePath;
    }


    public ArrayList<Pair<String, String>> getParams() {
        ArrayList<Pair<String, String>> ans = new ArrayList<>();
        if(createTableParams != null) {
            ArrayList<AllowedCreateTableParam> params = createTableParams.getAllowedCreateTableParamArrayList();
            if(params != null) {
                for(AllowedCreateTableParam param: params) {
                    String paramName = param.getAllowedName().getName();
                    String paramType = param.getTableParamType().getType();
                    Pair<String, String> p = new Pair<>(paramType, paramName);
                    ans.add(p);
                }
            }
        }
        return ans;
    }
}
