package SingleParse.SQL.SqlStmtList.SqlStatement.Loader.CreateTableStatement;

import AST.ASTVisitor;
import GrammarRules.Node;

public class FileType extends Node {
    String type;
    public FileType(int line, int col) {
        super(line, col);
    }

    @Override
    public int getLine() {
        return super.getLine();
    }

    @Override
    public void setLine(int line) {
        super.setLine(line);
    }

    @Override
    public void setCol(int col) {
        super.setCol(col);
    }

    @Override
    public int getCol() {
        return super.getCol();
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(type != null) {
            System.out.println("AST_VISITOR --> type: " + type);
        }
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
