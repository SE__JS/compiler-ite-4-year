package SingleParse.SQL.SqlStmtList.SqlStatement.Loader.CreateTypeStatement;

import AST.ASTVisitor;

import SingleParse.Java.AllowedName;
import SingleParse.Java.AnyStatement.SingleLineStatement.SingleLineStatement;
import SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnName;
import SingleParse.SQL.SqlStmtList.SqlStatement.Loader.CreateTableStatement.AllowedCreateTableParam;
import SingleParse.SQL.SqlStmtList.SqlStatement.Loader.CreateTableStatement.CreateTableParams;
import javafx.util.Pair;
import oracle.jrockit.jfr.StringConstantPool;

import java.util.ArrayList;

public class CreateTypeStatement extends SingleLineStatement {
    ColumnName columnName;
    CreateTableParams createTableParams;
    public CreateTypeStatement(int line, int col) {
        super(line, col);
    }

    @Override
    public int getLine() {
        return super.getLine();
    }

    @Override
    public void setLine(int line) {
        super.setLine(line);
    }

    @Override
    public void setCol(int col) {
        super.setCol(col);
    }

    @Override
    public int getCol() {
        return super.getCol();
    }



    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
       if(columnName!=null){
           columnName.accept(astVisitor);
       }
       if(createTableParams!=null){
           createTableParams.accept(astVisitor);
       }
    }

    public ColumnName getColumnName() {
        return columnName;
    }

    public void setColumnName(ColumnName columnName) {
        this.columnName = columnName;
    }

    public CreateTableParams getCreateTableParams() {
        return createTableParams;
    }

    public void setCreateTableParams(CreateTableParams createTableParams) {
        this.createTableParams = createTableParams;
    }

    public ArrayList<Pair<String, String>> getParams() {
        ArrayList<Pair<String, String>> ans = new ArrayList<>();
        if(createTableParams != null) {

            ArrayList<AllowedCreateTableParam> params = createTableParams.getAllowedCreateTableParamArrayList();
            if(params != null) {
                for(AllowedCreateTableParam param: params) {
                    String paramName = param.getAllowedName().getName();
                    String paramType = param.getTableParamType().getType();
                    Pair<String, String> p = new Pair<>(paramType, paramName);
                    ans.add(p);
                }
            }
        }
        return ans;
    }
}
