package SingleParse.SQL.SqlStmtList.SqlStatement.FactoredSelectStatement;

import AST.ASTVisitor;
import SingleParse.Expression.Expression;
import SingleParse.SQL.SqlStmtList.SqlStatement.MainSql.OrderingTerm;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore.SelectCore;
import SingleParse.SQL.SqlStmtList.SqlStatement.SqlStatement;

import java.util.ArrayList;

public class FactoredSelectStatement extends SqlStatement {
    SelectCore selectCore;
    ArrayList<FactoredMain> factoredMains;
    ArrayList<OrderingTerm> orderingTerms;
    ArrayList<Expression> expressions;
    public FactoredSelectStatement(int line, int col) {
        super(line, col);
    }

    public SelectCore getSelectCore() {
        return selectCore;
    }

    public void setSelectCore(SelectCore selectCore) {
        this.selectCore = selectCore;
    }

    public ArrayList<FactoredMain> getFactoredMains() {
        return factoredMains;
    }

    public void setFactoredMains(ArrayList<FactoredMain> factoredMains) {
        this.factoredMains = factoredMains;
    }

    public ArrayList<OrderingTerm> getOrderingTerms() {
        return orderingTerms;
    }

    public void setOrderingTerms(ArrayList<OrderingTerm> orderingTerms) {
        this.orderingTerms = orderingTerms;
    }

    public ArrayList<Expression> getExpressions() {
        return expressions;
    }

    public void setExpressions(ArrayList<Expression> expressions) {
        this.expressions = expressions;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(selectCore != null)
        {
            selectCore.accept(astVisitor);

        }

        if(factoredMains != null)
        {
            for(FactoredMain factoredMain: factoredMains)
            {
                if(factoredMain != null)
                    factoredMain.accept(astVisitor);

            }

        }
        if(orderingTerms != null)
        {
            for(OrderingTerm orderingTerm: orderingTerms)
            {
                if(orderingTerm != null)
                    orderingTerm.accept(astVisitor);

            }

        }
        if(expressions != null)
        {
            for(Expression expression: expressions)
            {
                if(expression != null)
                    expression.accept(astVisitor);

            }

        }
    }
}
