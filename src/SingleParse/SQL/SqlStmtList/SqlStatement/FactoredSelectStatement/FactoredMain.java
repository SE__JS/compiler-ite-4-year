package SingleParse.SQL.SqlStmtList.SqlStatement.FactoredSelectStatement;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.SQL.SqlStmtList.SqlStatement.MainSql.CompoundOperator;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore.SelectCore;

public class FactoredMain extends Node {
    CompoundOperator compoundOperator;
    SelectCore selectCore;
    public FactoredMain(int line, int col) {
        super(line, col);
    }

    public CompoundOperator getCompoundOperator() {
        return compoundOperator;
    }

    public void setCompoundOperator(CompoundOperator compoundOperator) {
        this.compoundOperator = compoundOperator;
    }

    public SelectCore getSelectCore() {
        return selectCore;
    }

    public void setSelectCore(SelectCore selectCore) {
        this.selectCore = selectCore;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);

        if(compoundOperator != null)
        {
            compoundOperator.accept(astVisitor);
        }
        if(selectCore != null)
        {
            selectCore.accept(astVisitor);
        }
    }
}
