package SingleParse.SQL.SqlStmtList.SqlStatement.ResultColumn;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.Expression.Expression;

public class ResultColumnExpr extends Node {
    Expression expression;
    ColumnAlias columnAlias;
    public ResultColumnExpr(int line, int col) {
        super(line, col);
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    public ColumnAlias getColumnAlias() {
        return columnAlias;
    }

    public void setColumnAlias(ColumnAlias columnAlias) {
        this.columnAlias = columnAlias;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(expression != null)
        {
            expression.accept(astVisitor);
        }

        if(columnAlias != null)
        {
            columnAlias.accept(astVisitor);

        }
    }
}
