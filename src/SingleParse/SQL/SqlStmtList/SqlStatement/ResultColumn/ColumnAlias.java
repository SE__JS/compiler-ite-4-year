package SingleParse.SQL.SqlStmtList.SqlStatement.ResultColumn;

import AST.ASTVisitor;
import GrammarRules.Node;

public class ColumnAlias extends Node {
    String content;
    public ColumnAlias(int line, int col) {
        super(line, col);
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        System.out.println(content);
    }
}
