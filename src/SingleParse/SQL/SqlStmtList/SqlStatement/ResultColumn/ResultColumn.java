package SingleParse.SQL.SqlStmtList.SqlStatement.ResultColumn;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.Java.AllowedName;
import SingleParse.SQL.SqlStmtList.SqlStatement.MainSql.TableName;

public class ResultColumn extends Node {
    ResultColumnStar resultColumnStar;
    AllowedName allowedName;
    ResultColumnExpr resultColumnExpr;
    public ResultColumn(int line, int col) {
        super(line, col);
    }

    public ResultColumnStar getResultColumnStar() {
        return resultColumnStar;
    }

    public void setResultColumnStar(ResultColumnStar resultColumnStar) {
        this.resultColumnStar = resultColumnStar;
    }

    public AllowedName getAllowedName() {
        return allowedName;
    }

    public void setAllowedName(AllowedName allowedName) {
        this.allowedName = allowedName;
    }

    public ResultColumnExpr getResultColumnExpr() {
        return resultColumnExpr;
    }

    public void setResultColumnExpr(ResultColumnExpr resultColumnExpr) {
        this.resultColumnExpr = resultColumnExpr;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(resultColumnStar != null)
        {
            resultColumnStar.accept(astVisitor);
        }

        if(allowedName != null)
        {
            allowedName.accept(astVisitor);
        }
        if(resultColumnExpr != null)
        {
            resultColumnExpr.accept(astVisitor);

        }




    }
}
