package SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition;
import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnDefinitionConstraint.ColumnDefinitionConstraint;

import java.util.ArrayList;

public class ColumnDefinition extends Node {
    ArrayList<ColumnDefinitionConstraint> columnDefinitionConstraints;
    ColumnName columnName;
    public ColumnDefinition(int line, int col) {
        super(line, col);
    }

    public ArrayList<ColumnDefinitionConstraint> getColumnDefinitionConstraints() {
        return columnDefinitionConstraints;
    }

    public void setColumnDefinitionConstraints(ArrayList<ColumnDefinitionConstraint> columnDefinitionConstraints) {
        this.columnDefinitionConstraints = columnDefinitionConstraints;
    }

    public ColumnName getColumnName() {
        return columnName;
    }

    public void setColumnName(ColumnName columnName) {
        this.columnName = columnName;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);


        if(columnName != null)
        {
            columnName.accept(astVisitor);
        }
        if(columnDefinitionConstraints != null)
        {
            for(ColumnDefinitionConstraint columnDefinitionConstraint: columnDefinitionConstraints)
            {
                columnDefinitionConstraint.accept(astVisitor);

            }

        }
    }
}


