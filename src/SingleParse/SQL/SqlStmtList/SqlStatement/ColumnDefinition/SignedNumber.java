package SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition;

import AST.ASTVisitor;
import GrammarRules.Node;

public class SignedNumber extends Node {
    String number;
    public SignedNumber(int line, int col) {
        super(line, col);
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        System.out.println(number);
    }
}
