package SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnDefinitionConstraint;

import AST.ASTVisitor;
import SingleParse.Java.AllowedName;
import SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.SignedNumberAllowedName;

import java.util.ArrayList;

public class ColumnConstraintTypeName extends ColumnDefinitionConstraint {
    AllowedName allowedName;
    ArrayList<SignedNumberAllowedName> signedNumberAllowedNames;
    public ColumnConstraintTypeName(int line, int col) {
        super(line, col);
    }

    public AllowedName getAllowedName() {
        return allowedName;
    }

    public void setAllowedName(AllowedName allowedName) {
        this.allowedName = allowedName;
    }

    public ArrayList<SignedNumberAllowedName> getSignedNumberAllowedNames() {
        return signedNumberAllowedNames;
    }

    public void setSignedNumberAllowedNames(ArrayList<SignedNumberAllowedName> signedNumberAllowedNames) {
        this.signedNumberAllowedNames = signedNumberAllowedNames;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(allowedName != null)
        {
            allowedName.accept(astVisitor);
        }
        if(signedNumberAllowedNames != null)
        {
            for(SignedNumberAllowedName signedNumberAllowedName: signedNumberAllowedNames)
            {
                signedNumberAllowedName.accept(astVisitor);
            }
        }
    }
}
