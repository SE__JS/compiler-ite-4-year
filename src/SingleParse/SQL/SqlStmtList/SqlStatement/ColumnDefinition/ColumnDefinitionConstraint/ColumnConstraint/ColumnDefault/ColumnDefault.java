package SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnDefinitionConstraint.ColumnConstraint.ColumnDefault;

import AST.ASTVisitor;
import GrammarRules.Node;

public class ColumnDefault extends Node {

    public ColumnDefault(int line, int col) {
        super(line, col);
    }


    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
    }
}
