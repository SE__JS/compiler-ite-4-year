package SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnDefinitionConstraint.ColumnConstraint.ColumnDefault;

import AST.ASTVisitor;
import SingleParse.Expression.Expression;
import SingleParse.Java.AllowedName;
import SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnDefaultValue;

public class ColumnDefaultMain extends ColumnDefault {
    ColumnDefaultList columnDefaultList;
    ColumnDefaultValue columnDefaultValue;
    Expression expression;
    AllowedName allowedName;
    public ColumnDefaultMain(int line, int col) {
        super(line, col);
    }

    public ColumnDefaultList getColumnDefaultList() {
        return columnDefaultList;
    }

    public void setColumnDefaultList(ColumnDefaultList columnDefaultList) {
        this.columnDefaultList = columnDefaultList;
    }

    public ColumnDefaultValue getColumnDefaultValue() {
        return columnDefaultValue;
    }

    public void setColumnDefaultValue(ColumnDefaultValue columnDefaultValue) {
        this.columnDefaultValue = columnDefaultValue;
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    public AllowedName getAllowedName() {
        return allowedName;
    }

    public void setAllowedName(AllowedName allowedName) {
        this.allowedName = allowedName;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(allowedName != null)
        {
            allowedName.accept(astVisitor);

        }
        else if(expression != null)
        {
            expression.accept(astVisitor);

        } else if(columnDefaultValue != null)
        {
            columnDefaultValue.accept(astVisitor);

        }
        if(columnDefaultList != null)
        {
            columnDefaultList.accept(astVisitor);

        }
    }
}
