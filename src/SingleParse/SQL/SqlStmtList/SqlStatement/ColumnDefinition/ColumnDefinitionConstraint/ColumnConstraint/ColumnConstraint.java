package SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnDefinitionConstraint.ColumnConstraint;

import AST.ASTVisitor;
import SingleParse.Expression.Expression;
import SingleParse.Java.AllowedName;
import SingleParse.SQL.SqlStmtList.SqlStatement.MainSql.CollationName;
import SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnDefinitionConstraint.ColumnConstraint.ColumnDefault.ColumnDefault;
import SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnDefinitionConstraint.ColumnDefinitionConstraint;

public class ColumnConstraint extends ColumnDefinitionConstraint {
    AllowedName allowedName;
    ColumnConstraintPrimaryKey columnConstraintPrimaryKey;
    ColumnConstraintForeignKey columnConstraintForeignKey;
    ColumnConstraintNotNull columnConstraintNotNull;
    ColumnConstraintNull columnConstraintNull;
    Expression expression;
    ColumnDefault columnDefault;
    CollationName collationName;
    public ColumnConstraint(int line, int col) {
        super(line, col);
    }

    public AllowedName getAllowedName() {
        return allowedName;
    }

    public void setAllowedName(AllowedName allowedName) {
        this.allowedName = allowedName;
    }

    public ColumnConstraintPrimaryKey getColumnConstraintPrimaryKey() {
        return columnConstraintPrimaryKey;
    }

    public void setColumnConstraintPrimaryKey(ColumnConstraintPrimaryKey columnConstraintPrimaryKey) {
        this.columnConstraintPrimaryKey = columnConstraintPrimaryKey;
    }

    public ColumnConstraintForeignKey getColumnConstraintForeignKey() {
        return columnConstraintForeignKey;
    }

    public void setColumnConstraintForeignKey(ColumnConstraintForeignKey columnConstraintForeignKey) {
        this.columnConstraintForeignKey = columnConstraintForeignKey;
    }

    public ColumnConstraintNotNull getColumnConstraintNotNull() {
        return columnConstraintNotNull;
    }

    public void setColumnConstraintNotNull(ColumnConstraintNotNull columnConstraintNotNull) {
        this.columnConstraintNotNull = columnConstraintNotNull;
    }

    public ColumnConstraintNull getColumnConstraintNull() {
        return columnConstraintNull;
    }

    public void setColumnConstraintNull(ColumnConstraintNull columnConstraintNull) {
        this.columnConstraintNull = columnConstraintNull;
    }

    public ColumnDefault getColumnDefault() {
        return columnDefault;
    }

    public void setColumnDefault(ColumnDefault columnDefault) {
        this.columnDefault = columnDefault;
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    public CollationName getCollationName() {
        return collationName;
    }

    public void setCollationName(CollationName collationName) {
        this.collationName = collationName;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(allowedName != null)
        {
            allowedName.accept(astVisitor);
        }
        if(columnConstraintPrimaryKey != null)
        {
            columnConstraintPrimaryKey.accept(astVisitor);

        } else if(columnConstraintForeignKey != null)
        {
            columnConstraintForeignKey.accept(astVisitor);

        } else if(columnConstraintNotNull != null)
        {
            columnConstraintNotNull.accept(astVisitor);

        } else if(columnConstraintNull != null)
        {
            columnConstraintNull.accept(astVisitor);

        } else if(collationName != null)
        {
            collationName.accept(astVisitor);

        }
        else {
            if(columnDefault != null)
            {
                columnDefault.accept(astVisitor);

            }
        }
    }
}
