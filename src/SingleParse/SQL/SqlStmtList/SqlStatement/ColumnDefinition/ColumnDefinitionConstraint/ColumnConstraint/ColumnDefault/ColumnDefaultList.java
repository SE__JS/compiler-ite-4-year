package SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnDefinitionConstraint.ColumnConstraint.ColumnDefault;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.Java.AllowedName;

import java.util.ArrayList;

public class ColumnDefaultList extends Node {
    ArrayList<AllowedName> allowedNames;
    public ColumnDefaultList(int line, int col) {
        super(line, col);
    }

    public ArrayList<AllowedName> getAllowedNames() {
        return allowedNames;
    }

    public void setAllowedNames(ArrayList<AllowedName> allowedNames) {
        this.allowedNames = allowedNames;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(allowedNames != null)
        {
            for(AllowedName allowedName: allowedNames)
            {
                allowedName.accept(astVisitor);

            }

        }
    }
}
