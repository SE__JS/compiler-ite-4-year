package SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnDefinitionConstraint.ColumnConstraint;

import AST.ASTVisitor;
import GrammarRules.Node;

public class ColumnConstraintPrimaryKey extends Node {
    String content;
    public ColumnConstraintPrimaryKey(int line, int col) {
        super(line, col);
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        System.out.println(content);
    }
}
