package SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnDefinitionConstraint.ColumnConstraint;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey.ForeignKeyClause.ForeignKeyClause;

public class ColumnConstraintForeignKey extends Node {
    ForeignKeyClause foreignKeyClause;
    public ColumnConstraintForeignKey(int line, int col) {
        super(line, col);
    }

    public ForeignKeyClause getForeignKeyClause() {
        return foreignKeyClause;
    }

    public void setForeignKeyClause(ForeignKeyClause foreignKeyClause) {
        this.foreignKeyClause = foreignKeyClause;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(foreignKeyClause != null)
        {
            foreignKeyClause.accept(astVisitor);
        }
    }
}
