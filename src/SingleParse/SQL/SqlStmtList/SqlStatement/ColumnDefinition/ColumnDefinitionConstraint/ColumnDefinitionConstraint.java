package SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnDefinitionConstraint;
import AST.ASTVisitor;
import GrammarRules.Node;

public class ColumnDefinitionConstraint extends Node {
    public ColumnDefinitionConstraint(int line, int col) {
        super(line, col);
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
    }
}
