package SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.SQL.SqlStmtList.SqlStatement.MainSql.LiteralValue;

public class ColumnDefaultValue extends Node {
    SignedNumber signedNumber;
    LiteralValue literalValue;
    public ColumnDefaultValue(int line, int col) {
        super(line, col);
    }

    public SignedNumber getSignedNumber() {
        return signedNumber;
    }

    public void setSignedNumber(SignedNumber signedNumber) {
        this.signedNumber = signedNumber;
    }

    public LiteralValue getLiteralValue() {
        return literalValue;
    }

    public void setLiteralValue(LiteralValue literalValue) {
        this.literalValue = literalValue;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(signedNumber != null)
        {
            signedNumber.accept(astVisitor);

        } else if(literalValue != null)
        {
            literalValue.accept(astVisitor);

        }
    }
}
