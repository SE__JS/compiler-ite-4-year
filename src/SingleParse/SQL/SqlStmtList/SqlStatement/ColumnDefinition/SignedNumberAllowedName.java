package SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition;

import AST.ASTVisitor;
import SingleParse.Java.AllowedName;
import SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnDefinitionConstraint.ColumnConstraintTypeName;

public class SignedNumberAllowedName extends ColumnConstraintTypeName {
    SignedNumber signedNumber;
    AllowedName allowedName;
    public SignedNumberAllowedName(int line, int col) {
        super(line, col);
    }

    public SignedNumber getSignedNumber() {
        return signedNumber;
    }

    public void setSignedNumber(SignedNumber signedNumber) {
        this.signedNumber = signedNumber;
    }

    public AllowedName getAllowedName() {
        return allowedName;
    }

    public void setAllowedName(AllowedName allowedName) {
        this.allowedName = allowedName;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);

        if(signedNumber != null)
        {
            signedNumber.accept(astVisitor);
        }
        if(allowedName != null)
        {
            allowedName.accept(astVisitor);

        }
    }
}
