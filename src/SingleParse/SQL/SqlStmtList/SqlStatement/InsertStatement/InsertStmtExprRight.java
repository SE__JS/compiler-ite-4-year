package SingleParse.SQL.SqlStmtList.SqlStatement.InsertStatement;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.Expression.Expression;


import java.util.ArrayList;

public class InsertStmtExprRight extends Node {
    ArrayList<Expression> expressions;
    public InsertStmtExprRight(int line, int col) {
        super(line, col);
    }

    public ArrayList<Expression> getExpressions() {
        return expressions;
    }

    public void setExpressions(ArrayList<Expression> expressions) {
        this.expressions = expressions;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(expressions != null)
        {
            for(Expression expression: expressions)
            {
                expression.accept(astVisitor);

            }

        }
    }
}
