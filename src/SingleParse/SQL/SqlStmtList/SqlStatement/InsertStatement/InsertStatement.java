package SingleParse.SQL.SqlStmtList.SqlStatement.InsertStatement;

import AST.ASTVisitor;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectStatement.SelectStatement;
import SingleParse.SQL.SqlStmtList.SqlStatement.SqlStatement;

public class InsertStatement extends SqlStatement {
    SelectStatement selectStatement;
    InsertStatementDefault insertStatementDefault;
    InsertStatementExpression insertStatementExpression;
    InsertStatementMain insertStatementMain;
    public InsertStatement(int line, int col) {
        super(line, col);
    }

    public SelectStatement getSelectStatement() {
        return selectStatement;
    }

    public void setSelectStatement(SelectStatement selectStatement) {
        this.selectStatement = selectStatement;
    }

    public InsertStatementDefault getInsertStatementDefault() {
        return insertStatementDefault;
    }

    public void setInsertStatementDefault(InsertStatementDefault insertStatementDefault) {
        this.insertStatementDefault = insertStatementDefault;
    }

    public InsertStatementExpression getInsertStatementExpression() {
        return insertStatementExpression;
    }

    public void setInsertStatementExpression(InsertStatementExpression insertStatementExpression) {
        this.insertStatementExpression = insertStatementExpression;
    }

    public InsertStatementMain getInsertStatementMain() {
        return insertStatementMain;
    }

    public void setInsertStatementMain(InsertStatementMain insertStatementMain) {
        this.insertStatementMain = insertStatementMain;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(insertStatementMain != null)
        {
            insertStatementMain.accept(astVisitor);
        }


        if(insertStatementExpression != null)
        {
            insertStatementExpression.accept(astVisitor);

        }


        if(selectStatement != null)
        {
            selectStatement.accept(astVisitor);

        }
        if(insertStatementDefault != null)
        {
            insertStatementDefault.accept(astVisitor);

        }
    }
}
