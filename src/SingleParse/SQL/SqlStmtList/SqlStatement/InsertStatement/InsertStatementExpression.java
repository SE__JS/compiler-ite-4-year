package SingleParse.SQL.SqlStmtList.SqlStatement.InsertStatement;

import AST.ASTVisitor;
import GrammarRules.Node;

import java.util.ArrayList;

public class InsertStatementExpression extends Node {
    InsertStmtExprLeft insertStmtExprLeft;
    ArrayList<InsertStmtExprRight> insertStmtExprRights;

    public InsertStatementExpression(int line, int col) {
        super(line, col);
    }

    public InsertStmtExprLeft getInsertStmtExprLeft() {

        return insertStmtExprLeft;
    }

    public void setInsertStmtExprLeft(InsertStmtExprLeft insertStmtExprLeft) {
        this.insertStmtExprLeft = insertStmtExprLeft;
    }

    public ArrayList<InsertStmtExprRight> getInsertStmtExprRights() {
        return insertStmtExprRights;
    }

    public void setInsertStmtExprRights(ArrayList<InsertStmtExprRight> insertStmtExprRights) {
        this.insertStmtExprRights = insertStmtExprRights;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(insertStmtExprLeft != null)
        {
            insertStmtExprLeft.accept(astVisitor);
        }
        if(insertStmtExprRights != null)
        {
            for(InsertStmtExprRight insertStmtExprRight: insertStmtExprRights)
            {
                insertStmtExprRight.accept(astVisitor);

            }

        }
    }
}
