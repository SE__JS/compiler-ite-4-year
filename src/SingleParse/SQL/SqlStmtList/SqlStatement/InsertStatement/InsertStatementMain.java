package SingleParse.SQL.SqlStmtList.SqlStatement.InsertStatement;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.SQL.SqlStmtList.SqlStatement.MainSql.DatabaseName;
import SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnName;
import SingleParse.SQL.SqlStmtList.SqlStatement.MainSql.TableName;

import java.util.ArrayList;

public class InsertStatementMain extends Node {
    DatabaseName databaseName;
    ArrayList<ColumnName> columnNames;
    TableName tableName;
    public InsertStatementMain(int line, int col) {
        super(line, col);
    }

    public DatabaseName getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(DatabaseName databaseName) {
        this.databaseName = databaseName;
    }

    public ArrayList<ColumnName> getColumnNames() {
        return columnNames;
    }

    public void setColumnNames(ArrayList<ColumnName> columnNames) {
        this.columnNames = columnNames;
    }

    public TableName getTableName() {
        return tableName;
    }

    public void setTableName(TableName tableName) {
        this.tableName = tableName;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(databaseName != null)
        {
            databaseName.accept(astVisitor);

        }
        if(tableName != null)
        {
            tableName.accept(astVisitor);
        }


        if( columnNames != null)
        {
            for(ColumnName columnName: columnNames)
            {
                columnName.accept(astVisitor);
            }
        }
    }
}
