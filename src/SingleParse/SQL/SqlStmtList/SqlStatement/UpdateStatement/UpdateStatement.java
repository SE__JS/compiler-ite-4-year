package SingleParse.SQL.SqlStmtList.SqlStatement.UpdateStatement;

import AST.ASTVisitor;
import SingleParse.Expression.Expression;
import SingleParse.SQL.SqlStmtList.SqlStatement.DeleteStatement.QualifiedTableName.QualifiedTableName;
import SingleParse.SQL.SqlStmtList.SqlStatement.SqlStatement;

import java.util.ArrayList;

public class UpdateStatement extends SqlStatement {
    QualifiedTableName qualifiedTableName;

    ArrayList<UpdateStatementMain> updateStatementMains;
    Expression expression;
    public UpdateStatement(int line, int col) {
        super(line, col);
    }

    public QualifiedTableName getQualifiedTableName() {
        return qualifiedTableName;
    }

    public void setQualifiedTableName(QualifiedTableName qualifiedTableName) {
        this.qualifiedTableName = qualifiedTableName;
    }

    public ArrayList<UpdateStatementMain> getUpdateStatementMains() {
        return updateStatementMains;
    }

    public void setUpdateStatementMains(ArrayList<UpdateStatementMain> updateStatementMains) {
        this.updateStatementMains = updateStatementMains;
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(qualifiedTableName != null)
        {
            qualifiedTableName.accept(astVisitor);
        }
        if(updateStatementMains != null)
        {
            for(UpdateStatementMain updateStatementMain: updateStatementMains)
            {
                updateStatementMain.accept(astVisitor);
            }
        }
        if(expression != null)
        {
            expression.accept(astVisitor);
        }
    }
}
