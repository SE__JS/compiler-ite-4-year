package SingleParse.SQL.SqlStmtList.SqlStatement.UpdateStatement;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.Expression.Expression;
import SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnName;

public class UpdateStatementMain extends Node {
    ColumnName columnName;
    Expression expression;
    public UpdateStatementMain(int line, int col) {
        super(line, col);
    }

    public ColumnName getColumnName() {
        return columnName;
    }

    public void setColumnName(ColumnName columnName) {
        this.columnName = columnName;
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(columnName != null)
        {
            columnName.accept(astVisitor);

        }
        if(expression != null)
        {
            expression.accept(astVisitor);
        }

    }
}
