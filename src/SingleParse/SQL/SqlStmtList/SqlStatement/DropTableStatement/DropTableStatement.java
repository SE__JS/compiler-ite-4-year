package SingleParse.SQL.SqlStmtList.SqlStatement.DropTableStatement;

import AST.ASTVisitor;
import SingleParse.SQL.SqlStmtList.SqlStatement.MainSql.DatabaseName;
import SingleParse.SQL.SqlStmtList.SqlStatement.SqlStatement;
import SingleParse.SQL.SqlStmtList.SqlStatement.MainSql.TableName;


public class DropTableStatement extends SqlStatement {
    DatabaseName databaseName;
    TableName tableName;
    public DropTableStatement(int line, int col) {
        super(line, col);
    }

    public DatabaseName getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(DatabaseName databaseName) {
        this.databaseName = databaseName;
    }

    public TableName getTableName() {
        return tableName;
    }

    public void setTableName(TableName tableName) {
        this.tableName = tableName;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(databaseName != null)
        {
            databaseName.accept(astVisitor);


        }
        if(tableName != null)
        {
            tableName.accept(astVisitor);
        }


    }
}
