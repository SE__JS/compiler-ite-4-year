package SingleParse.SQL.SqlStmtList.SqlStatement.MainSql;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.Java.AllowedName;

public class IndexName extends Node {
    AllowedName allowedName;
    public IndexName(int line, int col) {
        super(line, col);
    }

    public AllowedName getAllowedName() {
        return allowedName;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(allowedName != null)
        {
            allowedName.accept(astVisitor);
        }

    }

    public void setAllowedName(AllowedName allowedName) {
        this.allowedName = allowedName;
    }
}
