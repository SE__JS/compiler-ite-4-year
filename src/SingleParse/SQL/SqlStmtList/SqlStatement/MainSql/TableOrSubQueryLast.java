package SingleParse.SQL.SqlStmtList.SqlStatement.MainSql;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectStatement.SelectStatement;

public class TableOrSubQueryLast extends Node {
    SelectStatement selectStatement;
    TableAlias tableAlias;
    public TableOrSubQueryLast(int line, int col) {
        super(line, col);
    }

    public SelectStatement getSelectStatement() {
        return selectStatement;
    }

    public void setSelectStatement(SelectStatement selectStatement) {
        this.selectStatement = selectStatement;
    }

    public TableAlias getTableAlias() {
        return tableAlias;
    }

    public void setTableAlias(TableAlias tableAlias) {
        this.tableAlias = tableAlias;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(selectStatement != null)
        {
            selectStatement.accept(astVisitor);
        }
        if(tableAlias != null)
        {
            tableAlias.accept(astVisitor);
        }
    }
}
