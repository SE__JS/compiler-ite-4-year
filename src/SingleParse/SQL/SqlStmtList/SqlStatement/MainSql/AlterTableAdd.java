package SingleParse.SQL.SqlStmtList.SqlStatement.MainSql;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraint;

public class AlterTableAdd extends Node {
    TableConstraint tableConstraint;
    public AlterTableAdd(int line, int col) {
        super(line, col);
    }

    public TableConstraint getTableConstraint() {
        return tableConstraint;
    }

    public void setTableConstraint(TableConstraint tableConstraint) {
        this.tableConstraint = tableConstraint;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(tableConstraint != null)
        {
            tableConstraint.accept(astVisitor);
        }
    }
}
