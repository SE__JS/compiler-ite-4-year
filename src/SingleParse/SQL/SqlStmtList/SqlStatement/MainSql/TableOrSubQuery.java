package SingleParse.SQL.SqlStmtList.SqlStatement.MainSql;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.SQL.SqlStmtList.SqlStatement.JoinClause.JoinClause;

import java.util.ArrayList;

public class TableOrSubQuery extends Node {
    DatabaseName databaseName;
    TableName tableName;
    TableAlias tableAlias;
    IndexName indexName;
    TableOrSubQueryMain tableOrSubQueryMain;
    TableOrSubQueryLast tableOrSubQueryLast;

    public TableOrSubQuery(int line, int col) {
        super(line, col);
    }

    public DatabaseName getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(DatabaseName databaseName) {
        this.databaseName = databaseName;
    }

    public TableName getTableName() {
        return tableName;
    }

    public void setTableName(TableName tableName) {
        this.tableName = tableName;
    }

    public TableAlias getTableAlias() {
        return tableAlias;
    }

    public void setTableAlias(TableAlias tableAlias) {
        this.tableAlias = tableAlias;
    }

    public IndexName getIndexName() {
        return indexName;
    }

    public void setIndexName(IndexName indexName) {
        this.indexName = indexName;
    }

    public TableOrSubQueryMain getTableOrSubQueryMain() {
        return tableOrSubQueryMain;
    }

    public void setTableOrSubQueryMain(TableOrSubQueryMain tableOrSubQueryMain) {
        this.tableOrSubQueryMain = tableOrSubQueryMain;
    }

    public TableOrSubQueryLast getTableOrSubQueryLast() {
        return tableOrSubQueryLast;
    }

    public void setTableOrSubQueryLast(TableOrSubQueryLast tableOrSubQueryLast) {
        this.tableOrSubQueryLast = tableOrSubQueryLast;
    }


    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(databaseName != null)
        {
            databaseName.accept(astVisitor);
        }
        if(tableName != null)
        {
            tableName.accept(astVisitor);
        }
        if(tableAlias != null)
        {
            tableName.accept(astVisitor);
        }
        if(indexName != null)
        {
            indexName.accept(astVisitor);
        }
        if(tableOrSubQueryMain != null)
        {
            tableOrSubQueryMain.accept(astVisitor);
        }
        if(tableOrSubQueryLast != null)
        {
            tableOrSubQueryLast.accept(astVisitor);
        }
    }
}
