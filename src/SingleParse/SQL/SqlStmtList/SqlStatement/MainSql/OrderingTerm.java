package SingleParse.SQL.SqlStmtList.SqlStatement.MainSql;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.Expression.Expression;

public class OrderingTerm extends Node {
    Expression expression;
    CollationName collationName;

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(expression != null)
        {
            expression.accept(astVisitor);
        }
        if(collationName != null)
        {
            collationName.accept(astVisitor);

        }

    }

    public OrderingTerm(int line, int col) {
        super(line, col);
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    public CollationName getCollationName() {
        return collationName;
    }

    public void setCollationName(CollationName collationName) {
        this.collationName = collationName;
    }
}
