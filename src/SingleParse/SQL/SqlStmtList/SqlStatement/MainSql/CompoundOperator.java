package SingleParse.SQL.SqlStmtList.SqlStatement.MainSql;

import AST.ASTVisitor;
import GrammarRules.Node;

public class CompoundOperator extends Node {
    String content;
    public CompoundOperator(int line, int col) {
        super(line, col);
    }

    public String getContent() {
        return content;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        System.out.println(content);
    }

    public void setContent(String content) {
        this.content = content;
    }
}
