package SingleParse.SQL.SqlStmtList.SqlStatement.MainSql;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnName;

public class IndexColumn extends Node {
    CollationName collationName;
    ColumnName columnName;
    public IndexColumn(int line, int col) {
        super(line, col);
    }

    public CollationName getCollationName() {
        return collationName;
    }

    public void setCollationName(CollationName collationName) {
        this.collationName = collationName;
    }

    public ColumnName getColumnName() {
        return columnName;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(columnName != null)
        {
            columnName.accept(astVisitor);
        }
        if(collationName != null)
        {
            collationName.accept(astVisitor);
        }
    }

    public void setColumnName(ColumnName columnName) {
        this.columnName = columnName;
    }
}
