package SingleParse.SQL.SqlStmtList.SqlStatement.MainSql;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.Java.AllowedName;

public class TableName extends Node {
    AllowedName allowedName;
    private boolean partOfSelection;
    public TableName(int line, int col) {
        super(line, col);
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(allowedName != null) {
            allowedName.accept(astVisitor);
        }
    }

    public boolean isPartOfSelection() {
        return partOfSelection;
    }

    public void setPartOfSelection(boolean partOfSelection) {
        this.partOfSelection = partOfSelection;
    }

    public AllowedName getAllowedName() {
        return allowedName;
    }

    public void setAllowedName(AllowedName allowedName) {
        this.allowedName = allowedName;
    }
}
