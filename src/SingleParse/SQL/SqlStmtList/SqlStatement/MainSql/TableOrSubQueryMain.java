package SingleParse.SQL.SqlStmtList.SqlStatement.MainSql;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.SQL.SqlStmtList.SqlStatement.JoinClause.JoinClause;

import java.util.ArrayList;

public class TableOrSubQueryMain extends Node {
    ArrayList<TableOrSubQuery> tableOrSubQueries;
    JoinClause joinClause;
    TableAlias tableAlias;
    public TableOrSubQueryMain(int line, int col) {
        super(line, col);
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(tableOrSubQueries != null)
        {
            for(TableOrSubQuery tableOrSubQuery: tableOrSubQueries)
            {
                tableOrSubQuery.accept(astVisitor);
            }
        }
        if(joinClause != null)
        {
            joinClause.accept(astVisitor);
        }
        if(tableAlias != null)
        {
            tableAlias.accept(astVisitor);
        }

    }

    public ArrayList<TableOrSubQuery> getTableOrSubQueries() {
        return tableOrSubQueries;
    }

    public void setTableOrSubQueries(ArrayList<TableOrSubQuery> tableOrSubQueries) {
        this.tableOrSubQueries = tableOrSubQueries;
    }

    public JoinClause getJoinClause() {
        return joinClause;
    }

    public void setJoinClause(JoinClause joinClause) {
        this.joinClause = joinClause;
    }

    public TableAlias getTableAlias() {
        return tableAlias;
    }

    public void setTableAlias(TableAlias tableAlias) {
        this.tableAlias = tableAlias;
    }
}
