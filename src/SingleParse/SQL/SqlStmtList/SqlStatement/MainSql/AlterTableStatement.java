package SingleParse.SQL.SqlStmtList.SqlStatement.MainSql;

import AST.ASTVisitor;
import SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnDefinition;
import SingleParse.SQL.SqlStmtList.SqlStatement.SqlStatement;

public class AlterTableStatement extends SqlStatement {
    DatabaseName databaseName;
    SourceTableName sourceTableName;
    NewTableName newTableName;
    AlterTableAdd alterTableAdd;
    AlterTableAddConstraint alterTableAddConstraint;
    ColumnDefinition columnDefinition;
    public AlterTableAddConstraint getAlterTableAddConstraint() {
        return alterTableAddConstraint;
    }

    public void setAlterTableAddConstraint(AlterTableAddConstraint alterTableAddConstraint) {
        this.alterTableAddConstraint = alterTableAddConstraint;
    }

    public AlterTableAdd getAlterTableAdd() {
        return alterTableAdd;
    }

    public void setAlterTableAdd(AlterTableAdd alterTableAdd) {
        this.alterTableAdd = alterTableAdd;
    }

    public AlterTableStatement(int line, int col) {
        super(line, col);
    }

    public DatabaseName getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(DatabaseName databaseName) {
        this.databaseName = databaseName;
    }

    public SourceTableName getSourceTableName() {
        return sourceTableName;
    }

    public void setSourceTableName(SourceTableName sourceTableName) {
        this.sourceTableName = sourceTableName;
    }

    public NewTableName getNewTableName() {
        return newTableName;
    }

    public void setNewTableName(NewTableName newTableName) {
        this.newTableName = newTableName;
    }

    public ColumnDefinition getColumnDefinition() {
        return columnDefinition;
    }

    public void setColumnDefinition(ColumnDefinition columnDefinition) {
        this.columnDefinition = columnDefinition;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(databaseName != null)
        {
            databaseName.accept(astVisitor);

        }
        if(sourceTableName != null)
        {
            sourceTableName.accept(astVisitor);
        }
        if(newTableName != null)
        {
            newTableName.accept(astVisitor);

        }
        if(alterTableAdd != null)
        {
            alterTableAdd.accept(astVisitor);

        }
        if(alterTableAddConstraint != null)
        {
            alterTableAddConstraint.accept(astVisitor);

        }
        if(columnDefinition != null)
        {
            columnDefinition.accept(astVisitor);

        }
    }
}
