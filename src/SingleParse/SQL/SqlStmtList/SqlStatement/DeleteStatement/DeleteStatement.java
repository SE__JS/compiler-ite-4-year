package SingleParse.SQL.SqlStmtList.SqlStatement.DeleteStatement;

import AST.ASTVisitor;
import SingleParse.Expression.Expression;
import SingleParse.SQL.SqlStmtList.SqlStatement.DeleteStatement.QualifiedTableName.QualifiedTableName;
import SingleParse.SQL.SqlStmtList.SqlStatement.SqlStatement;

public class DeleteStatement extends SqlStatement {
    QualifiedTableName qualifiedTableName;
    Expression expression;
    public DeleteStatement(int line, int col) {
        super(line, col);
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    public QualifiedTableName getQualifiedTableName() {
        return qualifiedTableName;
    }

    public void setQualifiedTableName(QualifiedTableName qualifiedTableName) {
        this.qualifiedTableName = qualifiedTableName;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(qualifiedTableName != null)
        {
            qualifiedTableName.accept(astVisitor);
        }


        if(expression != null)
        {
            expression.accept(astVisitor);

        }
    }
}
