package SingleParse.SQL.SqlStmtList.SqlStatement.DeleteStatement.QualifiedTableName;

import AST.ASTVisitor;

public class NotIndexedQualifiedTableName extends IndexedOrNot {
    String content;
    public NotIndexedQualifiedTableName(int line, int col) {
        super(line, col);
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        System.out.println(content);
    }
}
