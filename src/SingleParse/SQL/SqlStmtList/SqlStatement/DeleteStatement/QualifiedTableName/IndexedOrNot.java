package SingleParse.SQL.SqlStmtList.SqlStatement.DeleteStatement.QualifiedTableName;

import AST.ASTVisitor;
import GrammarRules.Node;

public class IndexedOrNot extends Node {
    public IndexedOrNot(int line, int col) {
        super(line, col);
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
    }
}
