package SingleParse.SQL.SqlStmtList.SqlStatement.DeleteStatement.QualifiedTableName;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.SQL.SqlStmtList.SqlStatement.MainSql.DatabaseName;
import SingleParse.SQL.SqlStmtList.SqlStatement.MainSql.TableName;

public class QualifiedTableName extends Node {
    DatabaseName databaseName;
    TableName tableName;
    IndexedOrNot indexedOrNot;
    public QualifiedTableName(int line, int col) {
        super(line, col);
    }

    public DatabaseName getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(DatabaseName databaseName) {
        this.databaseName = databaseName;
    }

    public TableName getTableName() {
        return tableName;
    }

    public void setTableName(TableName tableName) {
        this.tableName = tableName;
    }

    public IndexedOrNot getIndexedOrNot() {
        return indexedOrNot;
    }

    public void setIndexedOrNot(IndexedOrNot indexedOrNot) {
        this.indexedOrNot = indexedOrNot;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(databaseName != null)
        {
            databaseName.accept(astVisitor);

        }
        if(tableName!=null) {
            tableName.accept(astVisitor);

        }
        if(indexedOrNot!=null){
        indexedOrNot.accept(astVisitor);

        }
    }
}
