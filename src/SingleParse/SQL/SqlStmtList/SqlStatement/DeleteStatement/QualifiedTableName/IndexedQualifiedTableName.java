package SingleParse.SQL.SqlStmtList.SqlStatement.DeleteStatement.QualifiedTableName;

import AST.ASTVisitor;
import SingleParse.Java.AllowedName;

public class IndexedQualifiedTableName extends IndexedOrNot {
    AllowedName allowedName;
    public IndexedQualifiedTableName(int line, int col) {
        super(line, col);
    }

    public AllowedName getAllowedName() {
        return allowedName;
    }

    public void setAllowedName(AllowedName allowedName) {
        this.allowedName = allowedName;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(allowedName != null)
        {
            allowedName.accept(astVisitor);
        }
    }
}
