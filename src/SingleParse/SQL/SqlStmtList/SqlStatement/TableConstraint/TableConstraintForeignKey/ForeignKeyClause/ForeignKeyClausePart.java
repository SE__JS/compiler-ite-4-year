package SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey.ForeignKeyClause;

import AST.ASTVisitor;
import GrammarRules.Node;

public class ForeignKeyClausePart extends Node {
    String content;
    public ForeignKeyClausePart(int line, int col) {
        super(line, col);
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);

    }
}
