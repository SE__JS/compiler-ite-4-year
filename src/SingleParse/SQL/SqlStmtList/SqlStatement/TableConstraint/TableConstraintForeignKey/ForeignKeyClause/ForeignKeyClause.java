package SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey.ForeignKeyClause;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey.ForeignKeyClause.ForeignKeyClauseMain.ForeignKeyClauseMain;

public class ForeignKeyClause extends Node {
    ForeignKeyClauseMain foreignKeyClauseMain;
    ForeignKeyClauseList foreignKeyClauseList;
    ForeignKeyClausePart foreignKeyClausePart;
    public ForeignKeyClause(int line, int col) {
        super(line, col);
    }

    public ForeignKeyClauseMain getForeignKeyClauseMain() {
        return foreignKeyClauseMain;
    }

    public void setForeignKeyClauseMain(ForeignKeyClauseMain foreignKeyClauseMain) {
        this.foreignKeyClauseMain = foreignKeyClauseMain;
    }

    public ForeignKeyClauseList getForeignKeyClauseList() {
        return foreignKeyClauseList;
    }

    public void setForeignKeyClauseList(ForeignKeyClauseList foreignKeyClauseList) {
        this.foreignKeyClauseList = foreignKeyClauseList;
    }

    public ForeignKeyClausePart getForeignKeyClausePart() {
        return foreignKeyClausePart;
    }

    public void setForeignKeyClausePart(ForeignKeyClausePart foreignKeyClausePart) {
        this.foreignKeyClausePart = foreignKeyClausePart;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(foreignKeyClauseMain != null)
        {
            foreignKeyClauseMain.accept(astVisitor);
        }
        if(foreignKeyClauseList != null)
        {
            foreignKeyClauseList.accept(astVisitor);
        }
        if(foreignKeyClausePart != null)
        {
            foreignKeyClausePart.accept(astVisitor);
        }
    }
}
