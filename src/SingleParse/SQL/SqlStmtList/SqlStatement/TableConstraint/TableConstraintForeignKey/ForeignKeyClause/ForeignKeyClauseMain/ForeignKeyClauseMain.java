package SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey.ForeignKeyClause.ForeignKeyClauseMain;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.SQL.SqlStmtList.SqlStatement.MainSql.DatabaseName;
import SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey.ForeignKeyClause.ForeignKeyClauseList;

public class ForeignKeyClauseMain extends Node {
    DatabaseName databaseName;
    ForeignTable foreignTable;
    ForeignKeyClauseList foreignKeyClauseList;
    public ForeignKeyClauseMain(int line, int col) {
        super(line, col);
    }

    public DatabaseName getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(DatabaseName databaseName) {
        this.databaseName = databaseName;
    }

    public ForeignTable getForeignTable() {
        return foreignTable;
    }

    public void setForeignTable(ForeignTable foreignTable) {
        this.foreignTable = foreignTable;
    }

    public ForeignKeyClauseList getForeignKeyClauseList() {
        return foreignKeyClauseList;
    }

    public void setForeignKeyClauseList(ForeignKeyClauseList foreignKeyClauseList) {
        this.foreignKeyClauseList = foreignKeyClauseList;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(databaseName != null)
        {
            databaseName.accept(astVisitor);

        }
        if(foreignTable != null)
        {
            foreignTable.accept(astVisitor);
        }
        if(foreignKeyClauseList != null)
        {
            foreignKeyClauseList.accept(astVisitor);
        }
    }
}
