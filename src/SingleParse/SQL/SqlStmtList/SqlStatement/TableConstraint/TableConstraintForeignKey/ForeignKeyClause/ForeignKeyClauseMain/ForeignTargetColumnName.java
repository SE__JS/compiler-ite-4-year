package SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey.ForeignKeyClause.ForeignKeyClauseMain;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.Java.AllowedName;




public class ForeignTargetColumnName extends Node {
    AllowedName allowedName;
    public ForeignTargetColumnName(int line, int col) {
        super(line, col);
    }

    public AllowedName getAllowedName() {
        return allowedName;
    }

    public void setAllowedName(AllowedName allowedName) {
        this.allowedName = allowedName;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(allowedName != null)
        {
            allowedName.accept(astVisitor);
        }


    }
}
