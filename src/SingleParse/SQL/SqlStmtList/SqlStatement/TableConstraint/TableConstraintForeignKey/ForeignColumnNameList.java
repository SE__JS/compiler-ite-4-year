package SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey;

import AST.ASTVisitor;
import GrammarRules.Node;

import java.util.ArrayList;

public class ForeignColumnNameList extends Node {
    ArrayList<ForeignColumnName> foreignColumnNames;
    public ForeignColumnNameList(int line, int col) {
        super(line, col);
    }

    public ArrayList<ForeignColumnName> getForeignColumnNames() {
        return foreignColumnNames;
    }

    public void setForeignColumnNames(ArrayList<ForeignColumnName> foreignColumnNames) {
        this.foreignColumnNames = foreignColumnNames;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(foreignColumnNames != null)
        {
            for(ForeignColumnName foreignColumnName: foreignColumnNames)
            {
                foreignColumnName.accept(astVisitor);
            }

        }

    }
}
