package SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey;

import AST.ASTVisitor;
import SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraint;

public class TableConstraintForeignKey extends TableConstraint {
    public TableConstraintForeignKey(int line, int col) {
        super(line, col);
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
    }
}
