package SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey;

import AST.ASTVisitor;
import SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey.ForeignKeyClause.ForeignKeyClause;

public class TableConstraintForeignKeyBody extends TableConstraintForeignKey {
    ForeignColumnNameList foreignColumnNameList;
    ForeignKeyClause foreignKeyClause;
    public TableConstraintForeignKeyBody(int line, int col) {
        super(line, col);
    }

    public ForeignColumnNameList getForeignColumnNameList() {
        return foreignColumnNameList;
    }

    public void setForeignColumnNameList(ForeignColumnNameList foreignColumnNameList) {
        this.foreignColumnNameList = foreignColumnNameList;
    }

    public ForeignKeyClause getForeignKeyClause() {
        return foreignKeyClause;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);

        if(foreignColumnNameList != null)
        {
            foreignColumnNameList.accept(astVisitor);
        }
        if(foreignKeyClause != null)
        {
            foreignKeyClause.accept(astVisitor);
        }

    }

    public void setForeignKeyClause(ForeignKeyClause foreignKeyClause) {
        this.foreignKeyClause = foreignKeyClause;
    }
}
