package SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.Java.AllowedName;

public class ForeignColumnName extends Node {
    AllowedName allowedName;
    public ForeignColumnName(int line, int col) {
        super(line, col);
    }

    public AllowedName getAllowedName() {
        return allowedName;
    }

    public void setAllowedName(AllowedName allowedName) {
        this.allowedName = allowedName;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(allowedName != null)
        {
            allowedName.accept(astVisitor);
        }

    }
}
