package SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.SQL.SqlStmtList.SqlStatement.MainSql.IndexColumn;

import java.util.ArrayList;

public class IndexedColumnList extends Node {
    ArrayList<IndexColumn> indexColumns;
    public IndexedColumnList(int line, int col) {
        super(line, col);
    }

    public ArrayList<IndexColumn> getIndexColumns() {
        return indexColumns;
    }

    public void setIndexColumns(ArrayList<IndexColumn> indexColumns) {
        this.indexColumns = indexColumns;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(indexColumns != null)
        {
            for(IndexColumn indexColumn: indexColumns)
            {
                indexColumn.accept(astVisitor);
            }
        }
    }
}
