package SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint;


import AST.ASTVisitor;

public class TableConstraintPrimaryKey extends TableConstraint {
    IndexedColumnList indexedColumnList;
    public TableConstraintPrimaryKey(int line, int col) {
        super(line, col);
    }

    public IndexedColumnList getIndexedColumnList() {
        return indexedColumnList;
    }

    public void setIndexedColumnList(IndexedColumnList indexedColumnList) {
        this.indexedColumnList = indexedColumnList;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);

        if(indexedColumnList != null)
        {
            indexedColumnList.accept(astVisitor);
        }
    }
}
