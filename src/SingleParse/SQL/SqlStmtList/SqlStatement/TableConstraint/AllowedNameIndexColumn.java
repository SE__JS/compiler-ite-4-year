package SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.Java.AllowedName;

public class AllowedNameIndexColumn extends Node {
    IndexedColumnList indexedColumnList;
    AllowedName allowedName;
    public AllowedNameIndexColumn(int line, int col) {
        super(line, col);
    }

    public IndexedColumnList getIndexedColumnList() {
        return indexedColumnList;
    }

    public void setIndexedColumnList(IndexedColumnList indexedColumnList) {
        this.indexedColumnList = indexedColumnList;
    }

    public AllowedName getAllowedName() {
        return allowedName;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(allowedName != null)
        {
            allowedName.accept(astVisitor);

        }
        if(indexedColumnList != null)
        {
            indexedColumnList.accept(astVisitor);
        }
    }

    public void setAllowedName(AllowedName allowedName) {
        this.allowedName = allowedName;
    }
}
