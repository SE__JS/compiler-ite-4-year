package SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.Expression.Expression;
import SingleParse.Java.AllowedName;
import SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraintForeignKey.TableConstraintForeignKey;

public class TableConstraint extends Node {
    AllowedName allowedName;
    TableConstraintPrimaryKey tableConstraintPrimaryKey;
    TableConstraintKey tableConstraintKey;
    TableConstraintUnique tableConstraintUnique;
    TableConstraintForeignKey tableConstraintForeignKey;
    Expression expression;
    public TableConstraint(int line, int col) {
        super(line, col);
    }

    public AllowedName getAllowedName() {
        return allowedName;
    }

    public void setAllowedName(AllowedName allowedName) {
        this.allowedName = allowedName;
    }

    public TableConstraintPrimaryKey getTableConstraintPrimaryKey() {
        return tableConstraintPrimaryKey;
    }

    public void setTableConstraintPrimaryKey(TableConstraintPrimaryKey tableConstraintPrimaryKey) {
        this.tableConstraintPrimaryKey = tableConstraintPrimaryKey;
    }

    public TableConstraintKey getTableConstraintKey() {
        return tableConstraintKey;
    }

    public void setTableConstraintKey(TableConstraintKey tableConstraintKey) {
        this.tableConstraintKey = tableConstraintKey;
    }

    public TableConstraintUnique getTableConstraintUnique() {
        return tableConstraintUnique;
    }

    public void setTableConstraintUnique(TableConstraintUnique tableConstraintUnique) {
        this.tableConstraintUnique = tableConstraintUnique;
    }

    public TableConstraintForeignKey getTableConstraintForeignKey() {
        return tableConstraintForeignKey;
    }

    public void setTableConstraintForeignKey(TableConstraintForeignKey tableConstraintForeignKey) {
        this.tableConstraintForeignKey = tableConstraintForeignKey;
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(allowedName != null)
        {
            allowedName.accept(astVisitor);

        }

        if(tableConstraintForeignKey != null) {
            tableConstraintForeignKey.accept(astVisitor);

        }
        if(tableConstraintPrimaryKey != null)
        {
            tableConstraintPrimaryKey.accept(astVisitor);

        }
        if(expression != null)
        {
            expression.accept(astVisitor);

        }
        if(tableConstraintUnique != null)
        {
            tableConstraintUnique.accept(astVisitor);

        }
        if(tableConstraintKey != null)
        {
            tableConstraintKey.accept(astVisitor);

        }
    }
}
