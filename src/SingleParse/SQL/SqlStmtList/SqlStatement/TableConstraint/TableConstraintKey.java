package SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint;


import AST.ASTVisitor;

public class TableConstraintKey extends TableConstraint {
    AllowedNameIndexColumn allowedNameIndexColumn;
    public TableConstraintKey(int line, int col) {
        super(line, col);
    }

    public AllowedNameIndexColumn getAllowedNameIndexColumn() {
        return allowedNameIndexColumn;
    }

    public void setAllowedNameIndexColumn(AllowedNameIndexColumn allowedNameIndexColumn) {
        this.allowedNameIndexColumn = allowedNameIndexColumn;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if (allowedNameIndexColumn != null) {
            allowedNameIndexColumn.accept(astVisitor);
        }
    }
}
