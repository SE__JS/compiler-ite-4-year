package SingleParse.SQL.SqlStmtList.SqlStatement.CreateTableStatement;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnDefinition;

import java.util.ArrayList;

public class CreateTableStmtMain extends Node {
    ColumnDefinition columnDefinition;
    ArrayList<SingleTableConstraintOrColumnDef> singleTableConstraintOrColumnDefs;
    public CreateTableStmtMain(int line, int col) {
        super(line, col);
    }

    public ColumnDefinition getColumnDefinition() {
        return columnDefinition;
    }

    public void setColumnDefinition(ColumnDefinition columnDefinition) {
        this.columnDefinition = columnDefinition;
    }

    public ArrayList<SingleTableConstraintOrColumnDef> getSingleTableConstraintOrColumnDefs() {
        return singleTableConstraintOrColumnDefs;
    }

    public void setSingleTableConstraintOrColumnDefs(ArrayList<SingleTableConstraintOrColumnDef> singleTableConstraintOrColumnDefs) {
        this.singleTableConstraintOrColumnDefs = singleTableConstraintOrColumnDefs;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(columnDefinition != null)
        {
            columnDefinition.accept(astVisitor);
        }


        if(singleTableConstraintOrColumnDefs != null)
        {
            for(SingleTableConstraintOrColumnDef singleTableConstraintOrColumnDef: singleTableConstraintOrColumnDefs)
            {
                singleTableConstraintOrColumnDef.accept(astVisitor);
            }
        }
    }
}
