package SingleParse.SQL.SqlStmtList.SqlStatement.CreateTableStatement;

import AST.ASTVisitor;
import SingleParse.SQL.SqlStmtList.SqlStatement.MainSql.DatabaseName;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectStatement.SelectStatement;
import SingleParse.SQL.SqlStmtList.SqlStatement.SqlStatement;
import SingleParse.SQL.SqlStmtList.SqlStatement.MainSql.TableName;

class CreateTableStatement extends SqlStatement {
    DatabaseName databaseName;
    TableName tableName;
    CreateTableStmtMain createTableStmtMain;
    SelectStatement selectStatement;
    public CreateTableStatement(int line, int col) {
        super(line, col);
    }

    public DatabaseName getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(DatabaseName databaseName) {
        this.databaseName = databaseName;
    }

    public TableName getTableName() {
        return tableName;
    }

    public void setTableName(TableName tableName) {
        this.tableName = tableName;
    }

    public CreateTableStmtMain getCreateTableStmtMain() {
        return createTableStmtMain;
    }

    public void setCreateTableStmtMain(CreateTableStmtMain createTableStmtMain) {
        this.createTableStmtMain = createTableStmtMain;
    }

    public SelectStatement getSelectStatement() {
        return selectStatement;
    }

    public void setSelectStatement(SelectStatement selectStatement) {
        this.selectStatement = selectStatement;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(databaseName != null)
        {
            databaseName.accept(astVisitor);

        }
        if(tableName != null)
        {
            tableName.accept(astVisitor);
        }


        if(createTableStmtMain != null)
        {
            createTableStmtMain.accept(astVisitor);

        } else if(selectStatement != null)
        {
            selectStatement.accept(astVisitor);

        }
    }
}
