package SingleParse.SQL.SqlStmtList.SqlStatement.CreateTableStatement;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnDefinition;
import SingleParse.SQL.SqlStmtList.SqlStatement.TableConstraint.TableConstraint;

public class SingleTableConstraintOrColumnDef extends Node {
    ColumnDefinition columnDefinition;
    TableConstraint tableConstraint;
    public SingleTableConstraintOrColumnDef(int line, int col) {
        super(line, col);
    }

    public ColumnDefinition getColumnDefinition() {
        return columnDefinition;
    }

    public void setColumnDefinition(ColumnDefinition columnDefinition) {
        this.columnDefinition = columnDefinition;
    }

    public TableConstraint getTableConstraint() {
        return tableConstraint;
    }

    public void setTableConstraint(TableConstraint tableConstraint) {
        this.tableConstraint = tableConstraint;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(tableConstraint != null)
        {
            tableConstraint.accept(astVisitor);

        }
        else if(columnDefinition != null)
        {
            columnDefinition.accept(astVisitor);

        }
    }
}
