package SingleParse.SQL.SqlStmtList.SqlStatement.AggregationFunction;

import AST.ASTVisitor;
import SingleParse.Java.AllowedName;
import SingleParse.Java.AnyStatement.SingleLineStatement.SingleLineStatement;


public class AggregationFunction extends SingleLineStatement {
    AllowedName functionName;
    JarPath jarPath;
    ClassName className;
    MethodName methodName;
    ReturnType returnType;
    AggregationFunctionParameterArray aggregationFunctionParameterArray;
    public AggregationFunction(int line, int col) {
        super(line, col);
    }

    @Override
    public int getLine() {
        return super.getLine();
    }

    @Override
    public void setLine(int line) {
        super.setLine(line);
    }

    @Override
    public void setCol(int col) {
        super.setCol(col);
    }

    @Override
    public int getCol() {
        return super.getCol();
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(functionName != null) functionName.accept(astVisitor);
        if(jarPath != null) jarPath.accept(astVisitor);
        if(className != null) className.accept(astVisitor);
        if(methodName != null) methodName.accept(astVisitor);

        if(aggregationFunctionParameterArray != null) {
            aggregationFunctionParameterArray.accept(astVisitor);
        }

        if(returnType != null) returnType.accept(astVisitor);
    }

    public AllowedName getFunctionName() {
        return functionName;
    }

    public void setFunctionName(AllowedName functionName) {
        this.functionName = functionName;
    }

    public JarPath getJarPath() {
        return jarPath;
    }

    public void setJarPath(JarPath jarPath) {
        this.jarPath = jarPath;
    }

    public ClassName getClassName() {
        return className;
    }

    public void setClassName(ClassName className) {
        this.className = className;
    }

    public MethodName getMethodName() {
        return methodName;
    }

    public void setMethodName(MethodName methodName) {
        this.methodName = methodName;
    }

    public ReturnType getReturnType() {
        return returnType;
    }

    public void setReturnType(ReturnType returnType) {
        this.returnType = returnType;
    }

    public AggregationFunctionParameterArray getAggregationFunctionParameterArray() {
        return aggregationFunctionParameterArray;
    }

    public void setAggregationFunctionParameterArray(AggregationFunctionParameterArray aggregationFunctionParameterArray) {
        this.aggregationFunctionParameterArray = aggregationFunctionParameterArray;
    }
}
