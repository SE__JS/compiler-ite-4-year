package SingleParse.SQL.SqlStmtList.SqlStatement.AggregationFunction;

import AST.ASTVisitor;
import GrammarRules.Node;

import java.util.ArrayList;

public class AggregationFunctionParameterArray extends Node {

    ArrayList<ReturnType> returnTypeArrayList;
    public AggregationFunctionParameterArray(int line, int col) {
       super(line, col);
    }

    public ArrayList<ReturnType> getReturnTypeArrayList() {
        return returnTypeArrayList;
    }

    public void setReturnTypeArrayList(ArrayList<ReturnType> returnTypeArrayList) {
        this.returnTypeArrayList = returnTypeArrayList;
    }

    @Override
    public int getLine() {
        return super.getLine();
    }

    @Override
    public void setLine(int line) {
        super.setLine(line);
    }

    @Override
    public void setCol(int col) {
        super.setCol(col);
    }

    @Override
    public int getCol() {
        return super.getCol();
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(returnTypeArrayList != null && returnTypeArrayList.size() > 0) {
            for(int i =0 ; i < returnTypeArrayList.size(); i++) {
                if(returnTypeArrayList.get(i) != null) {
                    returnTypeArrayList.get(i).accept(astVisitor);
                }
            }
        }
    }
}
