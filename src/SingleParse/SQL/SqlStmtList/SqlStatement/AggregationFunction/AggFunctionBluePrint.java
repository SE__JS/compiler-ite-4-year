package SingleParse.SQL.SqlStmtList.SqlStatement.AggregationFunction;

import java.util.ArrayList;

public class AggFunctionBluePrint {
    String jarPath;
    String className;
    String methodName;
    String returnType;
    ArrayList<String> args;
    String aggregationFunctionName;


    public AggFunctionBluePrint() {
        args = new ArrayList<>();
    }

    public String getJarPath() {
        return jarPath;
    }

    public void setJarPath(String jarPath) {
        jarPath.replace("\"", "");
        this.jarPath = jarPath.replace("\"", "");
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className.replace("\"", "");
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName.replace("\"", "");
    }

    public String getReturnType() {
        return returnType;
    }

    public void setReturnType(String returnType) {
        this.returnType = returnType.replace("\"", "");
    }

    public ArrayList<String> getArgs() {
        return args;
    }

    public void setArgs(ArrayList<String> args) {
        this.args = args;
    }

    public void addArg(String arg) {
        arg = arg.replace("\"", "");
        this.args.add(arg);
    }

    public String getAggregationFunctionName() {
        return aggregationFunctionName;
    }

    public void setAggregationFunctionName(String aggregationFunctionName) {
        this.aggregationFunctionName = aggregationFunctionName;
    }

    @Override
    public String toString() {
        return "AggFunctionBluePrint{" +
                "jarPath='" + jarPath + '\'' +
                ", className='" + className + '\'' +
                ", methodName='" + methodName + '\'' +
                ", returnType='" + returnType + '\'' +
                ", args=" + args +
                ", aggregationFunctionName='" + aggregationFunctionName + '\'' +
                '}';
    }
}
