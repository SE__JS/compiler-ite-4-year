package SingleParse.SQL.SqlStmtList.SqlStatement.AggregationFunction;

import AST.ASTVisitor;
import GrammarRules.Node;

public class ReturnType extends Node {
    String type;

    public ReturnType(int line, int col) {
        super(line, col);
    }

    @Override
    public int getLine() {
        return super.getLine();
    }

    @Override
    public void setLine(int line) {
        super.setLine(line);
    }

    @Override
    public void setCol(int col) {
        super.setCol(col);
    }

    @Override
    public int getCol() {
        return super.getCol();
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(type != null) {
            System.out.println("Type = " + type);
        }
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
