package SingleParse.Java;

import AST.ASTVisitor;
import GrammarRules.Node;
import SymbolTable.Scope;
import SymbolTable.Symbol;

public class AllowedName extends Node {
   private String name;
    Symbol symbol;
    Scope scope;
    boolean ignore;
    boolean isFunctionName = false;
  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);
  }

  public AllowedName(int line, int col) {
        super(line, col);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public Symbol getSymbol() {
        return symbol;
    }

    public void setSymbol(Symbol symbol) {
        this.symbol = symbol;
    }

    public Scope getScope() {
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public boolean isIgnore() {
        return ignore;
    }

    public boolean isFunctionName() {
        return isFunctionName;
    }

    public void setFunctionName(boolean functionName) {
        isFunctionName = functionName;
    }

    public void setIgnore(boolean ignore) {
        this.ignore = ignore;
    }
}
