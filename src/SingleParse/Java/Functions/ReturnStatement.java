package SingleParse.Java.Functions;

import AST.ASTVisitor;
import SingleParse.Java.AnyStatement.AnyStatement;
import SingleParse.Java.AnyStatement.SingleLineStatement.SingleLineStatement;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.PossibleValue.PossibleValue;
import SingleParse.SQL.SqlStmtList.SqlStatement.SqlStatement;

public class ReturnStatement extends SingleLineStatement {

  private SingleLineStatement statement;
  private PossibleValue possibleValue;
  private SqlStatement sqlStatement;

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);

    if (statement != null) {
      statement.accept(astVisitor);
    }
    if (possibleValue != null) {
      possibleValue.accept(astVisitor);
    }
    if (sqlStatement != null) {
      sqlStatement.accept(astVisitor);
    }
  }

  public SqlStatement getSqlStatement() {
    return sqlStatement;
  }

  public void setSqlStatement(SqlStatement sqlStatement) {
    this.sqlStatement = sqlStatement;
  }

  public SingleLineStatement getStatement() {
    return statement;
  }

  public void setStatement(SingleLineStatement statement) {
    this.statement = statement;
  }

  public PossibleValue getPossibleValue() {
    return possibleValue;
  }

  public void setPossibleValue(PossibleValue possibleValue) {
    this.possibleValue = possibleValue;
  }

  public ReturnStatement(int line, int col) {
    super(line, col);
  }
}
