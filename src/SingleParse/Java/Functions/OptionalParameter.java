package SingleParse.Java.Functions;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.Java.AllowedName;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.PossibleValue.PossibleValue;

public class OptionalParameter extends Node {

    AllowedName parameterName;
    PossibleValue value;

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);
    parameterName.accept(astVisitor);
    value.accept(astVisitor);

  }

  public AllowedName getParameterName() {
        return parameterName;
    }

    public void setParameterName(AllowedName parameterName) {
        this.parameterName = parameterName;
    }

    public PossibleValue getValue() {
        return value;
    }

    public void setValue(PossibleValue value) {
        this.value = value;
    }

    public OptionalParameter(int line, int col) {
        super(line, col);
    }
}
