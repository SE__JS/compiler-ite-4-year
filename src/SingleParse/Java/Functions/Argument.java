package SingleParse.Java.Functions;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.Java.AllowedName;

public class Argument extends Node {

  HigherOrderFunction higherOrderFunction;
  AllowedName allowedName;
  String nummericValue;
  String stringLiteral;
  String booleanValue;
  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);

    if (higherOrderFunction != null) {
      higherOrderFunction.accept(astVisitor);
    }
    if (allowedName != null) {
      allowedName.accept(astVisitor);
    }
    if (nummericValue != null) {
      System.out.println("nummericValue = " + nummericValue);
    }
    if (stringLiteral != null) {
      System.out.println("stringLiteral = " + stringLiteral);
    }
    if(booleanValue != null) {
      System.out.println("Boolean value: " + booleanValue);
    }
  }
  public String getActualValue() {
    if (allowedName != null) {
      return allowedName.getName();
    }
    if (nummericValue != null) {
      return "number";
    }
    if (stringLiteral != null) {
      return stringLiteral;
    }
    if(booleanValue != null) {
      return "boolean";
    }
    return "";
  }
  public String getNummericValue() {
    return nummericValue;
  }

  public void setNummericValue(String nummericValue) {
    this.nummericValue = nummericValue;
  }

  public String getStringLiteral() {
    return stringLiteral;
  }

  public void setStringLiteral(String stringLiteral) {
    this.stringLiteral = stringLiteral;
  }

  public HigherOrderFunction getHigherOrderFunction() {
    return higherOrderFunction;
  }

  public void setHigherOrderFunction(HigherOrderFunction higherOrderFunction) {
    this.higherOrderFunction = higherOrderFunction;
  }

  public AllowedName getAllowedName() {
    return allowedName;
  }

  public void setAllowedName(AllowedName allowedName) {
    this.allowedName = allowedName;
  }

  public Argument(int line, int col) {
    super(line, col);
  }

  public String getBooleanValue() {
    return booleanValue;
  }

  public void setBooleanValue(String booleanValue) {
    this.booleanValue = booleanValue;
  }
}
