package SingleParse.Java.Functions;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.Java.AllowedName;

public class Parameter extends Node {

    AllowedName parameterName;

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);

    parameterName.accept(astVisitor);
  }

  public AllowedName getParameterName() {
        return parameterName;
    }

    public void setParameterName(AllowedName parameterName) {
        this.parameterName = parameterName;
    }

    public Parameter(int line, int col) {
        super(line, col);
    }
}
