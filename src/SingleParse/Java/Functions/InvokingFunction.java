package SingleParse.Java.Functions;

import AST.ASTVisitor;
import SingleParse.Java.AllowedName;
import SingleParse.Java.AnyStatement.AnyStatement;
import SingleParse.Java.AnyStatement.SingleLineStatement.SingleLineStatement;

import java.util.ArrayList;

public class InvokingFunction extends SingleLineStatement {

  AllowedName allowedName;
  ArrayList<Argument> argumentsList = new ArrayList<>();

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);
    if(allowedName != null) {
      allowedName.accept(astVisitor);
    }
    if (argumentsList != null) {
      for (Argument arg : argumentsList) {
        arg.accept(astVisitor);
      }
    }
  }

  public AllowedName getAllowedName() {
    return allowedName;
  }

  public void setAllowedName(AllowedName allowedName) {
    this.allowedName = allowedName;
  }

  public ArrayList<Argument> getArgumentsList() {
    return argumentsList;
  }

  public void setArgumentsList(ArrayList<Argument> argumentsList) {
    this.argumentsList = argumentsList;
  }

  public InvokingFunction(int line, int col) {
    super(line, col);
  }
}
