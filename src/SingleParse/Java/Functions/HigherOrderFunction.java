package SingleParse.Java.Functions;

import AST.ASTVisitor;
import SingleParse.Java.AnyStatement.AnyStatement;

import java.util.ArrayList;

public class HigherOrderFunction extends AnyStatement {

  AnyStatement statementList;

  ArrayList<Argument> ArgumentsList;

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);
    if (statementList != null) {
      statementList.accept(null);
    }
    if (ArgumentsList != null) {
      for (Argument arg : ArgumentsList) {
        arg.accept(astVisitor);
      }
    }


  }

  public ArrayList<Argument> getArgumentsList() {
    return ArgumentsList;
  }

  public void setArgumentsList(ArrayList<Argument> argumentsList) {
    ArgumentsList = argumentsList;
  }

  public AnyStatement getStatementList() {
    return statementList;
  }

  public void setStatementList(AnyStatement statementList) {
    this.statementList = statementList;
  }

  public HigherOrderFunction(int line, int col) {
    super(line, col);
  }
}
