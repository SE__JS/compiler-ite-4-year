package SingleParse.Java;


import AST.ASTVisitor;
import SingleParse.Java.AnyStatement.AnyStatement;
import SingleParse.Java.AnyStatement.SingleLineStatement.SingleLineStatement;
import SingleParse.Java.Functions.OptionalParameter;
import SingleParse.Java.Functions.Parameter;
import SingleParse.SingleParse;

import java.util.ArrayList;

public class FunctionDefinition extends SingleLineStatement {

  AllowedName allowedName;
  ArrayList<Parameter> parameters=new ArrayList<>();
  ArrayList<OptionalParameter> optionalParameters=new ArrayList<>();

  ArrayList<AnyStatement> statements;

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);
      if(allowedName != null) {
        allowedName.accept(astVisitor);
      }

      for (Parameter param : this.parameters) {
        param.accept(astVisitor);
      }
      for (OptionalParameter opParam:this.optionalParameters) {
        opParam.accept(astVisitor);
      }
    if (statements != null){
      for (int i = 0; i < statements.size(); i++) {
        statements.get(i).accept(astVisitor);
      }
    }

  }

  public ArrayList<Parameter> getParameters() {
    return parameters;
  }

  public void setParameters(ArrayList<Parameter> parameters) {
    this.parameters = parameters;
  }

  public ArrayList<OptionalParameter> getOptionalParameters() {
    return optionalParameters;
  }

  public void setOptionalParameters(ArrayList<OptionalParameter> optionalParameters) {
    this.optionalParameters = optionalParameters;
  }

  public ArrayList<AnyStatement> getStatements() {
    return statements;
  }

  public void setStatements(ArrayList<AnyStatement> statements) {
    this.statements = statements;
  }

  public AllowedName getAllowedName() {
    return allowedName;
  }

  public void setAllowedName(AllowedName allowedName) {
    this.allowedName = allowedName;
  }

  public FunctionDefinition(int line, int col) {
    super(line, col);
  }
}
