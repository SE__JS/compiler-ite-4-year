package SingleParse.Java.AnyStatement.SwitchCase;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.Java.AnyStatement.AnyStatement;

public class DefaultCase extends Node {

  AnyStatement statements;

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);
    if (statements != null) {
      statements.accept(astVisitor);
    }
  }

  public AnyStatement getStatements() {
    return statements;
  }

  public void setStatements(AnyStatement statements) {
    this.statements = statements;
  }

  public DefaultCase(int line, int col) {
    super(line, col);
  }
}
