package SingleParse.Java.AnyStatement.SwitchCase;

import AST.ASTVisitor;
import SingleParse.Java.AnyStatement.SingleLineStatement.SingleLineStatement;

public class BreakStatement extends SingleLineStatement {
  public BreakStatement(int line, int col) {
    super(line, col);
  }

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);
  }
}
