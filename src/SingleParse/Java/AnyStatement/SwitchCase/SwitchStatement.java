package SingleParse.Java.AnyStatement.SwitchCase;

import AST.ASTVisitor;
import SingleParse.Expression.Expression;
import SingleParse.Java.AnyStatement.AnyStatement;


import java.util.ArrayList;

public class SwitchStatement extends AnyStatement {

  private Expression expression;
  private ArrayList<CaseBlock> cases = new ArrayList<>();
  private DefaultCase defaultCase;

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);
    expression.accept(astVisitor);
    if (cases != null) {
      for (int i = 0; i < cases.size(); i++) {
        cases.get(i).accept(astVisitor);
      }
    }
    if (defaultCase != null) {
      defaultCase.accept(astVisitor);
    }
  }

  public Expression getExpression() {
    return expression;
  }

  public void setExpression(Expression expression) {
    this.expression = expression;
  }

  public ArrayList<CaseBlock> getCases() {
    return cases;
  }

  public void setCases(ArrayList<CaseBlock> cases) {
    this.cases = cases;
  }

  public DefaultCase getDefaultCase() {
    return defaultCase;
  }

  public void setDefaultCase(DefaultCase defaultCase) {
    this.defaultCase = defaultCase;
  }

  public SwitchStatement(int line, int col) {
    super(line, col);
  }
}
