package SingleParse.Java.AnyStatement.SwitchCase;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.Java.AnyStatement.AnyStatement;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.PossibleValue.PossibleValue;

import java.util.ArrayList;

public class CaseBlock extends Node {

  private PossibleValue value;

  private ArrayList<AnyStatement> statements;

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);
    value.accept(astVisitor);
    if (statements != null){
      for (int i = 0; i < statements.size(); i++) {
        statements.get(i).accept(astVisitor);
      }
    }
  }

  public PossibleValue getValue() {
    return value;
  }

  public void setValue(PossibleValue value) {
    this.value = value;
  }

  public ArrayList<AnyStatement> getStatements() {
    return statements;
  }

  public void setStatements(ArrayList<AnyStatement> statements) {
    this.statements = statements;
  }

  public CaseBlock(int line, int col) {
    super(line, col);
  }
}
