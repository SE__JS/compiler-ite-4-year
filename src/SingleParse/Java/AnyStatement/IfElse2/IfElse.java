package SingleParse.Java.AnyStatement.IfElse2;

import AST.ASTVisitor;
import SingleParse.Java.AnyStatement.AnyStatement;
import SingleParse.Java.AnyStatement.CheckingBlock;

public class IfElse extends AnyStatement {

  CheckingBlock checkingBlock;

  IfStatements ifStatements;

  ElseStatements elseStatements;


  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);

    checkingBlock.accept(astVisitor);

    ifStatements.accept(astVisitor);

    if (elseStatements != null) {
      elseStatements.accept(astVisitor);
    }
  }

  public IfStatements getIfStatements() {
    return ifStatements;
  }

  public void setIfStatements(IfStatements ifStatements) {
    this.ifStatements = ifStatements;
  }

  public ElseStatements getElseStatements() {
    return elseStatements;
  }

  public void setElseStatements(ElseStatements elseStatements) {
    this.elseStatements = elseStatements;
  }

  public CheckingBlock getCheckingBlock() {
    return checkingBlock;
  }

  public void setCheckingBlock(CheckingBlock checkingBlock) {
    this.checkingBlock = checkingBlock;
  }


  public IfElse(int line, int col) {
    super(line, col);
  }
}
