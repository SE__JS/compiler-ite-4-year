package SingleParse.Java.AnyStatement.IfElse2;

import AST.ASTVisitor;
import SingleParse.Java.AnyStatement.AnyStatement;

public class ElseStatements extends AnyStatement {
  AnyStatement statements;

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);
    statements.accept(astVisitor);
  }

  public AnyStatement getStatements() {
    return statements;
  }

  public void setStatements(AnyStatement statements) {
    this.statements = statements;
  }

  public ElseStatements(int line, int col) {
    super(line, col);
  }
}
