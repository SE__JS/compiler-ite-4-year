package SingleParse.Java.AnyStatement.TernaryExpression;

import AST.ASTVisitor;
import SingleParse.Java.AllowedName;
import SingleParse.Java.AnyStatement.SingleLineStatement.SingleLineStatement;
import SingleParse.Java.AnyStatement.SingleLineStatement.TerminalNode;

import java.util.ArrayList;

public class TernaryExpression extends SingleLineStatement {
  ArrayList<TernaryExpression> ternaryExpressionArrayList;
  TerminalNode terminalNode;
  String operator;
  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);
    if(terminalNode != null) {
      terminalNode.accept(astVisitor);
    }
    if(ternaryExpressionArrayList != null && ternaryExpressionArrayList.size() > 0) {

      for(int i = 0; i < ternaryExpressionArrayList.size(); i++) {
        if(ternaryExpressionArrayList.get(i) != null) {
          ternaryExpressionArrayList.get(i).accept(astVisitor);
        }
      }
    }
    if(operator != null) {
      System.out.println("Operator { "+ operator + " }");
    }
  }

  public TernaryExpression(int line, int col) {
        super(line, col);
    }

  public ArrayList<TernaryExpression> getTernaryExpressionArrayList() {
    return ternaryExpressionArrayList;
  }

  public void setTernaryExpressionArrayList(ArrayList<TernaryExpression> ternaryExpressionArrayList) {
    this.ternaryExpressionArrayList = ternaryExpressionArrayList;
  }


  public String getOperator() {
    return operator;
  }

  public void setOperator(String operator) {
    this.operator = operator;
  }

  public TerminalNode getTerminalNode() {
    return terminalNode;
  }

  public void setTerminalNode(TerminalNode terminalNode) {
    this.terminalNode = terminalNode;
  }

  @Override
  public String toString() {
    return "TernaryExpression{" +
            "ternaryExpressionArrayList=" + ternaryExpressionArrayList +
            ", terminalNode=" + terminalNode +
            ", operator='" + operator + '\'' +
            '}';
  }
}
