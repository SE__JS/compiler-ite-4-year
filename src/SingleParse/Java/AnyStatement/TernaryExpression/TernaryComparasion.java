package SingleParse.Java.AnyStatement.TernaryExpression;

import AST.ASTVisitor;
import GrammarRules.Node;

public class TernaryComparasion extends Node {
    String operator;
    public TernaryComparasion(int line, int col) {
        super(line, col);
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    @Override
    public int getLine() {
        return super.getLine();
    }

    @Override
    public void setLine(int line) {
        super.setLine(line);
    }

    @Override
    public void setCol(int col) {
        super.setCol(col);
    }

    @Override
    public int getCol() {
        return super.getCol();
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(operator != null) {
            System.out.println("Operator = " + operator);
        }
    }
}
