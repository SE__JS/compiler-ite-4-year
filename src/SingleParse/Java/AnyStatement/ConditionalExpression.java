package SingleParse.Java.AnyStatement;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.Java.AllowedName;

public class ConditionalExpression extends Node {

  ConditionalParams leftSide;
  String comparisionFactor;
  ConditionalParams rightSide;

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);
    if (leftSide != null)
      leftSide.accept(astVisitor);

    System.out.println("comparisionFactor = " + comparisionFactor);

    if (rightSide != null)
      rightSide.accept(astVisitor);
  }

  public ConditionalParams getLeftSide() {
    return leftSide;
  }

  public void setLeftSide(ConditionalParams leftSide) {
    this.leftSide = leftSide;
  }

  public String getComparisionFactor() {
    return comparisionFactor;
  }

  public void setComparisionFactor(String comparisionFactor) {
    this.comparisionFactor = comparisionFactor;
  }

  public ConditionalParams getRightSide() {
    return rightSide;
  }

  public void setRightSide(ConditionalParams rightSide) {
    this.rightSide = rightSide;
  }

  public ConditionalExpression(int line, int col) {
    super(line, col);
  }

  @Override
  public String toString() {
    return super.toString();
  }
}
