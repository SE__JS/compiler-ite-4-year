package SingleParse.Java.AnyStatement.SingleLineStatement;

import SingleParse.Java.AnyStatement.AnyStatement;

public abstract class SingleLineStatement extends AnyStatement {
  public SingleLineStatement(int line, int col) {
    super(line, col);
  }
}
