package SingleParse.Java.AnyStatement.SingleLineStatement;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.Java.AnyStatement.ConditionalExpression;

public class NextConditionalExpr extends Node {
  private ConditionalExpression conditionalExpression;
  private String logicalOperation;

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);

    System.out.println("logicalOperation = " + logicalOperation);

    conditionalExpression.accept(astVisitor);
  }

  public ConditionalExpression getConditionalExpression() {
    return conditionalExpression;
  }

  public void setConditionalExpression(ConditionalExpression conditionalExpression) {
    this.conditionalExpression = conditionalExpression;
  }

  public String getLogicalOperation() {
    return logicalOperation;
  }

  public void setLogicalOperation(String logicalOperation) {
    this.logicalOperation = logicalOperation;
  }

  public NextConditionalExpr(int line, int col) {
    super(line, col);
  }
}
