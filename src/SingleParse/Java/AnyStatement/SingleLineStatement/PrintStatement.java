package SingleParse.Java.AnyStatement.SingleLineStatement;


import AST.ASTVisitor;
import SingleParse.Expression.Expression;
import SingleParse.Java.AllowedName;

public class PrintStatement extends SingleLineStatement {

    Expression expression;
    AllowedName allowedName;

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);

        if (expression != null) {
            expression.accept(astVisitor);
        }
    }

    public AllowedName getAllowedName() {
        return allowedName;
    }

    public void setAllowedName(AllowedName allowedName) {
        this.allowedName = allowedName;
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    public PrintStatement(int line, int col) {
        super(line, col);
    }
}
