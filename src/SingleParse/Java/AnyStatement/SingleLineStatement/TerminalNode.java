package SingleParse.Java.AnyStatement.SingleLineStatement;

import AST.ASTVisitor;


import SingleParse.Java.AllowedName;
import SingleParse.Java.AnyStatement.SingleLineStatement.TernatyStetement.TernaryStatement;
import SingleParse.Java.AnyStatement.TernaryExpression.TernaryExpression;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.ArrayAssignment.ArrayDefinition;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.ArrayAssignment.ArrayInvoking;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.PossibleValue.*;
import SingleParse.Java.Functions.InvokingFunction;
import SingleParse.SQL.SqlStmtList.SqlStatement.FactoredSelectStatement.FactoredSelectStatement;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore.SelectCore;
import SingleParse.SQL.SqlStmtList.SqlStatement.SqlStatement;

public class TerminalNode extends PossibleValue {
    private ArrayDefinition arrayDefinition;
    private AllowedName allowedName;
    private DotBlock dotBlock;
    private TernaryExpression ternaryExpression;
    private InvokingFunction invokingFunction;
    private TernaryStatement ternaryStatement;
    NummericValue nummericValue;
    BooleanValue booleanValue;
    NullValue nullValue;
    ArrayInvoking arrayInvoking;
    SqlStatement sqlStatement;

    public ArrayInvoking getArrayInvoking() {
        return arrayInvoking;
    }

    public void setArrayInvoking(ArrayInvoking arrayInvoking) {
        this.arrayInvoking = arrayInvoking;
    }

    public SqlStatement getSqlStatement() {
        return sqlStatement;
    }

    public void setSqlStatement(SqlStatement sqlStatement) {
        this.sqlStatement = sqlStatement;
    }

    public NummericValue getNummericValue() {
        return nummericValue;
    }

    public void setNummericValue(NummericValue nummericValue) {
        this.nummericValue = nummericValue;
    }

    public BooleanValue getBooleanValue() {
        return booleanValue;
    }

    public void setBooleanValue(BooleanValue booleanValue) {
        this.booleanValue = booleanValue;
    }

    public NullValue getNullValue() {
        return nullValue;
    }

    public void setNullValue(NullValue nullValue) {
        this.nullValue = nullValue;
    }

    public TerminalNode(int line, int col) {
        super(line, col);
    }

    public InvokingFunction getInvokingFunction() {
        return invokingFunction;
    }

    public void setInvokingFunction(InvokingFunction invokingFunction) {
        this.invokingFunction = invokingFunction;
    }

    public SelectCore getSelectCoreFromTerminalNode() {
        if (sqlStatement != null) {
            try {
                FactoredSelectStatement factoredSelectStatement = (FactoredSelectStatement) sqlStatement;
                return factoredSelectStatement.getSelectCore();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        return null;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if (arrayDefinition != null) {
            arrayDefinition.accept(astVisitor);
        }
        if (allowedName != null) {
            allowedName.accept(astVisitor);
        }
        if (dotBlock != null) {
            dotBlock.accept(astVisitor);
        }
        if (ternaryExpression != null) {
            ternaryExpression.accept(astVisitor);
        }
        if (invokingFunction != null) {
            invokingFunction.accept(astVisitor);
        }
        if (nummericValue != null) {
            nummericValue.accept(astVisitor);
        }
        if (booleanValue != null) {
            booleanValue.accept(astVisitor);
        }
        if (nullValue != null) {
            nullValue.accept(astVisitor);
        }
        if (arrayInvoking != null) {
            arrayInvoking.accept(astVisitor);
        }
        if (sqlStatement != null) {
            sqlStatement.accept(astVisitor);
        }
        if (ternaryStatement != null) {
            ternaryStatement.accept(astVisitor);
        }

    }

    @Override
    public TernaryStatement getTernaryStatement() {
        return ternaryStatement;
    }

    @Override
    public void setTernaryStatement(TernaryStatement ternaryStatement) {
        this.ternaryStatement = ternaryStatement;
    }

    public TernaryExpression getTernaryExpression() {
        return ternaryExpression;
    }

    public void setTernaryExpression(TernaryExpression ternaryExpression) {
        this.ternaryExpression = ternaryExpression;
    }

    public DotBlock getDotBlock() {
        return dotBlock;
    }

    public void setDotBlock(DotBlock dotBlock) {
        this.dotBlock = dotBlock;
    }

    public AllowedName getAllowedName() {
        return allowedName;
    }

    public void setAllowedName(AllowedName allowedName) {
        this.allowedName = allowedName;
    }

    public ArrayDefinition getArrayDefinition() {
        return arrayDefinition;
    }

    public void setArrayDefinition(ArrayDefinition arrayDefinition) {
        this.arrayDefinition = arrayDefinition;
    }

    @Override
    public String toString() {
        return "TerminalNode{" +
                "arrayDefinition=" + arrayDefinition +
                ", allowedName=" + allowedName +
                ", dotBlock=" + dotBlock +
                ", ternaryExpression=" + ternaryExpression +
                ", invokingFunction=" + invokingFunction +
                ", ternaryStatement=" + ternaryStatement +
                ", nummericValue=" + nummericValue +
                ", booleanValue=" + booleanValue +
                ", nullValue=" + nullValue +
                ", arrayInvoking=" + arrayInvoking +
                ", sqlStatement=" + sqlStatement +
                '}';
    }
}