package SingleParse.Java.AnyStatement.SingleLineStatement.TernatyStetement;

import AST.ASTVisitor;
import SingleParse.Java.AllowedName;

import java.util.ArrayList;

public class RightTernaryStatement extends TernaryStatement {

  /**
   * zero or more
   */
  private ArrayList<SinglePlusMinus> plusMinusList;
  private AllowedName allowedName;

  private RightTernaryRightSide rightSide;

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);

    if (plusMinusList != null && !plusMinusList.isEmpty()) {
      for (int i = 0; i < plusMinusList.size(); i++) {
        plusMinusList.get(i).accept(astVisitor);
      }
    }
    allowedName.accept(astVisitor);
    if (rightSide != null) {
      rightSide.accept(astVisitor);
    }
  }

  public ArrayList<SinglePlusMinus> getPlusMinusList() {
    return plusMinusList;
  }

  public void setPlusMinusList(ArrayList<SinglePlusMinus> plusMinusList) {
    this.plusMinusList = plusMinusList;
  }

  public AllowedName getAllowedName() {
    return allowedName;
  }

  public void setAllowedName(AllowedName allowedName) {
    this.allowedName = allowedName;
  }

  public RightTernaryRightSide getRightSide() {
    return rightSide;
  }

  public void setRightSide(RightTernaryRightSide rightSide) {
    this.rightSide = rightSide;
  }

  public RightTernaryStatement(int line, int col) {
    super(line, col);
  }
}
