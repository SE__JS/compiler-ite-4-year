package SingleParse.Java.AnyStatement.SingleLineStatement.TernatyStetement;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.PossibleValue.PossibleValue;

import java.util.ArrayList;

public class LeftTernaryRightSide extends Node {

  /**
   * Zero or more
   */
  private ArrayList<SinglePlusMinus> plusMinusList;

  private PossibleValue possibleValue;

  private String ternaryOperator;

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);

    if (plusMinusList != null && !plusMinusList.isEmpty()) {
      for (int i = 0; i < plusMinusList.size(); i++) {
        plusMinusList.get(i).accept(astVisitor);
      }
    } else {
      System.out.println("ternaryOperator = " + ternaryOperator);
    }
  }

  public ArrayList<SinglePlusMinus> getPlusMinusList() {
    return plusMinusList;
  }

  public void setPlusMinusList(ArrayList<SinglePlusMinus> plusMinusList) {
    this.plusMinusList = plusMinusList;
  }

  public PossibleValue getPossibleValue() {
    return possibleValue;
  }

  public void setPossibleValue(PossibleValue possibleValue) {
    this.possibleValue = possibleValue;
  }

  public String getTernaryOperator() {
    return ternaryOperator;
  }

  public void setTernaryOperator(String ternaryOperator) {
    this.ternaryOperator = ternaryOperator;
  }

  public LeftTernaryRightSide(int line, int col) {
    super(line, col);
  }
}
