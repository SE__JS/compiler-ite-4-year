package SingleParse.Java.AnyStatement.SingleLineStatement.TernatyStetement;

import AST.ASTVisitor;
import GrammarRules.Node;

public class SinglePlusMinus extends Node {
    String value;

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);

    System.out.println("value = " + value);
  }

  public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public SinglePlusMinus(int line, int col) {
        super(line, col);
    }
}
