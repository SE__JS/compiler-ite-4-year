package SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement;

import SingleParse.Java.AnyStatement.SingleLineStatement.SingleLineStatement;

public abstract class VarStatement extends SingleLineStatement {
    public VarStatement(int line, int col) {
        super(line, col);
    }
}
