package SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.PossibleValue;

import AST.ASTVisitor;

public class NummericValue extends PossibleValue {

    String Value;

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);
    System.out.println("Value = " + Value);
  }

  public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public NummericValue(int line, int col) {
        super(line, col);
    }
}
