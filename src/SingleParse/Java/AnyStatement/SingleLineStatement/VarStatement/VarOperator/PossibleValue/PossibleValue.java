package SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.PossibleValue;

import AST.ASTVisitor;
import SingleParse.Expression.Expression;
import SingleParse.Java.AllowedName;
import SingleParse.Java.AnyStatement.SingleLineStatement.TernatyStetement.TernaryStatement;
import SingleParse.Java.AnyStatement.TernaryExpression.TernaryExpression;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.ArrayAssignment.ArrayDefinition;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.VarOperator;
import SingleParse.Java.Functions.InvokingFunction;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore.SelectCore;
import SingleParse.SQL.SqlStmtList.SqlStatement.SqlStatement;

public class PossibleValue extends VarOperator {
  private Expression expression;
  private ArrayDefinition arrayDefinition;
  private AllowedName allowedName;
  private DotBlock dotBlock;
  private TernaryExpression ternaryExpression;
  private InvokingFunction invokingFunction;
  private String valueType;
  TernaryStatement ternaryStatement;
  public InvokingFunction getInvokingFunction() {
    return invokingFunction;
  }

  public void setInvokingFunction(InvokingFunction invokingFunction) {
    this.invokingFunction = invokingFunction;
  }
  public static SqlStatement getSqlStatement(PossibleValue possibleValue) {
    if(possibleValue.getExpression() != null) {
      if(possibleValue.getExpression().getTerminalNode() != null) {
        return possibleValue.getExpression().getTerminalNode().getSqlStatement();
      }
    }
   return null;
  }
  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);
    if (expression != null) {
      expression.accept(astVisitor);
    }
    if (arrayDefinition != null) {
      arrayDefinition.accept(astVisitor);
    }
    if (allowedName != null) {
      allowedName.accept(astVisitor);
    }
    if (dotBlock != null) {
      dotBlock.accept(astVisitor);
    }
    if (ternaryExpression != null) {
      ternaryExpression.accept(astVisitor);
    }
    if(invokingFunction != null)
    {
      invokingFunction.accept(astVisitor);
    }
    if(ternaryStatement != null) {
      ternaryStatement.accept(astVisitor);
    }

    if(valueType != null) {
      System.out.println("AST_VISITOR --> Value type: " + valueType);
    }

  }

  public TernaryExpression getTernaryExpression() {
    return ternaryExpression;
  }

  public void setTernaryExpression(TernaryExpression ternaryExpression) {
    this.ternaryExpression = ternaryExpression;
  }

  public DotBlock getDotBlock() {
    return dotBlock;
  }

  public void setDotBlock(DotBlock dotBlock) {
    this.dotBlock = dotBlock;
  }

  public AllowedName getAllowedName() {
    return allowedName;
  }

  public void setAllowedName(AllowedName allowedName) {
    this.allowedName = allowedName;
  }

  public PossibleValue(int line, int col) {
    super(line, col);
  }

  public Expression getExpression() {
    return expression;
  }

  public void setExpression(Expression expression) {
    this.expression = expression;
  }

  public ArrayDefinition getArrayDefinition() {
    return arrayDefinition;
  }

  public void setArrayDefinition(ArrayDefinition arrayDefinition) {
    this.arrayDefinition = arrayDefinition;
  }

  public String getValueType() {
    return valueType;
  }

  public void setValueType(String valueType) {
    this.valueType = valueType;
  }

  public TernaryStatement getTernaryStatement() {
    return ternaryStatement;
  }

  public void setTernaryStatement(TernaryStatement ternaryStatement) {
    this.ternaryStatement = ternaryStatement;
  }
}
