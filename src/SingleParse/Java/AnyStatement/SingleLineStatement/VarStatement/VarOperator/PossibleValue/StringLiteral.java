package SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.PossibleValue;

import AST.ASTVisitor;

public class StringLiteral extends PossibleValue {
  String value;
  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);
    System.out.println("value = " + value);
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public StringLiteral(int line, int col) {
        super(line, col);
    }
}
