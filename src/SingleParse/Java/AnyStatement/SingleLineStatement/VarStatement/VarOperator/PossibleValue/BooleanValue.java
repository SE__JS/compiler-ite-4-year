package SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.PossibleValue;

import AST.ASTVisitor;

public class BooleanValue extends PossibleValue {
    private String booleanValue;

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);

    System.out.println("booleanValue = " + booleanValue);
  }

  public BooleanValue(int line, int col) {
        super(line, col);
    }

    public String getBooleanValue() {
        return booleanValue;
    }

    public void setBooleanValue(String booleanValue) {
        this.booleanValue = booleanValue;
    }
}
