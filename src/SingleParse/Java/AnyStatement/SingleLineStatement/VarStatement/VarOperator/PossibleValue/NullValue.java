package SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.PossibleValue;

import AST.ASTVisitor;

public class NullValue extends PossibleValue {
  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);
  }

  public NullValue(int line, int col) {
        super(line, col);
    }
}
