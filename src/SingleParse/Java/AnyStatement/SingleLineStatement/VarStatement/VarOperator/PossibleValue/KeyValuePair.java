package SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.PossibleValue;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.Java.AllowedName;

public class KeyValuePair extends Node {

  AllowedName key;
  PossibleValue value;

  @Override
  public void accept(ASTVisitor astVisitor) {
    // astVisitor.visit(this);
    key.accept(astVisitor);
    value.accept(astVisitor);
  }

  public PossibleValue getValue() {
    return value;
  }

  public void setValue(PossibleValue value) {
    this.value = value;
  }

  public AllowedName getKey() {
    return key;
  }

  public void setKey(AllowedName key) {
    this.key = key;
  }


  public KeyValuePair(int line, int col) {
    super(line, col);
  }
}
