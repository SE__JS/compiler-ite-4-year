package SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.PossibleValue;

import AST.ASTVisitor;

import java.util.ArrayList;

public class JsonObject extends PossibleValue {

  ArrayList<KeyValuePair> pairs = new ArrayList<>();

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);

    if (pairs != null) {
      for (int i = 0; i < pairs.size(); i++) {
        pairs.get(i).accept(astVisitor);
      }
    }
  }

  public JsonObject(int line, int col) {
    super(line, col);
  }

  public ArrayList<KeyValuePair> getPairs() {
    return pairs;
  }

  public void setPairs(ArrayList<KeyValuePair> pairs) {
    this.pairs = pairs;
  }


}
