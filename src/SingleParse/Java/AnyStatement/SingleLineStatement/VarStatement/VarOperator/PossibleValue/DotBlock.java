package SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.PossibleValue;

import AST.ASTVisitor;
import SingleParse.Java.AllowedName;
import SingleParse.Java.AnyStatement.SingleLineStatement.SingleLineStatement;

import java.util.ArrayList;

public class DotBlock extends SingleLineStatement {

  private DotBlock leftSide;
  private String rightSide;
  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);

    /*if(leftSide!=null){
      leftSide.accept(astVisitor);
    }
    if(rightSide!=null){
      System.out.println("rightSide = " + rightSide);
    }*/
  }
  public static void chainedObjects(DotBlock dotBlock, ArrayList<String> ans) {

    if(dotBlock.leftSide != null) {
      chainedObjects(dotBlock.leftSide, ans);
    }
    ans.add(dotBlock.rightSide);
  }
  public DotBlock(int line, int col) {
    super(line, col);
  }

  public DotBlock getLeftSide() {
    return leftSide;
  }

  public void setLeftSide(DotBlock leftSide) {
    this.leftSide = leftSide;
  }

  public String getRightSide() {
    return rightSide;
  }

  public void setRightSide(String rightSide) {
    this.rightSide = rightSide;
  }

}
