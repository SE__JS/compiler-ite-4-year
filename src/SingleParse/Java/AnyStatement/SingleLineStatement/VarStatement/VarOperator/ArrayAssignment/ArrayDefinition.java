package SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.ArrayAssignment;

import AST.ASTVisitor;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.PossibleValue.PossibleValue;

import java.util.ArrayList;

public class ArrayDefinition extends ArrayAssignment {
  ArrayList<PossibleValue> values = new ArrayList<>();

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);

    if (values != null) {
      for (int i = 0; i < values.size(); i++) {
        values.get(i).accept(astVisitor);
      }
    }

  }

  public ArrayList<PossibleValue> getValues() {
    return values;
  }

  public void setValues(ArrayList<PossibleValue> values) {
    this.values = values;
  }

  public ArrayDefinition(int line, int col) {
    super(line, col);
  }
}
