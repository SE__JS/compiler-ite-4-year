package SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.ArrayAssignment;

import AST.ASTVisitor;
import SingleParse.Java.AllowedName;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.PossibleValue.PossibleValue;

public class ArrayInvoking extends PossibleValue {
  private AllowedName arrayName;
  private PossibleValue index;

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);
    arrayName.accept(astVisitor);
    index.accept(astVisitor);
  }

  public ArrayInvoking(int line, int col) {
    super(line, col);
  }

  public AllowedName getArrayName() {
    return arrayName;
  }

  public void setArrayName(AllowedName arrayName) {
    this.arrayName = arrayName;
  }

  public PossibleValue getIndex() {
    return index;
  }

  public void setIndex(PossibleValue index) {
    this.index = index;
  }
}
