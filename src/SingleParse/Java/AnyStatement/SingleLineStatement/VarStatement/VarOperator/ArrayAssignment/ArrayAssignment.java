package SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.ArrayAssignment;

import AST.ASTVisitor;
import SingleParse.Java.AllowedName;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.VarOperator;

public class ArrayAssignment extends VarOperator {

  private ArrayDefinition arrayDefinition;
  private AllowedName allowedName;

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);
    if (arrayDefinition != null) {
      arrayDefinition.accept(astVisitor);
    }
    if (allowedName != null) {
      allowedName.accept(astVisitor);
    }
  }

  public ArrayAssignment(int line, int col) {
    super(line, col);
  }

  public ArrayDefinition getArrayDefinition() {
    return arrayDefinition;
  }

  public void setArrayDefinition(ArrayDefinition arrayDefinition) {
    this.arrayDefinition = arrayDefinition;
  }

  public AllowedName getAllowedName() {
    return allowedName;
  }

  public void setAllowedName(AllowedName allowedName) {
    this.allowedName = allowedName;
  }
}
