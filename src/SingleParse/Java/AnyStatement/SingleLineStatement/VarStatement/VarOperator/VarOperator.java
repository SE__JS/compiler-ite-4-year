package SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator;

import AST.ASTVisitor;
import SingleParse.Java.AnyStatement.SingleLineStatement.TerminalNode;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.ArrayAssignment.ArrayAssignment;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.PossibleValue.PossibleValue;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarStatement;
import SingleParse.Java.AnyStatement.TernaryExpression.TernaryExpression;
import SingleParse.SQL.SqlStmtList.SqlStatement.SqlStatement;

public class VarOperator extends VarStatement {

    private ArrayAssignment arrayAssignment;
    private PossibleValue possibleValue;

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if (arrayAssignment != null) {
            arrayAssignment.accept(astVisitor);
        }
        if (possibleValue != null) {
            possibleValue.accept(astVisitor);
        }
    }

    public static SqlStatement getValueAsSqlStatement(VarOperator varOperator) {
        PossibleValue possibleValue = varOperator.getPossibleValue();
        if (possibleValue != null) {
            TernaryExpression ternaryExpression = possibleValue.getTernaryExpression();
            if (ternaryExpression != null) {
                TerminalNode terminalNode = ternaryExpression.getTerminalNode();
                if (terminalNode != null) {
                    SqlStatement sqlStatement = terminalNode.getSqlStatement();
                    if (sqlStatement != null) {
                        return sqlStatement;
                    }
                }
            }
        }
        return null;
    }

    public PossibleValue getPossibleValue() {
        return possibleValue;
    }

    public void setPossibleValue(PossibleValue possibleValue) {
        this.possibleValue = possibleValue;
    }

    public ArrayAssignment getArrayAssignment() {
        return arrayAssignment;
    }

    public void setArrayAssignment(ArrayAssignment arrayAssignment) {
        this.arrayAssignment = arrayAssignment;
    }

    public VarOperator(int line, int col) {
        super(line, col);
    }


}
