package SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement;

import AST.ASTVisitor;
import SingleParse.Java.AllowedName;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.PossibleValue.PossibleValue;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.VarOperator;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore.SelectCore;
import SymbolTable.SymbolTable;
import SymbolTable.Type;
import javafx.util.Pair;

public class VarAssignment extends VarStatement {

    private AllowedName allowedName;
    private VarOperator varOperator;

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if (allowedName != null) {
            allowedName.accept(astVisitor);

        }
        if (varOperator != null) {
            varOperator.accept(astVisitor);
        }
    }

    public static Pair<String, String> getVariableAndItsAssignmentType(VarAssignment varAssignment) {
        String variableName = varAssignment.getAllowedName().getName();
        String variableType = "";
        VarOperator varOperator = varAssignment.getVarOperator();
        if (varOperator != null) {
            PossibleValue possibleValue = varOperator.getPossibleValue();
            if (possibleValue != null) {
                if (possibleValue.getAllowedName() != null)
                {
                    String n = possibleValue.getAllowedName().getName();
                    variableType = (n.startsWith("\"") || n.startsWith("\'")) ? "string" : n;
                }
                else {
                    variableType = possibleValue.getValueType();
                }
            }
        }
        return new Pair<>(variableName, variableType);
    }

    public AllowedName getAllowedName() {
        return allowedName;
    }

    public void setAllowedName(AllowedName allowedName) {
        this.allowedName = allowedName;
    }

    public VarOperator getVarOperator() {
        return varOperator;
    }

    public void setVarOperator(VarOperator varOperator) {
        this.varOperator = varOperator;
    }

    public VarAssignment(int line, int col) {
        super(line, col);
    }
}
