package SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement;

import AST.ASTVisitor;
import SingleParse.Java.AllowedName;
import SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement.VarOperator.VarOperator;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore.SelectCore;
import SymbolTable.Symbol;
import javafx.util.Pair;

public class VarArgument extends VarDefinition {

    private AllowedName allowedName;
    private VarArgument varArgument;
    private VarOperator varOperator;

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);

        if (allowedName != null) {
            allowedName.accept(astVisitor);
        }

        if (varArgument != null) {
            varArgument.accept(astVisitor);
        }
        if (varOperator != null) {
            varOperator.accept(astVisitor);
        }
    }
    public AllowedName getAllowedName() {
        return allowedName;
    }

    public void setAllowedName(AllowedName allowedName) {
        this.allowedName = allowedName;
    }

    public VarArgument getVarArgument() {
        return varArgument;
    }

    public void setVarArgument(VarArgument varArgument) {
        this.varArgument = varArgument;
    }

    public VarArgument(int line, int col) {
        super(line, col);
    }

    public VarOperator getVarOperator() {
        return varOperator;
    }

    public void setVarOperator(VarOperator varOperator) {
        this.varOperator = varOperator;
    }

    public static Pair<String, String> getVarAndType(VarArgument varArgument) {
        Pair<String, String> p;
        String variableName = varArgument.getAllowedName().getName();
        if (varArgument.getVarOperator() != null) {
            if (varArgument.getVarOperator().getPossibleValue() != null) {
                if (varArgument.getVarOperator().getPossibleValue().getAllowedName() != null) {
                    String t = varArgument.getVarOperator().getPossibleValue().getAllowedName().getName();
                    if (t.startsWith("\"") || t.startsWith("\'")) t = "string";
                    p = new Pair<>(variableName, t);
                    return p;
                } else { // Number or Boolean
                    p = new Pair<>(variableName, varArgument.getVarOperator().getPossibleValue().getValueType());
                    return p;
                }
            }
        }
        return null;
    }
}
