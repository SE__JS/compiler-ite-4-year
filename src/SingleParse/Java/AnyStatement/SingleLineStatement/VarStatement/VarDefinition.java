package SingleParse.Java.AnyStatement.SingleLineStatement.VarStatement;

import AST.ASTVisitor;

import java.util.ArrayList;

public class VarDefinition extends VarStatement {
  ArrayList<VarArgument> varArguments = new ArrayList<>();

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);

    if (varArguments != null) {
      for (int i = 0; i < varArguments.size(); i++) {
        varArguments.get(i).accept(astVisitor);
      }
    }

  }

  public VarDefinition(int line, int col) {
    super(line, col);
  }

  public ArrayList<VarArgument> getVarArguments() {
    return varArguments;
  }

  public void setVarArguments(ArrayList<VarArgument> varArguments) {
    this.varArguments = varArguments;
  }
}
