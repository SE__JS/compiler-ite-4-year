package SingleParse.Java.AnyStatement;

import AST.ASTVisitor;

import java.util.ArrayList;

public class ScopeBlock extends AnyStatement {

    ArrayList<AnyStatement> statements = new ArrayList<>();

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);

    for (int i = 0; i < statements.size(); i++) {
      statements.get(i).accept(astVisitor);
    }

  }

  public ArrayList<AnyStatement> getStatements() {
        return statements;
    }

    public void setStatements(ArrayList<AnyStatement> statements) {
        this.statements = statements;
    }

    public ScopeBlock(int line, int col) {
        super(line, col);
    }
}
