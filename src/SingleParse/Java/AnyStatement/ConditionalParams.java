package SingleParse.Java.AnyStatement;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.Java.AllowedName;

public class ConditionalParams extends Node {
    AllowedName allowedName;
    String value;

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);

    if(allowedName!=null){
      allowedName.accept(astVisitor);
    }
    if(value!=null){
      System.out.println("ConditionalParamValue = " + value);
    }
  }

  public AllowedName getAllowedName() {
        return allowedName;
    }

    public void setAllowedName(AllowedName allowedName) {
        this.allowedName = allowedName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public ConditionalParams(int line, int col) {
        super(line, col);
    }
}

