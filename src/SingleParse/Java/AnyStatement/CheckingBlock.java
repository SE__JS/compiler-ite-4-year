package SingleParse.Java.AnyStatement;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.Java.AnyStatement.SingleLineStatement.NextConditionalExpr;

import java.util.ArrayList;

public class CheckingBlock extends Node {
  private ConditionalExpression baseExpression;
  private ArrayList<NextConditionalExpr> nextConditionalExprs;

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);

    baseExpression.accept(astVisitor);

    if (nextConditionalExprs != null) {
      for (NextConditionalExpr temp :
        nextConditionalExprs) {
        temp.accept(astVisitor);
      }
    }
  }

  public ArrayList<NextConditionalExpr> getNextConditionalExprs() {
    return nextConditionalExprs;
  }

  public void setNextConditionalExprs(ArrayList<NextConditionalExpr> nextConditionalExprs) {
    this.nextConditionalExprs = nextConditionalExprs;
  }

  public ConditionalExpression getBaseExpression() {
    return baseExpression;
  }

  public void setBaseExpression(ConditionalExpression baseExpression) {
    this.baseExpression = baseExpression;
  }

  public CheckingBlock(int line, int col) {
    super(line, col);
  }
}
