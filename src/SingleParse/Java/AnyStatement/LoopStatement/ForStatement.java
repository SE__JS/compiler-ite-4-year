package SingleParse.Java.AnyStatement.LoopStatement;

import AST.ASTVisitor;
import SingleParse.Java.AnyStatement.AnyStatement;
import SingleParse.Java.AnyStatement.ConditionalExpression;
import SingleParse.Java.AnyStatement.SingleLineStatement.SingleLineStatement;

public class ForStatement extends AnyStatement {
  private SingleLineStatement initialStatement;
  private SingleLineStatement singleLoopStatement;
  private AnyStatement statementList;
  private ConditionalExpression conditionalExpression;

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);
    if (initialStatement != null) {
      System.out.println("initialization statements");
      initialStatement.accept(astVisitor);
    }
    if (conditionalExpression != null) {
      conditionalExpression.accept(astVisitor);
    }
    if (singleLoopStatement != null) {
      System.out.println("start iteration statement");
      singleLoopStatement.accept(astVisitor);
    }
    statementList.accept(astVisitor);
  }

  public void setInitialStatement(SingleLineStatement initialStatement) {
    this.initialStatement = initialStatement;
  }

  public void setSingleLoopStatement(SingleLineStatement singleLoopStatement) {
    this.singleLoopStatement = singleLoopStatement;
  }

  public AnyStatement getInitialStatement() {
    return initialStatement;
  }

  public AnyStatement getSingleLoopStatement() {
    return singleLoopStatement;
  }


  public AnyStatement getStatementList() {
    return statementList;
  }

  public void setStatementList(AnyStatement statementList) {
    this.statementList = statementList;
  }

  public ConditionalExpression getConditionalExpression() {
    return conditionalExpression;
  }

  public void setConditionalExpression(ConditionalExpression conditionalExpression) {
    this.conditionalExpression = conditionalExpression;
  }

  //AnyStatement
  public ForStatement(int line, int col) {
    super(line, col);
  }
}
