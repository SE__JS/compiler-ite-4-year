package SingleParse.Java.AnyStatement.LoopStatement;

import AST.ASTVisitor;
import SingleParse.Java.AnyStatement.AnyStatement;
import SingleParse.Java.Functions.HigherOrderFunction;
import SingleParse.Java.Functions.InvokingFunction;

public class ForEachStatement extends AnyStatement {

    HigherOrderFunction higherOrderFunction;
    InvokingFunction invokingFunction;

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);
    if(higherOrderFunction!=null){
      higherOrderFunction.accept(astVisitor);
    }else{
      invokingFunction.accept(astVisitor);
    }
  }

  public HigherOrderFunction getHigherOrderFunction() {
        return higherOrderFunction;
    }

    public void setHigherOrderFunction(HigherOrderFunction higherOrderFunction) {
        this.higherOrderFunction = higherOrderFunction;
    }

    public InvokingFunction getInvokingFunction() {
        return invokingFunction;
    }

    public void setInvokingFunction(InvokingFunction invokingFunction) {
        this.invokingFunction = invokingFunction;
    }

    public ForEachStatement(int line, int col) {
        super(line, col);
    }
}
