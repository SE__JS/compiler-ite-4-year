package SingleParse.Java.AnyStatement.LoopStatement;

import AST.ASTVisitor;
import SingleParse.Java.AnyStatement.AnyStatement;
import SingleParse.Java.AnyStatement.CheckingBlock;
import SingleParse.Java.AnyStatement.ConditionalExpression;

public class WhileStatement extends AnyStatement {

    CheckingBlock checkingBlock;
    AnyStatement anyStatement;

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);
    checkingBlock.accept(astVisitor);
    anyStatement.accept(astVisitor);
  }

  public CheckingBlock getCheckingBlock() {
        return checkingBlock;
    }

    public void setCheckingBlock(CheckingBlock checkingBlock) {
        this.checkingBlock = checkingBlock;
    }

    public AnyStatement getAnyStatement() {
        return anyStatement;
    }

    public void setAnyStatement(AnyStatement anyStatement) {
        this.anyStatement = anyStatement;
    }

    public WhileStatement(int line, int col) {
        super(line, col);
    }

}
