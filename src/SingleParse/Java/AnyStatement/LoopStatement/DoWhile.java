package SingleParse.Java.AnyStatement.LoopStatement;

import AST.ASTVisitor;
import GrammarRules.Node;
import SingleParse.Java.AnyStatement.AnyStatement;
import SingleParse.Java.AnyStatement.CheckingBlock;

import java.util.ArrayList;

public class DoWhile extends AnyStatement {
    ArrayList<AnyStatement> anyStatements;
    CheckingBlock checkingBlock;
    public DoWhile(int line, int col) {
        super(line, col);
    }

    public ArrayList<AnyStatement> getAnyStatements() {
        return anyStatements;
    }

    public void setAnyStatements(ArrayList<AnyStatement> anyStatements) {
        this.anyStatements = anyStatements;
    }

    public CheckingBlock getCheckingBlock() {
        return checkingBlock;
    }

    public void setCheckingBlock(CheckingBlock checkingBlock) {
        this.checkingBlock = checkingBlock;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);
        if(anyStatements != null)
        {
            for(AnyStatement anyStatement: anyStatements)
            {
                anyStatement.accept(astVisitor);
            }
        }
        if(checkingBlock != null)
        {
            checkingBlock.accept(astVisitor);
        }

    }
}
