package SingleParse.Expression;

import AST.ASTVisitor;
import GrammarRules.Node;

public class Or extends Node {

    String content;

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);

    System.out.println("content = " + content);
  }

  public Or(int line, int col) {
        super(line, col);
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
