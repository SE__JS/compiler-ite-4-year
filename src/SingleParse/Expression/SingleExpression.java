package SingleParse.Expression;

public class SingleExpression {
    String leftSide;
    String rightSide;
    String operatorInBetween;
    String aggOperator;
    String aggValue;

    public SingleExpression() {}
    public SingleExpression(String leftSide, String rightSide, String operatorInBetween) {
        this.leftSide = leftSide;
        this.rightSide = rightSide;
        this.operatorInBetween = operatorInBetween;
    }

    public String getLeftSide() {
        return leftSide;
    }

    public void setLeftSide(String leftSide) {
        this.leftSide = leftSide;
    }

    public String getRightSide() {
        return rightSide;
    }

    public void setRightSide(String rightSide) {
        this.rightSide = rightSide;
    }

    public String getOperatorInBetween() {
        return operatorInBetween;
    }

    public void setOperatorInBetween(String operatorInBetween) {
        this.operatorInBetween = operatorInBetween;
    }

    public String getAggOperator() {
        return aggOperator;
    }

    public void setAggOperator(String aggOperator) {
        this.aggOperator = aggOperator;
    }

    public String getAggValue() {
        return aggValue;
    }

    public void setAggValue(String aggValue) {
        this.aggValue = aggValue;
    }

    @Override
    public String toString() {
        return "SingleExpression{" +
                "leftSide='" + leftSide + '\'' +
                ", rightSide='" + rightSide + '\'' +
                ", operatorInBetween='" + operatorInBetween + '\'' +
                ", aggOperator='" + aggOperator + '\'' +
                ", aggValue='" + aggValue + '\'' +
                '}';
    }
}
