package SingleParse.Expression;

import AST.ASTVisitor;
import CG.Mappers.SQLAGGTOJAVACONVERTER;
import GrammarRules.Node;
import SingleParse.Java.AllowedName;
import SingleParse.Java.AnyStatement.SingleLineStatement.TerminalNode;
import SingleParse.Java.AnyStatement.TernaryExpression.TernaryExpression;
import SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnName;
import SingleParse.SQL.SqlStmtList.SqlStatement.MainSql.LiteralValue;
import SingleParse.SQL.SqlStmtList.SqlStatement.MainSql.TableName;
import SingleParse.SQL.SqlStmtList.SqlStatement.SelectCore.SelectCore;
import com.sun.tracing.dtrace.FunctionName;
import javafx.util.Pair;

import java.util.ArrayList;


public class Expression extends Node {

    SqlColumnName sqlColumnName;
    ArrayList<Expression> expressionArrayList;

    TernaryExpression ternaryExpression;
    TerminalNode terminalNode;

    MathFirstPriorty mathFirstPriorty;
    MathSecondPriorty mathSecondPriorty;
    BitwiseOperator bitwiseOperator;
    LogicalOperator logicalOperator;
    SqlAndOr sqlAndOr;
    Or or;
    SqlLogicalOperator sqlLogicalOperator;
    ListOfValues listOfValues;
    AllowedName functionName;

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);

        if (expressionArrayList != null && expressionArrayList.size() > 0) {
            for (int i = 0; i < expressionArrayList.size(); i++) {
                expressionArrayList.get(i).accept(astVisitor);
            }
        }

        if (terminalNode != null) {
            terminalNode.accept(astVisitor);
        }
        if (mathFirstPriorty != null) {
            mathFirstPriorty.accept(astVisitor);
        }
        if (mathSecondPriorty != null) {
            mathSecondPriorty.accept(astVisitor);
        }
        if (bitwiseOperator != null) {
            bitwiseOperator.accept(astVisitor);
        }
        if (logicalOperator != null) {
            logicalOperator.accept(astVisitor);
        }
        if (sqlAndOr != null) {
            sqlAndOr.accept(astVisitor);
        }
        if (sqlLogicalOperator != null) {
            sqlLogicalOperator.accept(astVisitor);
        }
        if (sqlColumnName != null) {
            sqlColumnName.accept(astVisitor);
        }
        if (or != null) {
            or.accept(astVisitor);
        }
        if (ternaryExpression != null) {
            ternaryExpression.accept(astVisitor);
        }
        if (functionName != null) {
            functionName.accept(astVisitor);
        }
        if (listOfValues != null) {
            listOfValues.accept(astVisitor);
        }
    }

    public AllowedName getFunctionName() {
        return functionName;
    }

    public void setFunctionName(AllowedName functionName) {
        this.functionName = functionName;
    }

    public SelectCore getSelectCoreFromExpression() {
        if (ternaryExpression != null) {
            if (ternaryExpression.getTerminalNode() != null) {
                return ternaryExpression.getTerminalNode().getSelectCoreFromTerminalNode();
            }
        }
        return null;
    }

    public String getWhereVariableName() {
        if (sqlColumnName != null) {
            if (sqlColumnName.getColumnName() != null) {
                if (sqlColumnName.getColumnName().getAllowedName() != null) {
                    return sqlColumnName.getColumnName().getAllowedName().getName();
                }
            }
        }
        return "";
    }

    public String getConditionOperator() {
        if (sqlLogicalOperator != null) {
            return sqlLogicalOperator.getContent();
        }
        return "";
    }

    public BitwiseOperator getBitwiseOperator() {
        return bitwiseOperator;
    }

    public void setBitwiseOperator(BitwiseOperator bitwiseOperator) {
        this.bitwiseOperator = bitwiseOperator;
    }

    public LogicalOperator getLogicalOperator() {
        return logicalOperator;
    }

    public void setLogicalOperator(LogicalOperator logicalOperator) {
        this.logicalOperator = logicalOperator;
    }

    public SqlAndOr getSqlAndOr() {
        return sqlAndOr;
    }

    public void setSqlAndOr(SqlAndOr sqlAndOr) {
        this.sqlAndOr = sqlAndOr;
    }

    public Or getOr() {
        return or;
    }

    public void setOr(Or or) {
        this.or = or;
    }

    public SqlLogicalOperator getSqlLogicalOperator() {
        return sqlLogicalOperator;
    }

    public void setSqlLogicalOperator(SqlLogicalOperator sqlLogicalOperator) {
        this.sqlLogicalOperator = sqlLogicalOperator;
    }


    public ArrayList<Expression> getExpressionArrayList() {
        return expressionArrayList;
    }

    public void setExpressionArrayList(ArrayList<Expression> expressionArrayList) {
        this.expressionArrayList = expressionArrayList;
    }

    public TerminalNode getTerminalNode() {
        return terminalNode;
    }

    public void setTerminalNode(TerminalNode terminalNode) {
        this.terminalNode = terminalNode;
    }

    public MathFirstPriorty getMathFirstPriorty() {
        return mathFirstPriorty;
    }

    public void setMathFirstPriorty(MathFirstPriorty mathFirstPriorty) {
        this.mathFirstPriorty = mathFirstPriorty;
    }

    public MathSecondPriorty getMathSecondPriorty() {
        return mathSecondPriorty;
    }

    public void setMathSecondPriorty(MathSecondPriorty mathSecondPriorty) {
        this.mathSecondPriorty = mathSecondPriorty;
    }

    public Expression(int line, int col) {
        super(line, col);
    }

    public TernaryExpression getTernaryExpression() {
        return ternaryExpression;
    }

    public void setTernaryExpression(TernaryExpression ternaryExpression) {
        this.ternaryExpression = ternaryExpression;
    }

    public SqlColumnName getSqlColumnName() {
        return sqlColumnName;
    }

    public void setSqlColumnName(SqlColumnName sqlColumnName) {
        this.sqlColumnName = sqlColumnName;
    }

    private String getColumnNameInExpression() {

        if (sqlColumnName != null) {
            ColumnName columnName = sqlColumnName.getColumnName();
            TableName tableName = sqlColumnName.getTableName();
            if (!columnName.equals("") && (tableName != null && !tableName.equals(""))) {
                return tableName.getAllowedName().getName() + '.' + columnName.getAllowedName().getName();
            }
            return columnName.getAllowedName().getName();
        }
        return "";
    }


    private String getAggNameInExpression() {
        String ans = "";
        if (this.getFunctionName() != null) {
            String functionName = this.getFunctionName().getName();
            ans += functionName + "%";
            try {
                StringBuilder dataName = new StringBuilder();
                Expression subExpr = this.getExpressionArrayList().get(0);
                if (subExpr.getSqlColumnName() != null) {
                    if (subExpr.getSqlColumnName().getTableName() != null) {
                        dataName.append(subExpr.getSqlColumnName().getTableName().getAllowedName().getName());
                        dataName.append('.');
                    }
                    dataName.append(subExpr.getSqlColumnName().getColumnName().getAllowedName().getName());
                }
                ans += dataName.toString();

            } catch (Exception e) {
                System.out.println("Exception expression.java: " + e);
                return "";
            }
        }
        return ans;
    }

    private String getLastValue() {
        if (terminalNode != null) {
            if (terminalNode.getNummericValue() != null) {
                return terminalNode.getNummericValue().getValue();
            }
        }
        return "";
    }

    public String getOperatorBetweenTwoExpressions() {
        if (sqlAndOr != null) {
            return sqlAndOr.getContent();
        }
        if (or != null) {
            return or.getContent();
        }
        if (sqlLogicalOperator != null) {
            return sqlLogicalOperator.getContent();
        }
        if (logicalOperator != null) {
            return logicalOperator.getContent();
        }
        return "";
    }

    public SingleExpression getAnExpression(Expression one, Expression two, String opr) {
        String l = one.getColumnNameInExpression();
        String r = two.getLastValue();
        if (r.equals("")) r = two.getColumnNameInExpression();
        return new SingleExpression(l, r, opr);
    }

    public void getSingleExpressions(ArrayList<SingleExpression> arrayList, ArrayList<String> oprs) {
        try {
            Expression leftExpr = expressionArrayList.get(0);
            Expression rightExpr = expressionArrayList.get(1);


            String opr = this.getOperatorBetweenTwoExpressions();

            if (leftExpr != null && rightExpr != null) {
                if (rightExpr.expressionArrayList != null &&
                        rightExpr.expressionArrayList.get(0) != null &&
                        rightExpr.expressionArrayList.get(0).getListOfValues() != null) {
                    String lv = rightExpr.expressionArrayList.get(0).getListOfValues().getContent();
                    System.out.println("LV LV :" + lv);
                    System.out.println("opr opr opr: " + opr );
                    String[] arrOfValues = lv.split(",");
                    String l = leftExpr.getColumnNameInExpression();
                    for (int i = 0; i < arrOfValues.length; i++) {
                        if(opr.toLowerCase().equals("notin") || opr.toLowerCase().equals("not in") || opr.toLowerCase().equals("isnot") || opr.toLowerCase().equals("is not")) {
                            SingleExpression singleExpression = new SingleExpression(l, arrOfValues[i], "!=");
                            oprs.add("and");
                            arrayList.add(singleExpression);
                        }

                        if(opr.toLowerCase().equals("in") || opr.toLowerCase().equals("is")) {
                            SingleExpression singleExpression = new SingleExpression(l, arrOfValues[i], "=");
                            oprs.add("or");
                            arrayList.add(singleExpression);
                        }

                    }
                } else {

                    System.out.println("rightExpr: " + rightExpr);
                    String l = leftExpr.getColumnNameInExpression();
                    String r = rightExpr.getLastValue();
                    if (r.equals("")) r = rightExpr.getColumnNameInExpression();
                    if (!(l.equals("") && r.equals(""))) {
                        SingleExpression singleExpression = new SingleExpression(l, r, opr);
                        arrayList.add(singleExpression);
                    } else {
                        oprs.add(opr);
                        leftExpr.getSingleExpressions(arrayList, oprs);
                        rightExpr.getSingleExpressions(arrayList, oprs);
                    }
                }

            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }


    public void getSingleExpressionsForHavingClause(ArrayList<SingleExpression> arrayList, ArrayList<String> oprs) {
        try {
            Expression leftExpr = expressionArrayList.get(0);
            Expression rightExpr = expressionArrayList.get(1);
            String opr = this.getOperatorBetweenTwoExpressions();
            if (leftExpr != null && rightExpr != null) {

                String l = leftExpr.getAggNameInExpression();
                if (!l.equals("")) {
                    String[] str = l.split("%");
                    for (String s : str) {
                        System.out.println("AGGGGGGGGGGGGGGGGGGGGGG: " + s);
                    }
                    String r = rightExpr.getLastValue();
                    if (r.equals("")) r = rightExpr.getColumnNameInExpression();
                    String javaMethod = SQLAGGTOJAVACONVERTER.getMethodName(str[0]);
                    SingleExpression singleExpression = new SingleExpression(javaMethod, str[1], "agg_f");
                    System.out.println(singleExpression);
                    singleExpression.setAggOperator(opr);
                    singleExpression.setAggValue(r);
                    arrayList.add(singleExpression);
                } else {
                    l = leftExpr.getColumnNameInExpression();

                    String r = rightExpr.getLastValue();
                    if (r.equals("")) r = rightExpr.getColumnNameInExpression();

                    if (!(l.equals("") && r.equals(""))) {
                        if (!l.equals("")) {
                            SingleExpression singleExpression = new SingleExpression(l, r, opr);
                            arrayList.add(singleExpression);
                        }

                    } else {
                        oprs.add(opr);
                        leftExpr.getSingleExpressionsForHavingClause(arrayList, oprs);
                        rightExpr.getSingleExpressionsForHavingClause(arrayList, oprs);

                    }
                }


            }
        } catch (Exception e) {
            System.out.println("Damn error: " + e);
        }
    }

    public ListOfValues getListOfValues() {
        return listOfValues;
    }

    public void setListOfValues(ListOfValues listOfValues) {
        this.listOfValues = listOfValues;
    }

    @Override
    public String toString() {
        return "Expression{" +
                "sqlColumnName=" + sqlColumnName +
                ", expressionArrayList=" + expressionArrayList +
                ", ternaryExpression=" + ternaryExpression +
                ", terminalNode=" + terminalNode +
                ", mathFirstPriorty=" + mathFirstPriorty +
                ", mathSecondPriorty=" + mathSecondPriorty +
                ", bitwiseOperator=" + bitwiseOperator +
                ", logicalOperator=" + logicalOperator +
                ", sqlAndOr=" + sqlAndOr +
                ", or=" + or +
                ", sqlLogicalOperator=" + sqlLogicalOperator +
                ", listOfValues=" + listOfValues +
                ", functionName=" + functionName +
                '}';
    }
}
