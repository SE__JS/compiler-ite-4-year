package SingleParse.Expression;

import AST.ASTVisitor;
import GrammarRules.Node;

public class BitwiseOperator extends Node {

    String content;

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);
    System.out.println("content = " + content);
  }

  public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public BitwiseOperator(int line, int col) {
        super(line, col);
    }
}
