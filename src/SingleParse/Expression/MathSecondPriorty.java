package SingleParse.Expression;

import AST.ASTVisitor;
import GrammarRules.Node;

public class MathSecondPriorty extends Node {

    String content;

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);

    System.out.println("content = " + content);
  }

  public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public MathSecondPriorty(int line, int col) {
        super(line, col);
    }
}
