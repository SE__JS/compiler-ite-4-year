package SingleParse.Expression;

import AST.ASTVisitor;
import GrammarRules.Node;

import SingleParse.SQL.SqlStmtList.SqlStatement.ColumnDefinition.ColumnName;
import SingleParse.SQL.SqlStmtList.SqlStatement.MainSql.DatabaseName;
import SingleParse.SQL.SqlStmtList.SqlStatement.MainSql.TableName;

public class SqlColumnName extends Node {
  TableName tableName;
  DatabaseName databaseName;
  ColumnName columnName;

  @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);
    if (databaseName != null && columnName != null) {
      databaseName.accept(astVisitor);
      columnName.accept(astVisitor);
    }
    if(tableName != null) {
      tableName.accept(astVisitor);
    }

  }

  public SqlColumnName(int line, int col) {
    super(line, col);
  }

  public TableName getTableName() {
    return tableName;
  }

  public void setTableName(TableName tableName) {
    this.tableName = tableName;
  }

  public DatabaseName getDatabaseName() {
    return databaseName;
  }

  public void setDatabaseName(DatabaseName databaseName) {
    this.databaseName = databaseName;
  }

  public ColumnName getColumnName() {
    return columnName;
  }

  public void setColumnName(ColumnName columnName) {
    this.columnName = columnName;
  }
}
