package SingleParse;

import AST.ASTVisitor;

public class SingleParseError extends SingleParse {
  String error;

    public SingleParseError(int line, int col) {
        super(line, col);
    }

    @Override
  public void accept(ASTVisitor astVisitor) {
    astVisitor.visit(this);

    System.out.println("error = " + error);

  }

  public SingleParseError(int line, int col,String test) {
        super(line, col);
        this.error = test;
    }
}
