
grammar Sql_final;

parse:
 ( single_parse )* EOF
;
single_parse:
    any_statement
    | function_definition
    | error
;

error
 : UNEXPECTED_CHAR
   {
     throw new RuntimeException("UNEXPECTED_CHAR=" + $UNEXPECTED_CHAR.text);
   }
 ;

sql_stmt_list
 : ';'* sql_stmt ( ';'+ sql_stmt )* ';'*
 ;

sql_stmt
 :
    (
    // alter_table_stmt
     // | create_table_stmt
     // | delete_stmt
     // drop_table_stmt
     factored_select_stmt
     // | insert_stmt
     // | update_stmt
     )
;

//alter_table_stmt
// : K_ALTER K_TABLE ( database_name '.' )? source_table_name
//   ( K_RENAME K_TO new_table_name
//   | alter_table_add
//   | alter_table_add_constraint
//   | K_ADD K_COLUMN? column_def
//   )
// ;

//alter_table_add_constraint
// : K_ADD K_CONSTRAINT allowed_name table_constraint
// ;
//
//alter_table_add
// : K_ADD table_constraint
// ;



//create_table_stmt
// : K_CREATE K_TABLE ( K_IF K_NOT K_EXISTS )?
//   ( database_name '.' )? table_name
//   ( create_table_stmt_main
//   | K_AS select_stmt
//   )
// ;
// create_table_stmt_main:
//    '(' column_def ( single_table_constraint_or_column_def )* ')'
// ;
//single_table_constraint_or_column_def:
//',' table_constraint | ',' column_def
//;

//delete_stmt
// :  K_DELETE K_FROM qualified_table_name
//   ( K_WHERE expr )?
// ;

//drop_table_stmt
// : K_DROP K_TABLE ( K_IF K_EXISTS )? ( database_name '.' )? table_name
// ;

factored_select_stmt
 :  select_core ( factored_main )*
   ( K_ORDER K_BY ordering_term ( ',' ordering_term )* )?
   ( K_LIMIT expr ( ( K_OFFSET | ',' ) expr )? )?
 ;
factored_main:
    compound_operator select_core;

//insert_stmt
// : insert_stmt_main
//   ( insert_stmt_expr
//   | select_stmt
//   | insert_stmt_default
//   )
// ;
//
// insert_stmt_main:
//    K_INSERT K_INTO
//    ( database_name '.' )? table_name ( '(' column_name ( ',' column_name )* ')' )?
// ;
//insert_stmt_expr:
//    K_VALUES  insert_stmt_expr_left (insert_stmt_expr_right)*
//;
//insert_stmt_expr_left:
// '(' expr ( ',' expr )* ')'
//;
//insert_stmt_expr_right:
//    ( ',' '(' expr ( ',' expr )* ')' )
//;
//insert_stmt_default:
// K_DEFAULT K_VALUES
//;

select_stmt
 : select_core
   ( K_ORDER K_BY ordering_term ( ',' ordering_term )* )?
   ( K_LIMIT expr ( ( K_OFFSET | ',' ) expr )? )?
 ;



//update_stmt
// :K_UPDATE  qualified_table_name
//   K_SET  update_stmt_main ( ',' update_stmt_main )* ( K_WHERE expr )?
// ;
//update_stmt_main:
//    column_name '=' expr
//;
//column_def_constraint:
//    column_constraint | column_constraint_type_name
//;
//column_def
// : column_name ( column_def_constraint )*
// ;

//column_constraint_type_name
// : allowed_name ( '(' signed_number_allowed_name ')'
//         | '(' signed_number_allowed_name ',' signed_number_allowed_name ')' )?
// ;
//signed_number_allowed_name:
//    signed_number (allowed_name)?
//;
//column_constraint
// : ( K_CONSTRAINT allowed_name )?
//   ( column_constraint_primary_key
//   | column_constraint_foreign_key
//   | column_constraint_not_null
//   | column_constraint_null
//   | K_CHECK '(' expr ')'
//   | column_default
//   | K_COLLATE collation_name
//   )
// ;

//column_constraint_primary_key
// : K_PRIMARY K_KEY ( K_ASC | K_DESC )? K_AUTOINCREMENT?
// ;
//
//column_constraint_foreign_key
// : foreign_key_clause
// ;
//
//column_constraint_not_null
// : K_NOT K_NULL
// ;
//
//column_constraint_null
// : K_NULL
// ;
//
//column_default
// : K_DEFAULT  column_default_main
// ;
//column_default_main:
//    (column_default_value | '(' expr ')' | K_NEXTVAL '(' expr ')' | allowed_name ) column_default_list
//;
//column_default_list:
//( '::' allowed_name+ )?
//;
//
//column_default_value
// : ( signed_number | literal_value )
// ;
expr_or:
'||'
;
expr_math_first_priority_expr:
( '*' | '/' | '%' )
;
expr_math_second_priority:
( '+' | '-' )
;
expr_bitwise_operator:
( '<<' | '>>' | '&' | '|' )
;
expr_logical_operator:
    '<' | '>' | '<=' | '>='
;
expr_sql_logical_operator:
    ( '=' | '==' | '!=' | '<>' | K_IS | K_IS K_NOT | K_NOT K_IN | K_IN | K_LIKE | K_GLOB | K_MATCH | K_REGEXP )
;

expr_sql_andor:
    K_AND| K_OR
;
listOfValue:
     ( (allowed_name | NUMERIC_LITERAL) ',')* (allowed_name| NUMERIC_LITERAL)
;
expr
 :
 sql_column_name
 | unary_operator expr
 | expr expr_or expr
 | expr expr_math_first_priority_expr expr
 | expr expr_math_second_priority expr
 | expr expr_bitwise_operator expr
 | expr expr_logical_operator expr
 | expr expr_sql_logical_operator expr
 | expr expr_sql_andor expr
 | function_name '(' ( K_DISTINCT? expr ( ',' expr )* | '*' )? ')'
 | '(' expr ')'
 | terminal_node
 | listOfValue
 | trExpression
 // | array_invoking

 ;
terminal_node:
 nummeric_value
    | sql_stmt
    | boolean_value
    | null_value
    // | array_def
    // | json_object
    | allowed_name
    | invoking_function
    | dotBlock
    // | ternary_exp
    | array_invoking
    | ternary_stmt

;
sql_column_name:
  ( ( database_name '.' )? table_name '.' )? column_name
;

array_invoking:
    allowed_name '[' possible_value ']'
;
//foreign_key_clause
// : foreign_key_clause_main
//   foreign_key_clause_list
//   foreign_key_clause_part
// ;
// foreign_key_clause_main:
//    K_REFERENCES ( database_name '.' )? foreign_table fk_target_column_name_list
// ;
//foreign_key_clause_list:
//    ( ( K_ON ( K_DELETE | K_UPDATE ) ( K_SET K_NULL
//                                        | K_SET K_DEFAULT
//                                        | K_CASCADE
//                                        | K_RESTRICT
//                                        | K_NO K_ACTION )
//         | K_MATCH allowed_name
//         )
//       )*
//;
//foreign_key_clause_part:
//    ( K_NOT? K_DEFERRABLE ( K_INITIALLY K_DEFERRED | K_INITIALLY K_IMMEDIATE )? K_ENABLE? )?
//;
//fk_target_column_name_list:
// ( '(' fk_target_column_name ( ',' fk_target_column_name )* ')' )?
//;
//fk_target_column_name
// : allowed_name
// ;

//indexed_column
// : column_name ( K_COLLATE collation_name )? ( K_ASC | K_DESC )?
// ;

//table_constraint
// : ( K_CONSTRAINT allowed_name )?
//   ( table_constraint_primary_key
//   | table_constraint_key
//   | table_constraint_unique
//   | K_CHECK '(' expr ')'
//   | table_constraint_foreign_key
//   )
// ;

//indexed_column_list:
//    '(' indexed_column ( ',' indexed_column )* ')'
//;
//allowed_name_index_column:
//   allowed_name? indexed_column_list
//;
//table_constraint_primary_key
// : K_PRIMARY K_KEY indexed_column_list
// ;
//
//table_constraint_foreign_key
// : K_FOREIGN K_KEY table_constraint_foreign_key_body
// ;
//table_constraint_foreign_key_body:
//    foreign_column_name_list foreign_key_clause
//;
//foreign_column_name_list:
//    '(' fk_origin_column_name ( ',' fk_origin_column_name )* ')'
//;
//table_constraint_unique
// : K_UNIQUE K_KEY? allowed_name_index_column
// ;
//
//table_constraint_key
// : K_KEY allowed_name_index_column
// ;

//fk_origin_column_name
// : allowed_name
// ;
//
//qualified_table_name
// : ( database_name '.' )? table_name indexed_or_not
// ;
//indexed_or_not:
//    ( indexed_qualified_table_name |  not_indexed_qualified_table_name)?
//;
//indexed_qualified_table_name:
//    K_INDEXED K_BY allowed_name
//;
//not_indexed_qualified_table_name:
//    K_NOT K_INDEXED
//;
ordering_term
 : expr ( K_COLLATE collation_name )? ( K_ASC | K_DESC )?
 ;
result_column_star:
    '*'
;
result_column
 : result_column_star
 | allowed_name
 | allowed_name '.' '*'
 | result_column_expr
 ;

result_column_expr:
 expr ( K_AS? column_alias )?
;

table_or_subquery
 : ( database_name '.' )? table_name ( K_AS? table_alias )?
   ( K_INDEXED K_BY index_name
   | K_NOT K_INDEXED )?
 | table_or_subquery_main
 | table_or_subquery_last
 ;
table_or_subquery_last:
'(' select_stmt ')' ( K_AS? table_alias )?;
table_or_subquery_main:

'(' ( table_or_subquery ( ',' table_or_subquery )*
       | join_clause )
   ')' ( K_AS? table_alias )?

   ;
join_clause_main:
    join_operator table_or_subquery join_constraint
;

join_clause
 : table_or_subquery ( join_clause_main )*
 ;

join_operator
 : ','
 | ( K_LEFT K_OUTER? | K_RIGHT K_OUTER? | K_INNER )? K_JOIN
 ;

join_constraint
 : ( K_ON expr)?
 ;

whereClause:
 K_WHERE expr
;
groupByClause:
K_GROUP K_BY expr ( ',' expr )*;

havingClause:
( K_HAVING expr )?;

select_core
 : K_SELECT ( K_DISTINCT | K_ALL )? result_column ( ',' result_column )*
   select_core_first
   whereClause?
   ( groupByClause  havingClause )?
 // | K_VALUES insert_stmt_expr_left (insert_stmt_expr_right)*
 ;
select_core_first:
    ( K_FROM ( table_or_subquery ( ',' table_or_subquery )* | join_clause ) )?
;
compound_operator
 : K_UNION
 | K_UNION K_ALL
 | K_INTERSECT
 | K_EXCEPT
 ;

//signed_number
// : ( ( '+' | '-' )? NUMERIC_LITERAL | '*' )
// ;
//
//literal_value
// : NUMERIC_LITERAL
// | STRING_LITERAL
// | BLOB_LITERAL
// | K_NULL
// | K_CURRENT_TIME
// | K_CURRENT_DATE
// | K_CURRENT_TIMESTAMP
// ;

unary_operator
 : '-'
 | '+'
 | '~'
 | K_NOT
 ;

column_alias
 : IDENTIFIER
 | STRING_LITERAL
 ;


keyword
 : K_ABORT
 | K_ACTION
 | K_ADD
 | K_AFTER
 | K_ALL
 | K_ALTER
 | K_ANALYZE
 | K_AND
 | K_AS
 | K_ASC
 | K_ATTACH
 | K_AUTOINCREMENT
 | K_BEFORE
 | K_BEGIN
 | K_BETWEEN
 | K_BY
 | K_CASCADE
 | K_CASE
 | K_CAST
 | K_CHECK
 | K_COLLATE
 | K_COLUMN
 | K_COMMIT
 | K_CONFLICT
 | K_CONSTRAINT
 | K_CREATE
 | K_CROSS
 | K_CURRENT_DATE
 | K_CURRENT_TIME
 | K_CURRENT_TIMESTAMP
 | K_DATABASE
 | K_DEFAULT
 | K_DEFERRABLE
 | K_DEFERRED
 | K_DELETE
 | K_DESC
 | K_DETACH
 | K_DISTINCT
 | K_DROP
 | K_EACH
 | K_ELSE
 | K_END
 | K_ENABLE
 | K_ESCAPE
 | K_EXCEPT
 | K_EXCLUSIVE
 | K_EXISTS
 | K_EXPLAIN
 | K_FAIL
 | K_FOR
 | K_FOREIGN
 | K_FROM
 | K_FULL
 | K_GLOB
 | K_GROUP
 | K_HAVING
 | K_IF
 | K_IGNORE
 | K_IMMEDIATE
 | K_IN
 | K_INDEX
 | K_INDEXED
 | K_INITIALLY
 | K_INNER
 | K_INSERT
 | K_INSTEAD
 | K_INTERSECT
 | K_INTO
 | K_IS
 | K_ISNULL
 | K_JOIN
 | K_KEY
 | K_LEFT
 | K_LIKE
 | K_LIMIT
 | K_MATCH
 | K_NATURAL
 | K_NO
 | K_NOT
 | K_NOTNULL
 | K_NULL
 | K_OF
 | K_OFFSET
 | K_ON
 | K_OR
 | K_ORDER
 | K_OUTER
 | K_PLAN
 | K_PRAGMA
 | K_PRIMARY
 | K_QUERY
 | K_RAISE
 | K_RECURSIVE
 | K_REFERENCES
 | K_REGEXP
 | K_REINDEX
 | K_RELEASE
 | K_RENAME
 | K_REPLACE
 | K_RESTRICT
 | K_RIGHT
 | K_ROLLBACK
 | K_ROW
 | K_SAVEPOINT
 | K_SELECT
 | K_SET
 | K_TABLE
 | K_TEMP
 | K_TEMPORARY
 | K_THEN
 | K_TO
 | K_TRANSACTION
 | K_TRIGGER
 | K_UNION
 | K_UNIQUE
 | K_UPDATE
 | K_USING
 | K_VACUUM
 | K_VALUES
 | K_VIEW
 | K_VIRTUAL
 | K_WHEN
 | K_WHERE
 | K_WITH
 | K_WITHOUT
 | K_NEXTVAL
 | K_VAR
 ;



function_name
 : allowed_name
 ;

database_name
 : allowed_name
 ;

//source_table_name
// : allowed_name
//;

table_name
 :  allowed_name
 ;
//new_table_name
// : allowed_name
//;
//
column_name
 : allowed_name
 ;

collation_name
 : allowed_name
 ;

//foreign_table
// : allowed_name
// ;

index_name
 : allowed_name
 ;

table_alias
 : allowed_name
 ;

//array_def:
//  '[' possible_value (',' possible_value)* ']'
//  | '[' ']'
// ;


//any_name
// : IDENTIFIER
// | keyword
// | STRING_LITERAL
// | '(' any_name ')'
// ;
allowed_name
 : STRING_LITERAL
 | IDENTIFIER
 | '(' allowed_name ')'
;
SCOL : ';';
DOT : '.';
OPEN_PAR : '(';
CLOSE_PAR : ')';
COMMA : ',';
ASSIGN : '=';
STAR : '*';
PLUS : '+';
MINUS : '-';
TILDE : '~';
PIPE2 : '||';
DIV : '/';
MOD : '%';
LT2 : '<<';
GT2 : '>>';
AMP : '&';
PIPE : '|';
LT : '<';
LT_EQ : '<=';
GT : '>';
GT_EQ : '>=';
EQ : '==';
NOT_EQ1 : '!=';
NOT_EQ2 : '<>';

// http://www.sqlite.org/lang_keywords.html
K_ABORT : A B O R T;
K_ACTION : A C T I O N;
K_ADD : A D D;
K_AFTER : A F T E R;
K_ALL : A L L;
K_ALTER : A L T E R;
K_ANALYZE : A N A L Y Z E;
K_AND : A N D;
K_AS : A S;
K_ASC : A S C;
K_ATTACH : A T T A C H;
K_AUTOINCREMENT : A U T O I N C R E M E N T;
K_BEFORE : B E F O R E;
K_BEGIN : B E G I N;
K_BETWEEN : B E T W E E N;
K_BY : B Y;
K_CASCADE : C A S C A D E;
K_CASE : C A S E;
K_BREAK: B R E A K;
K_CAST : C A S T;
K_CHECK : C H E C K;
K_COLLATE : C O L L A T E;
K_COLUMN : C O L U M N;
K_COMMIT : C O M M I T;
K_CONFLICT : C O N F L I C T;
K_CONSTRAINT : C O N S T R A I N T;
K_CREATE : C R E A T E;
K_CROSS : C R O S S;
K_CURRENT_DATE : C U R R E N T '_' D A T E;
K_CURRENT_TIME : C U R R E N T '_' T I M E;
K_CURRENT_TIMESTAMP : C U R R E N T '_' T I M E S T A M P;
K_DATABASE : D A T A B A S E;
K_DEFAULT : D E F A U L T;
K_DEFERRABLE : D E F E R R A B L E;
K_DEFERRED : D E F E R R E D;
K_DELETE : D E L E T E;
K_DESC : D E S C;
K_DETACH : D E T A C H;
K_DISTINCT : D I S T I N C T;
K_DROP : D R O P;
K_EACH : E A C H;
K_ELSE : E L S E;
K_END : E N D;
K_ENABLE : E N A B L E;
K_ESCAPE : E S C A P E;
K_EXCEPT : E X C E P T;
K_EXCLUSIVE : E X C L U S I V E;
K_EXISTS : E X I S T S;
K_EXPLAIN : E X P L A I N;
K_FAIL : F A I L;
K_FOR : F O R;
K_FOR_EACH: F O R E A C H;
K_FOREIGN : F O R E I G N;
K_FROM : F R O M;
K_FULL : F U L L;
K_GLOB : G L O B;
K_GROUP : G R O U P;
K_HAVING : H A V I N G;
K_IF : I F;
K_IGNORE : I G N O R E;
K_IMMEDIATE : I M M E D I A T E;
K_IN : I N;
K_INDEX : I N D E X;
K_INDEXED : I N D E X E D;
K_INITIALLY : I N I T I A L L Y;
K_INNER : I N N E R;
K_INSERT : I N S E R T;
K_INSTEAD : I N S T E A D;
K_INTERSECT : I N T E R S E C T;
K_INTO : I N T O;
K_IS : I S;
K_ISNULL : I S N U L L;
K_JOIN : J O I N;
K_KEY : K E Y;
K_LEFT : L E F T;
K_LIKE : L I K E;
K_LIMIT : L I M I T;
K_MATCH : M A T C H;
K_NATURAL : N A T U R A L;
K_NEXTVAL : N E X T V A L;
K_NO : N O;
K_NOT : N O T;
K_NOTNULL : N O T N U L L;
K_NULL : N U L L;
K_OF : O F;
K_OFFSET : O F F S E T;
K_ON : O N;
K_ONLY : O N L Y;
K_OR : O R;
K_ORDER : O R D E R;
K_OUTER : O U T E R;
K_PLAN : P L A N;
K_PRAGMA : P R A G M A;
K_PRIMARY : P R I M A R Y;
K_QUERY : Q U E R Y;
K_RAISE : R A I S E;
K_RECURSIVE : R E C U R S I V E;
K_REFERENCES : R E F E R E N C E S;
K_REGEXP : R E G E X P;
K_REINDEX : R E I N D E X;
K_RELEASE : R E L E A S E;
K_RENAME : R E N A M E;
K_REPLACE : R E P L A C E;
K_RESTRICT : R E S T R I C T;
K_RIGHT : R I G H T;
K_ROLLBACK : R O L L B A C K;
K_ROW : R O W;
K_SAVEPOINT : S A V E P O I N T;
K_SELECT : S E L E C T;
K_SET : S E T;
K_TABLE : T A B L E;
K_TEMP : T E M P;
K_TEMPORARY : T E M P O R A R Y;
K_THEN : T H E N;
K_TO : T O;
K_TRANSACTION : T R A N S A C T I O N;
K_TRIGGER : T R I G G E R;
K_UNION : U N I O N;
K_UNIQUE : U N I Q U E;
K_UPDATE : U P D A T E;
K_USING : U S I N G;
K_VACUUM : V A C U U M;
K_VALUES : V A L U E S;
K_VIEW : V I E W;
K_VIRTUAL : V I R T U A L;
K_WHEN : W H E N;
K_WHERE : W H E R E;
K_WITH : W I T H;
K_WITHOUT : W I T H O U T;
K_VAR : V A R;
K_WHILE: W H I L E;
K_RETURN: R E T U R N;
K_FUNCTION: F U N C T I O N;
K_TRUE: T R U E;
K_FALSE: F A L S E;
K_SWITCH: S W I T C H;
K_DO:
    D O
;
K_INTEGER: I N T E G E R;
K_DOUBLE: D O U B L E;
K_STRING: S T R I N G;
K_NUMBER: N U M B E R;
K_OBJECT: O B J E C T;
K_BOOLEAN: B O O L E A N;
K_AGGREGATION_FUNCTION: A G G R E G A T I O N '_' F U N C T I O N;
K_TYPE: T Y P E;
K_PATH: P A T H;

IDENTIFIER
 : '"' (~'"' | '""')* '"'
 | '`' (~'`' | '``')* '`'
 // | '[' ~']'* ']'
 | [a-zA-Z_] [a-zA-Z_0-9]*
 ;

NUMERIC_LITERAL
 : DIGIT+ ( '.' DIGIT* )? ( E [-+]? DIGIT+ )?
 | '.' DIGIT+ ( E [-+]? DIGIT+ )?
 ;

BIND_PARAMETER
 : '?' DIGIT*
 //| [:@$] IDENTIFIER
 ;

STRING_LITERAL
 : '\'' ( ~'\'' | '\'\'' )* '\''
 ;

BLOB_LITERAL
 : X STRING_LITERAL
 ;
/*
SINGLE_LINE_COMMENT
 : '--' ~[\r\n]* -> channel(HIDDEN)
 ;*/

MULTILINE_COMMENT
 : '/*' .*? ( '*/' | EOF ) -> channel(HIDDEN)
 ;

SPACES
 : [ \u000B\t\r\n] -> channel(HIDDEN)
 ;

UNEXPECTED_CHAR
 : .
 ;

// ====================================MY REVIEW==================================

var_operator:
    // array_assignment |
    (ASSIGN possible_value);

//array_assignment:
//    ('['']') ASSIGN (array_def | allowed_name )
//;
var_arg:
   allowed_name var_operator?
;

var_definition
: K_VAR var_arg (',' var_arg)*  ';'+
;

var_assignment:
      allowed_name var_operator ';'+
;

var_statement:
   var_definition | var_assignment
;
//key_value_pair:
//    allowed_name ':' possible_value
//;
//json_object:
//    '{' key_value_pair (',' key_value_pair)* '}' | '{' '}'
//;

boolean_value:
   K_TRUE
  | K_FALSE
;
null_value:
    K_NULL
;
nummeric_value:
    NUMERIC_LITERAL
;

possible_value:
    nummeric_value
    | boolean_value
    | null_value
    | allowed_name
    | trExpression
    // | array_def
    // | json_object
    | invoking_function
    | expr
    | dotBlock
    | ternary_stmt
    // | <assoc=right> ternary_exp
    | array_invoking
;

comparasion:
 EQ | GT_EQ | LT_EQ | NOT_EQ1 | GT | LT
;

condition_params:
allowed_name | NUMERIC_LITERAL | K_TRUE | K_FALSE | expr
;
checking_block:
 OPEN_PAR conditional_expr next_conditional_expr* CLOSE_PAR
;
next_conditional_expr:
 logical_opration conditional_expr
;
logical_opration:
'||' | '&&'
;
any_statement:
      sql_stmt_list
    | single_line_statement
    | while_statement
    | do_while
    | for_statement
    | enhance_for
    | for_each_statement
    | scope
    | switchStatement
    | if_else
    ;

single_line_statement:
      var_statement
    | sql_stmt
    | function_definition
    | invoking_function ';'+
    | trExpression
    | ternary_stmt
    | print_statement
    | return_stmt
    | break_stmt
    | dotBlock
    // | ternary_exp
    | aggregationFunction
    | createTableStatement
    | create_type_stmt

;

create_type_stmt
 : K_CREATE K_TYPE column_name
   OPEN_PAR
   createTableParams
   CLOSE_PAR
  ;
  tableParamType:
      (K_NUMBER | K_BOOLEAN | K_STRING | allowed_name)
  ;

allowedCreateTableParam:
    tableParamType allowed_name
;
createTableParams:
    allowedCreateTableParam (',' allowedCreateTableParam)*
;
fileType:
    K_TYPE
    ASSIGN
    (IDENTIFIER | STRING_LITERAL)
;

filePath:
    K_PATH
    ASSIGN
    (IDENTIFIER | STRING_LITERAL )

;
createTableStatement:
    K_CREATE K_TABLE allowed_name OPEN_PAR
    createTableParams
    CLOSE_PAR
    fileType
    COMMA
    filePath
;


jarPath:
    allowed_name
;
className:
    allowed_name
;
methodName:
    allowed_name
;

returnType:
    allowed_name | K_NUMBER | K_BOOLEAN | K_STRING
;
aggregationFunctionParameterArray:
     '[' returnType (',' returnType)* ']'
      | '[' ']'
;
aggregationFunction:
    K_CREATE K_AGGREGATION_FUNCTION allowed_name OPEN_PAR
    jarPath COMMA
    className COMMA
    methodName COMMA
    returnType (COMMA
    aggregationFunctionParameterArray)?
    CLOSE_PAR
;

do_while:
    K_DO '{' any_statement* '}' K_WHILE checking_block ';'+
;


break_stmt:
    K_BREAK ';'+
;

return_stmt:
(K_RETURN (  sql_stmt | single_line_statement | possible_value )? ';'*)
;
scope:
    '{' any_statement* '}'
;

print_statement:
    'print' (OPEN_PAR) ( allowed_name | expr )? (CLOSE_PAR) ';'
;

single_plus_minus: '++' | '--';

ternary_stmt:
    leftTernary_stmt | rightTernary_stmt
;
leftTernary_stmt:
(single_plus_minus)+ allowed_name leftTernaryRightSide ';'*
;

leftTernaryRightSide:
((single_plus_minus)* | (ternary_oprator possible_value))
;

rightTernary_stmt:
(single_plus_minus)* allowed_name rightTernaryRightSide  ';'*
;

rightTernaryRightSide:
((single_plus_minus)+ | (ternary_oprator possible_value))
;

ternary_oprator:
('+=' | '-=' | '/=' | '*=' )
;

for_statement:
    K_FOR OPEN_PAR initial_statement conditional_expr ';' ternary_stmt CLOSE_PAR
    any_statement
;
initial_statement:
    single_line_statement
;
single_loop_statement:
     single_line_statement
;
conditional_expr:
    condition_params rightSideComparasion?
;
rightSideComparasion:
    (comparasion condition_params)
;
enhance_for:
    K_FOR OPEN_PAR var_definition (':' | K_IN | K_OF | K_FROM)? allowed_name  CLOSE_PAR
    any_statement
;

for_each_statement:
    K_FOR_EACH OPEN_PAR (/*higher_order_function |*/ invoking_function )? CLOSE_PAR ';'+
;
while_statement:
    K_WHILE checking_block
    any_statement
;
if_else:
    K_IF checking_block
    if_statements

    ( K_ELSE else_statements )?
;

if_statements:
any_statement
;
else_statements:
any_statement
;


caseBlock:
    K_CASE possible_value ':'
        any_statement*
       // ( K_BREAK | K_RETURN )? ';'
;
defaultStatement:
    K_DEFAULT ':'
        any_statement
       // ( K_BREAK | K_RETURN )? ';'
;


switchStatement
    : K_SWITCH '(' expr ')' '{'

        caseBlock* defaultStatement?
'}'
;

dotBlock:
    dotBlock dotBlockRightSide | allowed_name
;
dotBlockRightSide:
    ('.' allowed_name) ';'*
;

function_definition:
    function_name OPEN_PAR ((param ',' )* (param | ((optional_param ',' )* optional_param)?))?  CLOSE_PAR
        '{'
               any_statement*
               // return_stmt?
        '}'
;
//higher_order_function:
//    K_FUNCTION OPEN_PAR arg_list CLOSE_PAR
//    any_statement
//;
arg_list:
    ((arg ',' )* arg)?
;
arg:
    NUMERIC_LITERAL
    | STRING_LITERAL
    | allowed_name
    | boolean_value
    // | higher_order_function
;
invoking_function:
    function_name OPEN_PAR arg_list  CLOSE_PAR ';'*
;


 param: (K_VAR allowed_name);

 optional_param: (K_VAR allowed_name '=' (possible_value));

//ternary_exp :
//     ternary_exp '*' ternary_exp
//     | <assoc=right> ternary_exp '+' ternary_exp
//     | <assoc=left> ternary_exp '-' ternary_exp
//     | <assoc=left> ternary_exp '/' ternary_exp
//     | <assoc=right> ternary_exp comparasion? ternary_exp '?' ternary_exp ':' ternary_exp
//     | <assoc=right> ternary_exp '=' ternary_exp
//     | NUMERIC_LITERAL
//     | allowed_name
// ;

 trExpression:
terminal_node
 | sql_column_name
 | unary_operator trExpression
 | trExpression expr_or trExpression
 | trExpression expr_math_first_priority_expr trExpression
 | trExpression expr_math_second_priority trExpression
 | trExpression expr_bitwise_operator trExpression
 | trExpression expr_logical_operator trExpression
 | trExpression expr_sql_logical_operator trExpression
 | trExpression expr_sql_andor trExpression
 | function_name '(' ( K_DISTINCT? trExpression ( ',' trExpression )* | '*' )? ')'
 | <assoc=right> trExpression '?' trExpression ':' trExpression
 | <assoc=right> trExpression '=' trExpression
 | '(' trExpression ')'
 // | array_invoking
 ;
// ====================================MY REVIEW==================================
fragment DIGIT : [0-9]+;
fragment A : [aA];
fragment B : [bB];
fragment C : [cC];
fragment D : [dD];
fragment E : [eE];
fragment F : [fF];
fragment G : [gG];
fragment H : [hH];
fragment I : [iI];
fragment J : [jJ];
fragment K : [kK];
fragment L : [lL];
fragment M : [mM];
fragment N : [nN];
fragment O : [oO];
fragment P : [pP];
fragment Q : [qQ];
fragment R : [rR];
fragment S : [sS];
fragment T : [tT];
fragment U : [uU];
fragment V : [vV];
fragment W : [wW];
fragment X : [xX];
fragment Y : [yY];
fragment Z : [zZ];
